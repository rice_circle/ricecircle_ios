//
//  AppDelegate.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/4/25.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

