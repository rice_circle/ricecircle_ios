//
//  RiceCircleController.m
//  RiceCircle
//
//  Created by Passion on 2017/5/16.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "RiceCircleController.h"
#import "HomePageViewController.h"
#import "RiceCircleViewController.h"
#import "MineViewController.h"
#import "HomePageViewController2.h"
#import "RiceCircleViewController2.h"
#import "RiceCircleViewController3.h"

@interface RiceCircleController ()

@end

@implementation RiceCircleController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    // Do any additional setup after loading the view from its nib.
    self.automaticallyAdjustsScrollViewInsets = YES;
    [self setupViews];
    [self initTabbarItem];
}

- (void)setupViews{
    //首页
    HomePageViewController2 *homeVC = [[HomePageViewController2 alloc] init];
    UINavigationController *homeNav = [[UINavigationController alloc] initWithRootViewController:homeVC];
    //饭圈
    RiceCircleViewController3 *riceCircleVC = [[RiceCircleViewController3 alloc] init];
    UINavigationController *riceCircleNav = [[UINavigationController alloc] initWithRootViewController:riceCircleVC];
    //我的
    MineViewController *mineVC = [[MineViewController alloc] init];
    UINavigationController *mineNav = [[UINavigationController alloc] initWithRootViewController:mineVC];
    self.viewControllers=@[homeNav,riceCircleNav,mineNav];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initTabbarItem{
        NSArray *items = self.tabBar.items;
        UITabBarItem *item = nil;
        NSLog(@"SSSS");
        NSLog(@"item count %lu",[items count]);
        for (NSInteger i = 0 ; i < [items count]; i ++) {
            UIImage *imgSelect = nil;
            UIImage *imgUnSelect = nil;
            NSString *title = nil;
            NSInteger tag = 0;
            item = [items objectAtIndex:i];
            switch (i) {
                case 0:
                    imgSelect = [[UIImage imageNamed:@"tabbar_icon_homepage_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    imgUnSelect = [[UIImage imageNamed:@"tabbar_icon_homepage_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    title = @"主页";
                    tag = 1000;
                    break;
    
                case 1:
                    imgSelect = [[UIImage imageNamed:@"tabbar_icon_circle_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    imgUnSelect = [[UIImage imageNamed:@"tabbar_icon_circle_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    title = @"饭圈";
                    tag = 1001;
                    break;
                case 2:
                    imgSelect = [[UIImage imageNamed:@"tabbar_icon_my_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    imgUnSelect = [[UIImage imageNamed:@"tabbar_icon_my_noraml"]
                        imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    title = @"我的";
                    tag = 1002;
                    break;
            }
            if(IOS7){
                [item setSelectedImage:imgSelect];
                [item setImage:imgUnSelect];
                [item setTitle:title];
                [item setTag:tag];
            }else {
                [item setFinishedSelectedImage:imgSelect withFinishedUnselectedImage:imgUnSelect];
                [item setTitle:title];
                [item setTag:tag];
            }
        }
    }

@end
