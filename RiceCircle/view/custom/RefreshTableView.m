//
//  RefreshTableView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/16.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "RefreshTableView.h"
#import "MJRefresh.h"

@implementation RefreshTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style type:(TableType)type{
    self = [super initWithFrame:frame style:style];
    if(self){
        [self initRefresh:type];
    }
    return self;
}

//初始化刷新与加载
- (void)initRefresh:(TableType)type{
    if(type == REFRESH || type == BOTH){
        MJRefreshNormalHeader *header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
            NSLog(@"RefreshTableView refresh");
            if(nil != _refresh){
                self.refresh();
            }
        }];
        // 设置文字
        [header setTitle:@"下拉可刷新" forState:MJRefreshStateIdle];
        [header setTitle:@"松开立即刷新" forState:MJRefreshStatePulling];
        [header setTitle:@"加载中..." forState:MJRefreshStateRefreshing];
        
        [header.lastUpdatedTimeLabel setText:@"最后更新"];
        self.mj_header = header;
    }
    if(type == LOAD  || type == BOTH){
        MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            if(nil != _loading){
                self.loading();
            }
        }];
        self.tableFooterView = [[UIView alloc]init];
        self.mj_footer = footer;
        [footer setTitle:@"点击或上拉加载更多" forState:MJRefreshStateIdle];
        [footer setTitle:@"正在加载更多数据..." forState:MJRefreshStateRefreshing];
        [footer setTitle:@"已全部加载完毕" forState:MJRefreshStateNoMoreData];
        //开始的时候隐藏footer
        [footer setAutomaticallyHidden:YES];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
