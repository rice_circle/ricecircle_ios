//
//  GFCalendarScrollView.h
//
//  Created by Mercy on 2016/11/9.
//  Copyright © 2016年 Mercy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^DidSelectDayHandler)(NSInteger, NSInteger, NSInteger);

@interface GFCalendarScrollView : UIScrollView

@property (nonatomic, copy) DidSelectDayHandler didSelectDayHandler; // 日期点击回调

//每个月选择的日期
@property (nonatomic,strong) NSMutableDictionary *selectDictionary;

//最大日期
@property (nonatomic,strong) NSDate *maxDate;

//最小日期
@property (nonatomic,strong) NSDate *minDate;
//今天日期显示的颜色
@property (nonatomic,strong) UIColor *currentDayColor;

- (void)refreshToCurrentMonth; // 刷新 calendar 回到当前日期月份


@end
