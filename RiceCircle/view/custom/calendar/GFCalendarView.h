//
//  GFCalendarView.h
//
//  Created by Mercy on 2016/11/9.
//  Copyright © 2016年 Mercy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^DidSelectDayHandler)(NSInteger, NSInteger, NSInteger);

@interface GFCalendarView : UIView


/**
 *  构造方法
 *
 *  @param origin calendar 的位置
 *  @param width  calendar 的宽度（高度会根据给定的宽度自动计算）
 *
 *  @return bannerView对象
 */
- (instancetype)initWithFrameOrigin:(CGPoint)origin width:(CGFloat)width;


/**
 *  calendar 的高度（只读属性）
 */
@property (nonatomic, assign, readonly) CGFloat calendarHeight;


/**
 *  calendar 基本颜色
 */
@property (nonatomic, strong) UIColor *calendarBasicColor;


/**
 *  日期点击回调
 *  block 的参数表示当前日期的 NSDate 对象
 */
@property (nonatomic, copy) DidSelectDayHandler didSelectDayHandler;


/**
 设置每个月选择的日期

 @param selectDictionary 每个月选择的日期
 */
- (void)setSelectDictionary:(NSMutableDictionary *)selectDictionary;

/**
 设置日历最大日期

 @param maxDate 最大日期
 */
- (void)setMaxDate:(NSDate*)maxDate;

/**
 设置日历最小日期
 
 @param minDate 最小日期
 */
- (void)setMinDate:(NSDate*)minDate;


/**
 设置今天日期显示的颜色

 @param currentDayColor 显示颜色
 */
- (void)setCurrentDayColor:(UIColor *)currentDayColor;
@end
