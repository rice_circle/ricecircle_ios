//  带下划线的输入框
//  UnderlineInputView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/11.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "UnderlineInputView.h"
#import "UIColor+Hex.h"
#import "SCInputAccessoryView.h"

@interface UnderlineInputView()<AccessoryItemDelegate>
@property (nonatomic,strong) UITextField *txfContent;
@property (nonatomic,strong) UIView *underLine;
@property (nonatomic,strong) SCInputAccessoryView *inputAccessoryView;
@end

@implementation UnderlineInputView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setupViews];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setupViews];
}

//初始化view
- (void)setupViews{
    [self initInputAccessory];
    CGRect frame = self.frame;
    _txfContent = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - 1)];
    _txfContent.backgroundColor = [UIColor clearColor];
    _txfContent.font = SC_FONT_14;
    _txfContent.textColor = SC_Color_Black_999999;
    _txfContent.inputAccessoryView = _inputAccessoryView;
    [self addSubview:_txfContent];
    
    _underLine = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height - 1, frame.size.width, 1)];
    _underLine.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
    [self addSubview:_underLine];
}

//初始化键盘工具栏
- (void)initInputAccessory{
    _inputAccessoryView = [[SCInputAccessoryView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 44) withType:OneItem];
    _inputAccessoryView.accessoryDelegate = self;
    [_inputAccessoryView buildViews];
}

- (UITextField*)getContentView{
    return _txfContent;
}

#pragma mark AccessoryItemDelegate

- (void)accessoryItemClick:(NSInteger)index{
    NSLog(@"accessoryItemClick index=%lu",index);
    [_txfContent resignFirstResponder];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
