//  活动发布页面，活动项集合view
//  ActivityItemsView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/11.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ActivityItemsView.h"
#import "UnderlineInputView.h"
#import "NSString+Extension.h"
#import "UIColor+Hex.h"

@interface ActivityItemsView()
@property (nonatomic,assign) NSInteger count;
@property (nonatomic,assign) CGFloat y;
@end

@implementation ActivityItemsView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setupViews];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setupViews];
}

- (void)setupViews{
    _count = 1;
    _y = 0;
    CGRect frame = CGRectMake(0, _y, self.frame.size.width, 0);
    NSLog(@"frame=%@",NSStringFromCGRect(frame));
    UIView *item = [self createItem:frame];
    _y = item.frame.origin.y + item.frame.size.height;
    [self addSubview:item];
    //重新计算父view的frame
    CGRect superFrame = self.frame;
    self.frame = CGRectMake(superFrame.origin.x, superFrame.origin.y, superFrame.size.width, item.frame.size.height);
}

//添加一个item
- (void)addItem{
    CGRect frame = CGRectMake(0, _y, self.frame.size.width, 0);
    UIView *item = [self createItem:frame];
    _y = item.frame.origin.y + item.frame.size.height;
    [self addSubview:item];
    //重新计算父view的frame
    CGRect superFrame = self.frame;
    self.frame = CGRectMake(superFrame.origin.x, superFrame.origin.y, superFrame.size.width, superFrame.size.height + item.frame.size.height);
}

//创建一个item
- (UIView*)createItem:(CGRect)frame{
    UIView *item = [[UIView alloc] init];
    NSString *type = @"类型：";
    CGSize maxSize = CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
    //标签字体大小
    CGFloat fontSize = 12;
    //中间分割线的宽度
    CGFloat splitWith = 21;
    //分割线与两边的间距
    CGFloat space = 11;
    CGFloat height = 0;
    CGSize typeTextRealSize = [type sizeWithFont:[UIFont systemFontOfSize:fontSize] maxSize:maxSize].size;
    UILabel *lblType = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, typeTextRealSize.width, typeTextRealSize.height)];
    lblType.font = [UIFont systemFontOfSize:fontSize];
    lblType.textColor = [UIColor colorWithHexString:@"#999999"];
    lblType.text = type;
    [item addSubview:lblType];
    
    NSString *price = @"价格：";
    CGFloat halfWidth = (frame.size.width - splitWith - space * 2) / 2.0f;
    CGFloat secondStartX = halfWidth + space + splitWith + space;
    NSLog(@"halfWidth=%f,secondStartX=%f",halfWidth,secondStartX);
    CGSize priceTextRealSize = [price sizeWithFont:[UIFont systemFontOfSize:fontSize] maxSize:maxSize].size;
    UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(secondStartX, 0, priceTextRealSize.width, priceTextRealSize.height)];
    lblPrice.font = [UIFont systemFontOfSize:fontSize];
    lblPrice.textColor = [UIColor colorWithHexString:@"#999999"];
    lblPrice.text = type;
    [item addSubview:lblPrice];
    
    height += typeTextRealSize.height + 10;
    
    UnderlineInputView *typeInput = [[UnderlineInputView alloc] initWithFrame:CGRectMake(0, typeTextRealSize.height + 5, halfWidth, 25)];
    typeInput.getContentView.font = [UIFont systemFontOfSize:14];
    typeInput.getContentView.placeholder = @"请输入类型内容";
    [item addSubview:typeInput];
    
    CGFloat splitStartY = typeTextRealSize.height + 5 + 12;
    UIView *splitView = [[UIView alloc] initWithFrame:CGRectMake(halfWidth + space, splitStartY, splitWith, 1)];
    splitView.backgroundColor = [UIColor colorWithHexString:@"#999999"];
    [item addSubview:splitView];
    
    UnderlineInputView *priceInput = [[UnderlineInputView alloc] initWithFrame:CGRectMake(secondStartX, typeTextRealSize.height + 5, halfWidth, 25)];
    priceInput.getContentView.font = [UIFont systemFontOfSize:14];
    priceInput.getContentView.placeholder = @"请输入该类型价格";
    [item addSubview:priceInput];
    height += 25;
    item.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width,  height);
    return item;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
