//  流式布局
//  SCFlowLayout.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/24.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SCFlowLayout.h"
#import "NSString+Extension.h"
#import "UIColor+Hex.h"

@interface SCFlowLayout()
@property (nonatomic,strong) NSArray *dataSet;
@property (nonatomic,assign) NSInteger preIndex;
@property (nonatomic,strong) NSMutableArray *views;
@end

@implementation SCFlowLayout

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self initData];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self initData];
}

- (void)initData{
    _views = [NSMutableArray array];
    _itemMargin = 15;
    _itemPadding = 10;
    _direction = HORIZONTAL;
    _itemHeight = 25;
    _fontSize = 14;
    _preIndex = -1;
    _unSelectBorderColor = [UIColor colorWithHexString:@"#D2D2D2"];
    _selectBorderColor = [UIColor colorWithHexString:@"#f25f72"];
    _borderWidth = 1;
    _borderCornerRadius = 5;
}

- (void)setData:(NSArray*)dataSet{
    _dataSet = dataSet;
    [self measure];
}

- (void)setupViews:(NSArray*)dataSet{
    
}

- (void)measure{
    CGRect frame = [self frame];
    NSLog(@"SCFlowLayout measure frame=%@",NSStringFromCGRect(frame));
    CGSize textMaxSize = CGSizeMake(SCREEN_WIDTH, MAXFLOAT);
    //开始x位置
    CGFloat startX = 0;
    //开始y位置
    CGFloat startY = 0;
    //控件宽度
    CGFloat width = frame.size.width;
    //每一个item的宽度
    CGFloat itemWidth = 0;
    NSString *content = nil;
    UILabel *item = nil;
    //水平方向每摆放一个item之后，剩余的宽度
    CGFloat remainWidth = frame.size.width;
    //如果当前index不是最后一项，就获取下一个item的宽度信息
    CGFloat itemWidthNext = 0;
    for (NSInteger i = 0 ; i < _dataSet.count; i++) {
        content = [_dataSet objectAtIndex:i];
        CGSize textRealSize = [content sizeWithFont:[UIFont systemFontOfSize:_fontSize] maxSize:textMaxSize].size;
        itemWidth = textRealSize.width + _itemPadding * 2;
        item = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY, itemWidth, _itemHeight)];
        item.font = [UIFont systemFontOfSize:_fontSize];
        item.textAlignment = NSTextAlignmentCenter;
        item.text = content;
        if(i < _dataSet.count - 1){
            NSString *contentNext = [_dataSet objectAtIndex:(i + 1)];
            CGSize textRealSizeNext = [contentNext sizeWithFont:[UIFont systemFontOfSize:_fontSize] maxSize:textMaxSize].size;
            itemWidthNext = textRealSizeNext.width + 2 * _itemPadding + _itemMargin;
        }else{
            itemWidthNext = 0;
        }
        remainWidth -= itemWidth;
        NSLog(@"content=%@,itemWidth=%f,remainWidth=%f,itemWidthNext=%f",content,itemWidth,remainWidth,itemWidthNext);
        //如果剩余的宽度大于下一个item的宽度，startX就加上当前item的宽度，startY不变。否则startX就赋值为0，startY加上当前item的宽度
        if( remainWidth >= itemWidthNext){
            startX += itemWidth + _itemMargin;
        }else{
            remainWidth = frame.size.width;
            startX = 0;
            startY += _itemHeight + _itemMargin;
        }
        //
        if(itemWidthNext == 0.000000){
            startY += _itemHeight;
            NSLog(@"等于0 startY=%f",startY);
        }
        [item setTag:i];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemClick:)];
        item.layer.masksToBounds = YES;
        item.userInteractionEnabled = YES;
        [item addGestureRecognizer:tapGesture];
        item.layer.cornerRadius = _borderCornerRadius;
        item.backgroundColor = [UIColor whiteColor];
        item.layer.borderColor = [_unSelectBorderColor CGColor];
        item.layer.borderWidth = _borderWidth;
        [_views addObject:item];
        [self addSubview:item];
    }
    NSLog(@"startY=%f",startY);
    self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, startY);
}

//item点击执行事件
- (void)itemClick:(UITapGestureRecognizer*)recognizer{
    UILabel *view = (UILabel *)recognizer.view;
    NSInteger tag = view.tag;
    if(_preIndex != -1){
        UILabel *preItem = [_views objectAtIndex:_preIndex];
        preItem.layer.borderColor = [_unSelectBorderColor CGColor];
    }
    view.layer.borderColor = [_selectBorderColor CGColor];
    view.layer.borderWidth = _borderWidth;
    _preIndex = tag;
    if(nil != _flowDelegate){
        [_flowDelegate itemClick:tag];
    }
}

- (void)updateConstraints{
    [super updateConstraints];
    NSLog(@"updateConstraints frame=%@",NSStringFromCGRect([self frame]));
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
