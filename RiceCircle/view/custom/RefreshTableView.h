//
//  RefreshTableView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/16.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

//数据刷新block
typedef void(^Refresh)();
//数据加载block
typedef void(^Loading)();

//UITableView的操作类型枚举
typedef NS_ENUM(NSInteger,TableType){
    //只刷新
    REFRESH = 0,
    //只加载
    LOAD = 1,
    //两者都有
    BOTH = 2
};

@interface RefreshTableView : UITableView
//刷新
@property (nonatomic,copy) Refresh refresh;
//加载
@property (nonatomic,copy) Loading loading;
- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style type:(TableType)type;
@end
