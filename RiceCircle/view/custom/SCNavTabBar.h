//
//顶部栏

#import <UIKit/UIKit.h>

@protocol SCNavTabBarDelegate <NSObject>

@optional

- (void)itemDidSelectedWithIndex:(NSInteger)index;
- (void)itemDidSelectedWithIndex:(NSInteger)index withCurrentIndex:(NSInteger)currentIndex;

@end

@interface SCNavTabBar : UIView

@property (nonatomic, weak) id<SCNavTabBarDelegate>delegate;
//当前选择项
@property (nonatomic, assign) NSInteger currentItemIndex;
//标题
@property (nonatomic, strong) NSArray *itemTitles;
//下划线颜色
@property (nonatomic, strong) UIColor *lineColor;
//下划线高度
@property (nonatomic , assign) CGFloat lineHeight;

@property (nonatomic , strong) NSMutableArray *items;
//每一个标题字体大小
@property (nonatomic , assign) CGFloat titleTextSize;
//下划线颜色
@property (nonatomic, strong) UIColor *titleTextColor;
//每一项的高度
@property (nonatomic,assign) CGFloat itemHeight;
//每一项的左右两边的间隔距离
@property (nonatomic,assign) CGFloat itemPadding;
//最右边间隔大小
@property (nonatomic,assign) CGFloat rightPadding;
//是否显示分割线
@property (nonatomic,assign) Boolean showSplitLine;
//分割线的颜色
@property (nonatomic,strong) UIColor *splitLineColor;
//分割线的高度
@property (nonatomic,assign) NSInteger splitLineHeight;
//初始化
- (id)initWithFrame:(CGRect)frame;

//更新数据
- (void)updateData;

//计算高度
- (CGFloat)calcHeight;

@end

