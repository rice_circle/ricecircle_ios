//
//  SCCountTextView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/6.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SCCountTextView.h"
// 添加这个宏定义，以后在使用 Masonry 框架中的属性和方法的时候, 就可以省略mas_前缀
#define MAS_SHORTHAND
// 添加这个宏定义，就可以让 equalTo 方法就等价于 mas_equalTo 方法
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"
#import "SCInputAccessoryView.h"

@interface SCCountTextView()<UITextViewDelegate,AccessoryItemDelegate>

@property (nonatomic,strong) UITextView *contentView;
@property (nonatomic,strong) UILabel *lblCount;
@property (nonatomic,strong) UILabel *lblPlaceholder;
@property (nonatomic,strong) SCInputAccessoryView *inputView;
@property (nonatomic,assign) CGRect originFrame;
@end

@implementation SCCountTextView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setupViews];
    }
    return self;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setupViews];
}

//初始化inputAccessoryView
- (void)initInputView{
    _inputView = [[SCInputAccessoryView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44) withType:Default];
    _inputView.accessoryDelegate = self;
    [_inputView buildViews];
}


//初始化数据
- (void)initData{
    _showCount = NO;
    _placeHolderTextColor = [UIColor lightGrayColor];
    _placeHolderTextSize = [UIFont systemFontOfSize:14];
    _countTextSize = [UIFont systemFontOfSize:14];
    _countTextColor = [UIColor lightGrayColor];
    _totalCount = 0;
}

//构建view布局
- (void)setupViews{
    [self initData];
    [self initInputView];
    //创建UITextView
    _contentView = [[UITextView alloc] init];
    _contentView.delegate = self;
    _contentView.inputAccessoryView = _inputView;
    [self addSubview:_contentView];
    
    //创建默认文案UILabel
    _lblPlaceholder = [[UILabel alloc] init];
    _lblPlaceholder.textColor = _placeHolderTextColor;
    _lblPlaceholder.text = _placeHolder;
    _lblPlaceholder.font = _placeHolderTextSize;
    [self addSubview:_lblPlaceholder];
    
    //计数器
    _lblCount = [[UILabel alloc] init];
    _lblCount.textColor = _countTextColor;
    _lblCount.font = _countTextSize;
    _lblCount.text = [NSString stringWithFormat:@"%d/%lu",0,_totalCount];
    [self addSubview:_lblCount];
    _lblCount.hidden = !_showCount;
    
    [_contentView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self);
    }];
    
    [_lblPlaceholder makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView.left).offset(5);
        make.top.equalTo(_contentView.top).offset(10);
        make.height.equalTo(10);
    }];
    
    [_lblCount makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_contentView.right).offset(-5);
        make.bottom.equalTo(_contentView.bottom).offset(-5);
        make.height.equalTo(10);
    }];
}

- (void)setPlaceHolder:(NSString *)placeHolder{
    _placeHolder = placeHolder;
    _lblPlaceholder.text = placeHolder;
}

- (void)setPlaceHolderTextSize:(UIFont *)placeHolderTextSize{
    _placeHolderTextSize = placeHolderTextSize;
    _lblPlaceholder.font = placeHolderTextSize;
}

- (void)setPlaceHolderTextColor:(UIColor *)placeHolderTextColor{
    _placeHolderTextColor = placeHolderTextColor;
    _lblPlaceholder.textColor = placeHolderTextColor;
}

- (void)setCountTextSize:(UIFont *)countTextSize{
    _countTextSize = countTextSize;
    _lblCount.font = countTextSize;
}

- (void)setCountTextColor:(UIColor *)countTextColor {
    _countTextColor = countTextColor;
    _lblCount.textColor = countTextColor;
}

- (void)setShowCount:(BOOL)showCount{
    _showCount = showCount;
    _lblCount.hidden = !showCount;
}

- (void)setTotalCount:(NSInteger)totalCount{
    _totalCount = totalCount;
    _lblCount.text = [NSString stringWithFormat:@"%d/%lu",0,_totalCount];
}

/**
 改变计数器显示位置
 */
-(void)transformCountView:(CGFloat)startY withDuration:(double)duration{
    if(!_needTranslateCount){
        return;
    }
    // textfield的位置
    CGFloat viewY = [self screenViewYValue:_lblCount];
    CGFloat frameBottom = self.frame.origin.y + self.frame.size.height;
    NSLog(@"startY= %f,frameBottom=%f,duration=%f",startY,frameBottom,duration);
    if(startY >= frameBottom){
        return;
    }
    CGFloat y = frameBottom - startY + 69;
    [_lblCount updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_contentView.bottom).offset(-y);
    }];
    [UIView animateWithDuration:duration animations:^{
        [self layoutIfNeeded];
        //NSLog(@"rect.origin.y=%f",rect.origin.y);
    }];
}


/**
 还原计数器位置
 */
- (void)resetCountView{
    [_lblCount updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_contentView.bottom).offset(-5);
    }];
    [UIView animateWithDuration:0.250000 animations:^{
        [self layoutIfNeeded];
    }];
}

//获取UITextField在界面上的y轴位置
- (CGFloat)screenViewYValue:(UIView *)textfield {
    CGFloat y = 0;
    for (UIView *view = textfield; view; view = view.superview) {
        y += view.frame.origin.y;
        if ([view isKindOfClass:[UIScrollView class]]) {
            // 如果父视图是UIScrollView则要去掉内容滚动的距离
            UIScrollView* scrollView = (UIScrollView*)view;
            y -= scrollView.contentOffset.y;
        }
    }
    return y;
}

//键盘销毁
- (void)keyboardWillHide:(NSNotification *)notification{
    // 键盘的Y值
    NSDictionary *userInfo = [notification userInfo];
    // 动画
    NSNumber *duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:duration.doubleValue animations:^{
        self.lblCount.frame = _originFrame;
    }];
}

/**
 获取内容
 
 @return 编辑框中的内容
 */
- (NSString *)obtainContent{
    return _contentView.text;
}

/**
 设置内容
 
 @param content 内容
 */
- (void)setContent:(NSString*)content{
    _contentView.text = content;
}

#pragma mark UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView{
    //NSLog(@"textViewDidChange len=%lu _showCount=%d",[textView.text length],_showCount);
    NSLog(@"lblCount.frame=%@, self.frame=%@,height=%f",NSStringFromCGRect(_lblCount.frame),NSStringFromCGRect(self.frame),SCREEN_HEIGHT);
    if([textView.text length] == 0){
        _lblPlaceholder.alpha = 1;
    }else{
        _lblPlaceholder.alpha = 0;//这里给空
    }
    //计算剩余字数   不需要的也可不写
    if(_showCount){
        NSString *content = textView.text;
        NSInteger existTextNum = [content length];
        NSInteger remainTextNum = _totalCount - existTextNum;
        _lblCount.text = [NSString stringWithFormat:@"%lu/%lu",existTextNum,_totalCount];
    }
}

//设置超出最大字数 即不可输入 也是textview的代理方法
-(BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range
    replacementText:(NSString*)text{
    //这里"\n"对应的是键盘的 return 回收键盘之用
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return YES;
    }
    if(_totalCount == 0){
        return YES;
    }
    if (range.location >= _totalCount){
        return NO;
    }else {
        return YES;
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    NSLog(@"textViewDidBeginEditing");
    _originFrame = _lblCount.frame;
}

#pragma mark AccessoryItemDelegate

- (void)accessoryItemClick:(NSInteger)index{
    [self endEditing:YES];
    [self resetCountView];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
