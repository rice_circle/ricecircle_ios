//
//顶部栏
//


#define SCREENW  ([UIScreen mainScreen].bounds.size.width)


#import "SCNavTabBar.h"
#import "NSString+Extension.h"


@interface SCNavTabBar (){
    UIScrollView *_navgationTabBar;
    UIView *_line;                 // underscore show which item selected
    NSArray *_itemsWidth;           // an array of items' width
}
@end

@implementation SCNavTabBar

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self initData];
        [self initConfig];
    }
    return self;
}

//初始化数据,设置下划线默认高度和颜色 等
- (void)initData{
    _lineColor = [UIColor redColor];
    _lineHeight = 1.5f;
    _itemHeight = 40;
    _itemPadding = 15;
    _titleTextSize = 14;
    _titleTextColor = [UIColor blackColor];
    _rightPadding = 0;
    _splitLineColor = [UIColor grayColor];
    _splitLineHeight = 1;
    _showSplitLine = YES;
}

- (void)initConfig{
    _items = [@[] mutableCopy];
    [self viewConfig];
}

- (void)viewConfig{
    _navgationTabBar = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREENW, _itemHeight)];
    _navgationTabBar.backgroundColor = [UIColor clearColor];
    _navgationTabBar.showsHorizontalScrollIndicator = NO;
    [self addSubview:_navgationTabBar];
}

//更新
- (void)updateData{
    //重新设置_navgationTabBar的位置
    CGRect rect = _navgationTabBar.frame;
    rect.size.height = _itemHeight + _lineHeight + ( !_showSplitLine ? 0 : _splitLineHeight);
    _navgationTabBar.frame = rect;
    
//    CGRect contentRect = self.bounds;
//    rect.size.height = _itemHeight + _lineHeight + _splitLineHeight;
//    self.bounds = contentRect;
    [self addSplitLine];
    //设置内容宽度
    _itemsWidth = [self getButtonsWidthWithTitles:_itemTitles];
    if (_itemsWidth.count){
        CGFloat contentWidth = [self contentWidthAndAddNavTabBarItemsWithButtonsWidth:_itemsWidth];
        _navgationTabBar.contentSize = CGSizeMake(contentWidth, 0);
    }
}

//计算高度
- (CGFloat)calcHeight{
    return _itemHeight + _lineHeight + ( !_showSplitLine ? 0 : _splitLineHeight);
}

//添加分割线
- (void)addSplitLine{
    if(!_showSplitLine){
        return;
    }
    CGFloat maxWidth = SCREEN_WIDTH;
    UIView *splitLine = [[UIView alloc] initWithFrame:CGRectMake(0, _itemHeight + _lineHeight, maxWidth, _splitLineHeight)];
    splitLine.backgroundColor = _splitLineColor;
    [self addSubview:splitLine];
}

//设置每一项的大小、颜色等
- (CGFloat)contentWidthAndAddNavTabBarItemsWithButtonsWidth:(NSArray *)widths{
    CGFloat maxWidth = SCREEN_WIDTH - _rightPadding;
    //所有item字体总长度
    CGFloat totalWidth = 0;
    for (NSInteger i = 0; i < [widths count]; i++) {
        totalWidth += [[widths objectAtIndex:i] floatValue];
    }
    //计算_navgationTabBar的可视宽度与字体总宽度的差值，得到差值除以item的个数，如果得到的
    //值比_itemPadding要大，就把得到的值赋值给_itemPadding
    CGFloat difference = maxWidth - totalWidth;
    CGFloat value = difference / (widths.count * 2.0f);
    NSLog(@"value=%f difference=%f, _itemPadding = %f",value,difference,_itemPadding);
    _itemPadding = value > _itemPadding ? value : _itemPadding;
    NSLog(@"_itemPadding = %f",_itemPadding);
    CGFloat buttonX = 0;
    for (NSInteger index = 0; index < [_itemTitles count]; index++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:_itemTitles[index] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:_titleTextSize];
        CGSize textMaxSize = CGSizeMake(SCREENW, MAXFLOAT);
        CGSize textRealSize = [_itemTitles[index] sizeWithFont:[UIFont systemFontOfSize:_titleTextSize] maxSize:textMaxSize].size;

        textRealSize = CGSizeMake(textRealSize.width + _itemPadding * 2, _itemHeight);
        button.frame = CGRectMake(buttonX, 0,textRealSize.width, _itemHeight);
        
        //字体颜色
        [button setTitleColor:_titleTextColor forState:UIControlStateNormal];

        [button addTarget:self action:@selector(itemPressed:type:) forControlEvents:UIControlEventTouchUpInside];
        [_navgationTabBar addSubview:button];
        [_items addObject:button];
        buttonX += button.frame.size.width;
    }
    NSLog(@"buttonX = %f,maxWidth=%f",buttonX,maxWidth);
    [self showLineWithButtonWidth:[widths[0] floatValue]];
    return buttonX;
}

#pragma mark  下划线
- (void)showLineWithButtonWidth:(CGFloat)width{
    //第一个线的位置
    _line = [[UIView alloc] initWithFrame:CGRectMake(_itemPadding, _itemHeight, width, _lineHeight)];
    _line.backgroundColor = [UIColor redColor];
    [_navgationTabBar addSubview:_line];
    
    UIButton *btn = _items[0];
    [self itemPressed:btn type:0];
}

- (void)itemPressed:(UIButton *)button type:(int)type{
    NSLog(@"itemPressed type=%d",type);
    NSInteger index = [_items indexOfObject:button];
    //[self setCurrentItemIndex:index];
    [_delegate itemDidSelectedWithIndex:index withCurrentIndex:_currentItemIndex];
}

//计算数组内字体的宽度
- (NSArray *)getButtonsWidthWithTitles:(NSArray *)titles;{
    NSMutableArray *widths = [@[] mutableCopy];
    for (NSString *title in titles){
        CGSize textMaxSize = CGSizeMake(SCREENW, MAXFLOAT);
        CGSize textRealSize = [title sizeWithFont:[UIFont systemFontOfSize:_titleTextSize] maxSize:textMaxSize].size;
       
        NSNumber *width = [NSNumber numberWithFloat:textRealSize.width];
        [widths addObject:width];
    }
    return widths;
}

#pragma mark 偏移
- (void)setCurrentItemIndex:(NSInteger)currentItemIndex{
    _currentItemIndex = currentItemIndex;
    UIButton *button = _items[currentItemIndex];

    CGFloat flag = SCREENW - _rightPadding;
    
    if (button.frame.origin.x + button.frame.size.width + 50 >= flag){
        CGFloat offsetX = button.frame.origin.x + button.frame.size.width - flag;
        if (_currentItemIndex < [_itemTitles count]-1){
            offsetX = offsetX + button.frame.size.width;
        }
        [_navgationTabBar setContentOffset:CGPointMake(offsetX, 0) animated:YES];
        
    }else{
        [_navgationTabBar setContentOffset:CGPointMake(0, 0) animated:YES];
    }
       //下划线的偏移量
    [UIView animateWithDuration:0.1f animations:^{
        _line.frame = CGRectMake(button.frame.origin.x + _itemPadding, _line.frame.origin.y, [_itemsWidth[currentItemIndex] floatValue], _line.frame.size.height);
    }];
}
@end
