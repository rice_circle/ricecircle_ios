//
//  PhotoShowView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/19.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PhotoItemDelegate <NSObject>

//item点击回调事件
- (void)itemClick:(NSInteger)index;
@end

@interface PhotoShowView : UIScrollView
//每一个item的间隔
@property (nonatomic,assign) CGFloat itemPadding;
//每一个item的大小
@property (nonatomic,assign) CGFloat itemSize;

@property (nonatomic,weak) id<PhotoItemDelegate> itemDelegate;

- (void)setData:(NSArray *)data;
@end
