//
//  TabIndicatorView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/21.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "TabIndicatorView.h"
#import "NSString+Extension.h"
#import "UIColor+Hex.h"

@interface TabIndicatorView()
//前一个点击item序号
@property (nonatomic,assign) NSInteger preIndex;
@property (nonatomic,strong) NSMutableArray *views;
@end

@implementation TabIndicatorView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self initData];
}

//初始化数据
- (void)initData{
    _cornerRadius = -1;
    _itemWidth = -1;
    _itemHeight = -1;
    _itemPadding = -1;
    _indicatorColor = [UIColor colorWithHexString:@"#f25f72"];
    _preIndex = 0;
    _views = [NSMutableArray array];
    _fontSize = 16;
    _fontColor = [UIColor colorWithHexString:@"#333333"];
    _needCorner = NO;
}


/**
 设置数据

 @param dataSet 数据源
 */
- (void)setDataSet:(NSArray *)dataSet{
    _dataSet = dataSet;
    [self updateData:dataSet];
    if(_needCorner){
        self.layer.cornerRadius = _cornerRadius;
    }
    CGFloat startX = 0;
    CGSize textMaxSize = CGSizeMake(SCREEN_WIDTH, MAXFLOAT);
    for (NSInteger i = 0; i < dataSet.count; i ++ ) {
        CGSize textRealSize = [dataSet[i] sizeWithFont:[UIFont systemFontOfSize:_fontSize] maxSize:textMaxSize].size;
        UILabel *lblValue = [[UILabel alloc] initWithFrame:CGRectMake(startX, 0, textRealSize.width + _itemPadding, _itemHeight)];
        lblValue.textAlignment = NSTextAlignmentCenter;
        lblValue.layer.cornerRadius = _cornerRadius;
        lblValue.layer.masksToBounds = YES;
        if(_preIndex == i){
            lblValue.backgroundColor = _indicatorColor;
        }
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemClick:)];
        lblValue.userInteractionEnabled = YES;
        lblValue.tag = i;
        lblValue.font = [UIFont systemFontOfSize:_fontSize];
        lblValue.text = dataSet[i];
        lblValue.textColor = _fontColor;
        [lblValue addGestureRecognizer:tap];
        [self addSubview:lblValue];
        [_views addObject:lblValue];
        startX += textRealSize.width + _itemPadding;
    }
}

- (void)itemClick:(UITapGestureRecognizer*)recognizer {
    UILabel *view = (UILabel *)recognizer.view;
    NSInteger tag = view.tag;
    NSLog(@"itemClick tag=%lu",tag);
    if(_preIndex != -1){
        UILabel *preView = [_views objectAtIndex:_preIndex];
        preView.backgroundColor = [UIColor clearColor];
    }
    view.backgroundColor = _indicatorColor;
    if(nil != _indicatorDelegate){
        [_indicatorDelegate indicatorItemClick:(tag + 1)];
    }
    _preIndex = tag;
}

- (void)setCurrentIndex:(NSInteger)currentIndex{
    if(currentIndex == -1){
        return;
    }
    _currentIndex = currentIndex;
    UILabel *view = [_views objectAtIndex:currentIndex];
    NSInteger tag = view.tag;
    if(_preIndex != -1){
        UILabel *preView = [_views objectAtIndex:_preIndex];
        preView.backgroundColor = [UIColor clearColor];
    }
    view.backgroundColor = _indicatorColor;
    if(nil != _indicatorDelegate){
        //[_indicatorDelegate indicatorItemClick:(tag + 1)];
    }
    _preIndex = tag;
}

/**
 更新数据

 @param dataSet 数据源
 */
- (void)updateData:(NSArray *)dataSet{
    CGRect frame = [self frame];
    NSLog(@"TabIndicatorView frame=%@,screen_widht=%f,screen_height=%f",NSStringFromCGRect(frame),SCREEN_WIDTH,SCREEN_HEIGHT);
    if(_cornerRadius == -1){
        _cornerRadius = frame.size.height / 2.0f;
    }
    if(_itemPadding == -1){
        CGSize textMaxSize = CGSizeMake(SCREEN_WIDTH, MAXFLOAT);
        CGFloat totalWidth = 0;
        for (NSInteger i = 0 ; i < dataSet.count; i ++) {
            NSString *content = [dataSet objectAtIndex:i];
            CGSize textRealSize = [content sizeWithFont:[UIFont systemFontOfSize:_fontSize] maxSize:textMaxSize].size;
            totalWidth += textRealSize.width;
        }
        CGFloat remainderSize = frame.size.width - totalWidth;
        if(remainderSize >= 0){
            _itemPadding = remainderSize / (dataSet.count * 1.0f);
        }
    }
    _itemPadding = _itemPadding == -1 ? 20 : _itemPadding;
    if(_itemHeight == -1){
        _itemHeight = frame.size.height;
    }
    NSLog(@"updateData _cornerRadius=%f,_itemPadding=%f,_itemHeight=%f",_cornerRadius,_itemPadding,_itemHeight);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
