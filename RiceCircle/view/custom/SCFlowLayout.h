//
//  SCFlowLayout.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/24.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 布局方向

 - HORIZONTAL: 水平布局
 - VERTICAL: 垂直布局
 */
typedef NS_ENUM(NSInteger,FlowDirection){
    HORIZONTAL,
    VERTICAL
};

@protocol FlowItemDelegate <NSObject>

- (void)itemClick:(NSInteger)index;

@end

@interface SCFlowLayout : UIView
//布局方向
@property (nonatomic,assign) FlowDirection direction;
//每一项的内容的padding距离
@property (nonatomic,assign) CGFloat itemPadding;
//每一项的间隔距离
@property (nonatomic,assign) CGFloat itemMargin;
//字体大小
@property (nonatomic,assign) CGFloat fontSize;
//每一item的高度
@property (nonatomic,assign) CGFloat itemHeight;
//每一个item对象点击事件代理
@property (nonatomic,weak) id<FlowItemDelegate> flowDelegate;
//选中边框颜色
@property (nonatomic,strong) UIColor *selectBorderColor;
//未选中边框颜色
@property (nonatomic,strong) UIColor *unSelectBorderColor;
//变宽角度
@property (nonatomic,assign) CGFloat borderCornerRadius;
//边框宽度
@property (nonatomic,assign) CGFloat borderWidth;

- (void)setData:(NSArray*)dataSet;
@end
