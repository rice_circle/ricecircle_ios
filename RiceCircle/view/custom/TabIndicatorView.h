//
//  TabIndicatorView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/21.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TabIndicatroItemDelegate <NSObject>

- (void)indicatorItemClick:(NSInteger)index;

@end

@interface TabIndicatorView : UIView
//数据集
@property (nonatomic,strong) NSArray *dataSet;
//圆弧角度
@property (nonatomic,assign) CGFloat cornerRadius;
//每一项的宽度
@property (nonatomic,assign) CGFloat itemWidth;
//每一项的高度
@property (nonatomic,assign) CGFloat itemHeight;
//每一项之间的间隔距离
@property (nonatomic,assign) CGFloat itemPadding;
//字体大小
@property (nonatomic,assign) CGFloat fontSize;
//指示器颜色
@property (nonatomic,strong) UIColor *indicatorColor;
//字体颜色
@property (nonatomic,strong) UIColor *fontColor;
//背景需要圆弧
@property (nonatomic,assign) BOOL needCorner;
//item点击代理
@property (nonatomic,weak) id<TabIndicatroItemDelegate> indicatorDelegate;
//当前选中位置
@property (nonatomic,assign) NSInteger currentIndex;
/**
 设置数据
 
 @param dataSet 数据源
 */
- (void)setDataSet:(NSArray *)dataSet;
@end
