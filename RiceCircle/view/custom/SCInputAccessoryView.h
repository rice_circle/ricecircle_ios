//
//  SCInputAccessoryView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/3.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

//显示类型
typedef NS_ENUM(NSInteger,ShowType){
    Default = 1,
    Operable = 2,
    OneItem = 3
};

//每一个item点击的代理
@protocol AccessoryItemDelegate <NSObject>

- (void)accessoryItemClick:(NSInteger)index;

@end

@interface SCInputAccessoryView : UIToolbar
//显示类型
@property (nonatomic,assign) ShowType showType;
//代理
@property (nonatomic,weak) id<AccessoryItemDelegate> accessoryDelegate;


/**
 初始化

 @param frame 大小
 @param type 显示类型
 @return 返回当前对象实例
 */
- (instancetype)initWithFrame:(CGRect)frame withType:(ShowType)type;

/**
 添加子item
 
 @param items 子item
 @param textItem 子item是文字还是图片
 */
- (void)addItem:(NSMutableArray*)items isTextItem:(BOOL)textItem;

/**
 构建view
 */
- (void)buildViews;
@end
