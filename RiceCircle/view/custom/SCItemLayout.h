//
//  SCItemLayout.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ItemType){
    Readonly = 0,
    Arrow = 1,
    Edit = 2
};

@interface SCItemLayout : UIView
//类型
@property (nonatomic,assign) ItemType itemType;
//需要分割线
@property (nonatomic,assign) BOOL needSplit;


/**
 设置名称

 @param title 名称
 */
- (void)setTitle:(NSString *)title;

/**
 设置内容
 
 @param content 内容
 */
- (void)setContent:(NSString *)content;
- (UITextField*)obtainTextField;
@end
