//  自定义进度条
//  SCProgressView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/17.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SCProgressView.h"
#import "UIColor+Hex.h"

@implementation SCProgressView

+ (instancetype)initProgress{
    return [[self alloc] init];
}

- (instancetype)init{
    self = [super init];
    if(self){
        NSLog(@"init");
        _progress = 0;
        _progressBGColor = [UIColor colorWithHexString:@"#eeeeee"];
        _progressTintColor = [UIColor colorWithHexString:@"#f25f72"];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    NSLog(@"initWithCoder");
    if(self){
        _progress = 0;
        _progressBGColor = [UIColor colorWithHexString:@"#eeeeee"];
        _progressTintColor = [UIColor colorWithHexString:@"#f25f72"];
    }
    return self;
}

- (void)setFrame:(CGRect)frame{
    NSLog(@"setFrame ");
    _radius = frame.size.height / 2.0f;
    [super setFrame:frame];
}

/**/
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    NSLog(@"setFrame _progress=%f",_progress);
    //获取ctx
    CGContextRef context = UIGraphicsGetCurrentContext();
    ///////1、先绘制背景色
    //设置笔触颜色(设置颜色有很多方法，我觉得这个方法最好用)
    CGContextSetStrokeColorWithColor(context, _progressBGColor.CGColor);
    //设置填充色
    CGContextSetFillColorWithColor(context, _progressBGColor.CGColor);
    [self drawCircle:context rect:rect];
    
    /////2、绘制进度
    //设置笔触颜色(设置颜色有很多方法，我觉得这个方法最好用)
    CGContextSetStrokeColorWithColor(context, _progressTintColor.CGColor);
    //设置填充色
    CGContextSetFillColorWithColor(context, _progressTintColor.CGColor);
    float width = rect.size.width * (_progress > 1 ? 1 : _progress);
    CGRect progressRect = CGRectMake(rect.origin.x, rect.origin.y, width, rect.size.height);
    [self drawCircle:context rect:progressRect];
    
    //////3、绘制文字
    [self drawText:context rect:rect];
}

- (void)drawText:(CGContextRef)context rect:(CGRect)rect{
    //文字样式
    UIFont *font = [UIFont systemFontOfSize:10];
    NSDictionary *dict = @{NSFontAttributeName:font,
                           NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#ffffff"]};
    NSString *content = [self notRounding:_progress*100 afterPoint:2];
    NSString *c = [content stringByAppendingString:@"%"];
    CGRect textRect = [self getSizeWithStr:c fontSize:10];
    NSLog(@"drawText rect = %@,c=%@",NSStringFromCGRect(rect),c);
    float startX = (rect.size.width - textRect.size.width ) / 2.0f;
    float startY = (rect.size.height - textRect.size.height ) / 2.0f;
    [c drawInRect:CGRectMake(startX , startY, textRect.size.width, textRect.size.height) withAttributes:dict];
}

//获取字符串的长度
-(CGRect )getSizeWithStr:(NSString *)str fontSize:(float)fontSize{
    UIFont *font = [UIFont boldSystemFontOfSize:fontSize];
    return [str boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName, nil] context:nil];
}

//float按照给定位数取值
-(NSString *)notRounding:(float)price afterPoint:(int)position{
    NSDecimalNumberHandler* roundingBehavior = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown scale:position raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *ouncesDecimal;
    
    NSDecimalNumber *roundedOunces;
    
    ouncesDecimal = [[NSDecimalNumber alloc] initWithFloat:price];
    
    roundedOunces = [ouncesDecimal decimalNumberByRoundingAccordingToBehavior:roundingBehavior];
    return [NSString stringWithFormat:@"%@",roundedOunces];
}  


//绘制椭圆
- (void)drawCircle:(CGContextRef)context rect:(CGRect)rect{
    //设置线条样式
    CGContextSetLineCap(context, kCGLineCapRound);
    //设置线条粗细宽度
    CGContextSetLineWidth(context, 0.5);
    
    float width = rect.size.width;
    float height = rect.size.height;
    // 移动到初始点
    CGContextMoveToPoint(context, _radius, 0);
    // 绘制第1条线和第1个1/4圆弧
    CGContextAddLineToPoint(context, width - _radius, 0);
    CGContextAddArc(context, width - _radius, _radius, _radius, -0.5 * M_PI, 0.0, 0);
    // 绘制第2条线和第2个1/4圆弧
    CGContextAddLineToPoint(context, width, height - _radius);
    CGContextAddArc(context, width - _radius, height - _radius, _radius, 0.0, 0.5 * M_PI, 0);
    
    // 绘制第3条线和第3个1/4圆弧
    CGContextAddLineToPoint(context, _radius, height);
    CGContextAddArc(context, _radius, height - _radius, _radius, 0.5 * M_PI, M_PI, 0);
    
    // 绘制第4条线和第4个1/4圆弧
    CGContextAddLineToPoint(context, 0, _radius);
    CGContextAddArc(context, _radius, _radius, _radius, M_PI, 1.5 * M_PI, 0);
    
    // 闭合路径
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
}

//绘制进度
- (void)drawProgress:(CGContextRef)context rect:(CGRect)rect{
    
}
@end
