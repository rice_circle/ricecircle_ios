//
//  AreaPickerView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/5.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AreaPickerDelagate <NSObject>


/**
 取消
 */
- (void)cancelAreaPicker;


/**
 选择完成

 @param content 选择的内容
 */
- (void)selectComplete:(NSString*)content;

@end

@interface AreaPickerView : UIView
@property (nonatomic,strong) id<AreaPickerDelagate> delegate;
@end
