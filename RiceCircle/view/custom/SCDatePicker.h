//
//  SCDatePicker.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/7.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

//取消block
typedef void(^CancelDatePicker)(void);

//确定block
typedef void(^SureDatePicker)(NSString* content);

@interface SCDatePicker : UIView

@property (nonatomic,copy) CancelDatePicker cancelBlock;

@property (nonatomic,copy) SureDatePicker sureBlock;

@end
