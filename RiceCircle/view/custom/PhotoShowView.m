//  头像展示view
//  PhotoShowView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/19.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "PhotoShowView.h"

@implementation PhotoShowView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self initData];
    }
    return self;
}

- (void)initData{
    _itemPadding = 10.0f;
    _itemSize = 40.0f;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    NSLog(@"awakeFromNib");
    [self initData];
}

- (void)setData:(NSArray *)data{
    NSLog(@"PhotoShowView setData");
    [self updateViews:data];
}

- (void)updateViews:(NSArray *)data{
    NSLog(@"updateViews _itemSize=%f _itemPadding=%f",_itemSize,_itemPadding);
    CGFloat startX = 0;
    for (NSInteger i = 0; i < 9; i++) {
        UIImageView *item = [[UIImageView alloc] initWithFrame:CGRectMake(startX, 0, _itemSize, _itemSize)];
        item.image = [UIImage imageNamed:@"head"];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemClick:)];
        item.userInteractionEnabled = YES;
        [item addGestureRecognizer:tap];
        item.tag = i;
        [self addSubview:item];
        startX += _itemSize + _itemPadding;
        NSLog(@"startX=%f",startX);
    }
    startX += _itemSize + _itemPadding;
    CGRect bounds = self.bounds;
    bounds.size.height = _itemSize;
    self.bounds = bounds;
    self.contentSize = CGSizeMake(startX, 0);
}

- (void)itemClick:(UITapGestureRecognizer*)recognizer {
    UIView *view = (UIView *)recognizer.view;
    NSInteger tag = view.tag;
    NSLog(@"itemClick tag=%lu",tag);
    if(nil != _itemDelegate){
        [_itemDelegate itemClick:(tag + 1)];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
