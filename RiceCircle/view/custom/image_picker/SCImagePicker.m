//  图片选择
//  SCImagePicker.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/9.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SCImagePicker.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "VPImageCropperViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import "SCAlert.h"

#define ORIGINAL_MAX_WIDTH 640.0f

@interface SCImagePicker()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,VPImageCropperDelegate>
@property (nonatomic,strong) UIAlertController *alertVC;
@property (nonatomic,strong) UIActionSheet *alertSheet;
@property (nonatomic,strong) UIViewController *viewController;
//标题
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) UIImagePickerController *cameraPC;
@property (nonatomic,strong) UIImagePickerController *galleryPC;
//文件路径
@property (nonatomic,strong) NSData *fileData;
@end

@implementation SCImagePicker

- (instancetype)initWithController:(UIViewController*)controller withTitle:(NSString*)title{
    self = [super init];
    if(self){
        _viewController = controller;
        _title = title;
        [self initPicker];
    }
    return self;
}

//初始化picker
- (void)initPicker{
    if(IOS_VERSION_8_3_OR_LATER){
        NSLog(@"initPicker 8.3");
        _alertVC = [UIAlertController alertControllerWithTitle:_title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self selectFromCamera];
        }];
        UIAlertAction *galleryAction = [UIAlertAction actionWithTitle:@"从图库选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self selectFromGallery];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [_alertVC addAction:cameraAction];
        [_alertVC addAction:galleryAction];
        [_alertVC addAction:cancelAction];
        return;
    }
    _alertSheet = [[UIActionSheet alloc] initWithTitle:_title delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从图库选择", nil];
}

//显示选择框
- (void)showPicker{
    if(IOS_VERSION_8_3_OR_LATER){
        if(_viewController){
            [_viewController presentViewController:_alertVC animated:YES completion:^{
                
            }];
        }
        return;
    }
    if(_viewController && _alertSheet){
        [_alertSheet showInView:_viewController.view];
    }
}

//拍照
- (void)selectFromCamera{
    if(!_viewController){
        return;
    }
    //先判断是否授予了摄像头使用权限
    if(![self isAuthorizationCamera]){
        NSString *message = @"应用相机权限受限,请在iPhone的“设置-隐私-相机”选项中，允许饭圈访问你的相机。";
        [[SCAlert sharedSCAlert] showSingleButtonAlert:@"温馨提示" withMessage:message withButtonText:@"确定" withButtonBlock:^{
            
        } withParent:_viewController];
        return;
    }
    
    if (![self isCameraAvailable] || ![self doesCameraSupportTakingPhotos]){
        NSString *message = @"您的iPhone相机不能使用。";
        [[SCAlert sharedSCAlert] showSingleButtonAlert:@"温馨提示" withMessage:message withButtonText:@"确定" withButtonBlock:^{
            
        } withParent:_viewController];
        return;
    }
    _cameraPC = [[UIImagePickerController alloc] init];
    _cameraPC.delegate = self;
    _cameraPC.allowsEditing = YES;
    _cameraPC.sourceType = UIImagePickerControllerSourceTypeCamera;
    [_viewController presentViewController:_cameraPC animated:YES completion:^{
        
    }];
}

//判断手机是否对app摄像头功能授权了,9.0以前使用ALAuthorizationStatus判断，9.0以后使用AVAuthorizationStatus判断
- (BOOL)isAuthorizationCamera{
    if(IOS_VERSION_9_OR_LATER){
        AVAuthorizationStatus status = [AVCaptureDevice  authorizationStatusForMediaType:AVMediaTypeVideo];
        if (status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted) {
            // 没有授权访问摄像头
            return NO;
        }
        return YES;
    }else{
        ALAuthorizationStatus author =[ALAssetsLibrary authorizationStatus];
        if (author == ALAuthorizationStatusRestricted || author ==ALAuthorizationStatusDenied){
            return NO;
        }
        return YES;
    }
}

//判断手机是否对app图库使用功能授权了,9.0以前使用ALAuthorizationStatus判断，9.0以后使用PHAuthorizationStatus判断
- (BOOL)isAuthorizationGalley{
    if(IOS_VERSION_9_OR_LATER){
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if (status == PHAuthorizationStatusDenied || status == PHAuthorizationStatusRestricted) {
            // 没有授权访问相册
            return NO;
        }
        return YES;
    }
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    if (status == ALAuthorizationStatusDenied || status == ALAuthorizationStatusRestricted) {
        // 没有授权访问相册
        return NO;
    }
    return YES;
}

//从图库选择
- (void)selectFromGallery{
    if(!_viewController){
        return;
    }
    if (![self isPhotoLibraryAvailable]) {
        NSString *message = @"您的iPhone不支持图片浏览。";
        [[SCAlert sharedSCAlert] showSingleButtonAlert:@"温馨提示" withMessage:message withButtonText:@"确定" withButtonBlock:^{
            
        } withParent:_viewController];
        return;
    }
    _galleryPC = [[UIImagePickerController alloc]init];
    _galleryPC.delegate = self;
    _galleryPC.allowsEditing = YES;
    _galleryPC.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [_viewController presentViewController:_galleryPC animated:YES completion:^{
        
    }];
}

//图片缩放
- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    if (sourceImage.size.width < ORIGINAL_MAX_WIDTH){
        return sourceImage;
    }
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    if (sourceImage.size.width > sourceImage.size.height) {
        btHeight = ORIGINAL_MAX_WIDTH;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_WIDTH / sourceImage.size.height);
    } else {
        btWidth = ORIGINAL_MAX_WIDTH;
        btHeight = sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
    }
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}

- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if (widthFactor < heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    if ([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)kUTTypeImage]) {
        UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
        //UIImage *portraitImg = [self imageByScalingToMaxSize:img];
        CGFloat cropWidth = _viewController.view.frame.size.width;
        CGFloat cropHeight = _viewController.view.frame.size.height;
        // 裁剪
        //VPImageCropperViewController *imgEditorVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(0, 64, cropWidth, cropHeight) limitScaleRatio:3.0];
        //imgEditorVC.delegate = self;
        //[_viewController presentViewController:imgEditorVC animated:YES completion:^{
            // TO DO
        //}];
        if(nil != _delegate){
            [_delegate imagePickerFinish:img];
        }
    } else if ([[info objectForKey:UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)kUTTypeMovie]) {
        NSString *videoPath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
        self.fileData = [NSData dataWithContentsOfFile:videoPath];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self selectFromCamera];
            break;
            
        case 1:
            [self selectFromGallery];
            break;
            
        default:
            break;
    }
}

#pragma mark VPImageCropperDelegate
//图片裁剪完成回调
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage{
    if(_delegate){
        [_delegate imagePickerFinish:editedImage];
    }
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
    }];
}

//裁剪取消回调
- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController{
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
    }];
}

#pragma mark camera utility
//是否支持摄像头
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

//前置摄像头是否可用
- (BOOL) isRearCameraAvailable{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

//后置摄像头是否可用
- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

//是否支持图库浏览
- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}

//是否支持从图库选择视频
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

//是否支持从图库选择照片
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}


@end
