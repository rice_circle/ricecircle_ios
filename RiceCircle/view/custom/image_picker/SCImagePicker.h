//
//  SCImagePicker.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/9.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol SCImagePickerDelegate <NSObject>

@optional

- (void)imagePickerFinish:(UIImage*)image;

@end

@interface SCImagePicker : NSObject
//选择的图片的宽度
@property (nonatomic,assign) CGFloat resutlImageWidth;
//选择的图片的高度
@property (nonatomic,assign) CGFloat resutlImageHeight;

@property (nonatomic,weak) id<SCImagePickerDelegate> delegate;
- (instancetype)initWithController:(UIViewController*)controller withTitle:(NSString*)title;

//显示选择框
- (void)showPicker;
@end
