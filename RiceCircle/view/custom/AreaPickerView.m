//  地区选择器
//  AreaPickerView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/5.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "AreaPickerView.h"
#import "ProvinceModel.h"
#import "CityModel.h"
#import "AreaModel.h"

@interface AreaPickerView()<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic,strong) UIPickerView *pickerView;
@property (nonatomic,strong) NSMutableDictionary *dataSource;
@property (nonatomic,strong) NSMutableArray *cityArray;
@property (nonatomic,strong) NSDictionary *areaDic;
@property (nonatomic,strong) NSMutableArray *provinceArr;
@property (nonatomic,strong) NSString *content;
@end

@implementation AreaPickerView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self loading];
    }
    return self;
}

- (void)loading{
    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(globalQueue, ^{
        [self prepareData];
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        dispatch_async(mainQueue, ^{
            [self setupViews];
        });
    });
}

- (void)prepareData{
    //area.plist是字典
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"area" ofType:@"plist"];
    _areaDic = [[NSDictionary alloc]initWithContentsOfFile:plistPath];
    //NSLog(@"_areaDic=%@",_areaDic);
    //city.plist是数组
    NSString *plist = [[NSBundle mainBundle] pathForResource:@"city" ofType:@"plist"];
    NSMutableArray *dataCity = [[NSMutableArray alloc] initWithContentsOfFile:plist];
    //NSLog(@"dataCity=%@",dataCity);
    _provinceArr = [[NSMutableArray alloc]init];
    for (NSDictionary *dic in dataCity) {
        ProvinceModel *model  = [[ProvinceModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        model.citiesArr = [[NSMutableArray alloc]init];
        for (NSDictionary *dic in model.cities) {
            CityModel *cityModel = [[CityModel alloc]init];
            [cityModel setValuesForKeysWithDictionary:dic];
            [model.citiesArr addObject:cityModel];
            cityModel.areaArr = [[NSMutableArray alloc]init];
            NSMutableArray *areaArr = _areaDic[[cityModel.code description]];
            //NSLog(@"cityModel.code=%@,areaArr=%@",[cityModel.code description],areaArr);
            for (NSDictionary *areaDic in areaArr) {
                //NSLog(@"areaDic=%@",areaDic);
                AreaModel *areaModel = [[AreaModel alloc] init];
                [areaModel setValuesForKeysWithDictionary:areaDic];
                [cityModel.areaArr addObject:areaModel];
            }
        }
        [_provinceArr addObject:model];
    }
}

- (void)setupViews{
    CGFloat height = 44;
    self.layer.shadowColor = [[UIColor grayColor] CGColor];
    self.backgroundColor = SC_Color_F8F8F8;
    UIView *toolBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, height)];
    toolBar.backgroundColor = [UIColor grayColor];
    UILabel *lblCancel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, 32, height)];
    lblCancel.textAlignment = NSTextAlignmentCenter;
    lblCancel.textColor = SC_Color_Black;
    lblCancel.font = [UIFont systemFontOfSize:14];
    lblCancel.userInteractionEnabled = YES;
    lblCancel.text = @"取消";
    UITapGestureRecognizer *cancelGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancel)];
    [lblCancel addGestureRecognizer:cancelGesture];
    [toolBar addSubview:lblCancel];
    
    CGFloat startX = SCREEN_WIDTH - 16 - 32 - 16;
    UILabel *lblSure = [[UILabel alloc] initWithFrame:CGRectMake(startX, 0, 32, height)];
    lblSure.textAlignment = NSTextAlignmentCenter;
    lblSure.textColor = SC_Color_Black;
    lblSure.font = [UIFont systemFontOfSize:14];
    lblSure.userInteractionEnabled = YES;
    lblSure.text = @"确定";
    UITapGestureRecognizer *sureGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(complete)];
    [lblSure addGestureRecognizer:sureGesture];
    [toolBar addSubview:lblSure];
    
    [self addSubview:toolBar];
    
    _pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, height, SCREEN_WIDTH, 216)];
    _pickerView.delegate = self;
    _pickerView.dataSource = self;
    [self addSubview:_pickerView];
    [self measure];
    [_pickerView selectRow:0 inComponent:0 animated:YES];
    [self obtainData:0 withCity:0 withArea:0];
}

- (void)obtainData:(NSInteger)provinceIndex withCity:(NSInteger)cityIndex withArea:(NSInteger)areaIndex{
    ProvinceModel *model = _provinceArr[provinceIndex];
    CityModel *cityModel = model.citiesArr[cityIndex];
    NSArray *arr = cityModel.areaArr;
    AreaModel *areaModel = arr[areaIndex];
    _content = [NSString stringWithFormat:@"%@ %@ %@",model.name,cityModel.name,areaModel.name];
}

- (void)measure{
    CGRect frame = self.frame;
    self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 260);
}

//取消
- (void)cancel{
    if(nil != _delegate){
        [_delegate cancelAreaPicker];
    }
}

- (void)complete{
    if(nil != _delegate){
        [_delegate selectComplete:_content];
    }
}

#pragma mark -UIPickerView datasource delegate
//返回列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}


/**
 返回行数

 @param pickerView pickerview对象
 @param component 当前选择列
 @return 行数
 */
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (0 == component){
        return _provinceArr.count;
    } else if(1==component) {
        NSInteger rowProvince = [pickerView selectedRowInComponent:0];
        ProvinceModel *model = _provinceArr[rowProvince];
        return model.citiesArr.count;
    } else {
        NSInteger rowProvince = [pickerView selectedRowInComponent:0];
        NSInteger rowCity = [pickerView selectedRowInComponent:1];
        ProvinceModel *model = _provinceArr[rowProvince];
        CityModel *cityModel = model.citiesArr[rowCity];
        //NSString *str = [cityModel.code description];
        NSArray *arr = cityModel.areaArr;
        NSLog(@"component = %lu,count=%lu",component,arr.count);
        return arr.count;
    }
}

#pragma mark -UIPickView delegate


/**
 设置标题

 @param pickerView pickerview对象
 @param row 当前行
 @param component 当前列
 @return 内容
 */
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (0 == component) {
        ProvinceModel *model = _provinceArr[row];
        return model.name;
    } else if(1==component) {
        NSInteger rowProvince = [pickerView selectedRowInComponent:0];
        ProvinceModel *model = _provinceArr[rowProvince];
        CityModel *cityModel = model.citiesArr[row];
        return cityModel.name;
    } else {
        NSInteger rowProvince = [pickerView selectedRowInComponent:0];
        NSInteger rowCity = [pickerView selectedRowInComponent:1];
        ProvinceModel *model = _provinceArr[rowProvince];
        CityModel *cityModel = model.citiesArr[rowCity];
        //NSString *str = [cityModel.code description];
        //NSArray *arr = _areaDic[str];
        NSArray *arr = cityModel.areaArr;
        //AreaModel *areaModel = [[AreaModel alloc]init];
        //[areaModel setValuesForKeysWithDictionary:arr[row]];
        AreaModel *areaModel = arr[row];
        return areaModel.name;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *lalTitle=(UILabel *)view;
    if (!lalTitle) {
        lalTitle=[[UILabel alloc] init];
        lalTitle.minimumScaleFactor=8;//设置最小字体，与minimumFontSize相同，minimumFontSize在IOS 6后不能使用。
        lalTitle.adjustsFontSizeToFitWidth=YES;//设置字体大小是否适应lalbel宽度
        lalTitle.textAlignment=NSTextAlignmentCenter;//文字居中显示
        [lalTitle setTextColor:[UIColor blackColor]];
        [lalTitle setFont:[UIFont systemFontOfSize:14]];
    }
    lalTitle.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return lalTitle;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"didSelectRow row = %ld,component = %ld",row,component);
    if(0 == component){
        [pickerView reloadComponent:1];
        [pickerView reloadComponent:2];
    }
    if(1 == component){
        [pickerView reloadComponent:2];
    }
    NSInteger selectOne = [pickerView selectedRowInComponent:0];
    NSInteger selectTwo = [pickerView selectedRowInComponent:1];
    NSInteger selectThree = [pickerView selectedRowInComponent:2];
    
    [self obtainData:selectOne withCity:selectTwo withArea:selectThree];
    
    ProvinceModel *model = _provinceArr[selectOne];
    CityModel *cityModel = model.citiesArr[selectTwo];
    //NSString *str = [cityModel.code description];
    //NSArray *arr = _areaDic[str];
    NSArray *arr = cityModel.areaArr;
    //AreaModel *areaModel = [[AreaModel alloc]init];
    //[areaModel setValuesForKeysWithDictionary:arr[selectThree]];
    AreaModel *areaModel = arr[selectThree];
    NSLog(@"pickerView select %@ %@ %@",model.name,cityModel.name,areaModel.name);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
