//
//  SCCountTextView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/6.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCCountTextView : UIView
//是否显示计数
@property (nonatomic,assign) BOOL showCount;
//计数器字体颜色
@property (nonatomic,strong) UIColor *countTextColor;
//计数器字体大小
@property (nonatomic,strong) UIFont *countTextSize;
//总数
@property (nonatomic,assign) NSInteger totalCount;
//默认文案字体颜色
@property (nonatomic,strong) UIColor *placeHolderTextColor;
//默认文案字体大小
@property (nonatomic,strong) UIFont *placeHolderTextSize;
//默认文案
@property (nonatomic,strong) NSString *placeHolder;
//是否需要移动计算器
@property (nonatomic,assign) BOOL needTranslateCount;

/**
 改变计数器显示位置
 */
-(void)transformCountView:(CGFloat)startY withDuration:(double)duration;

/**
 还原计数器位置
 */
- (void)resetCountView;


/**
 设置内容

 @param content 内容
 */
- (void)setContent:(NSString*)content;
/**
 获取内容

 @return 编辑框中的内容
 */
- (NSString *)obtainContent;
@end
