//
//  UnderlineInputView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/11.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnderlineInputView : UIView

- (UITextField*)getContentView;
@end
