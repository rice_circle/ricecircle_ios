//
//  QuantitySelectView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/26.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^MinimumBlock)(void);

@interface QuantitySelectView : UIView
//最小值
@property (nonatomic,assign) CGFloat miniumValue;
//达到最小值时的回调block
@property (nonatomic,copy) MinimumBlock miniumBlock;
@end
