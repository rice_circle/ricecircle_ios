//
//  CustomView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/17.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self test];
    return self;
}

- (void)test{
    UISwipeGestureRecognizer  *swipe=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeGestureRecognizer:)];
    swipe.direction=UISwipeGestureRecognizerDirectionUp;
    //[self addGestureRecognizer:swipe];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"CustomView touchesBegan");
}


- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"CustomView touchesMoved");
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"CustomView touchesCancelled");
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"CustomView touchesEnded");
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    NSLog(@"CustomView hitTest ");
    return [super hitTest:point withEvent:event];
    //return [self subviews][0];
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    NSLog(@"CustomView pointInside");
    return YES;
}

-(void)swipeGestureRecognizer:(UISwipeGestureRecognizer *)recongnizer{
    NSLog(@"滑动");
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
