//
//  SCAlert.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/9.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SCAlert.h"

@interface SCAlert()

@end

@implementation SCAlert

IOS_SINGLETON_M(SCAlert);


/**
 显示带有确定/取消按钮的alert

 @param title 标题
 @param message message提示语
 @param cancelBlock 取消回调block
 @param okBlock 确定回调block
 @param parent UIViewController对象
 */
- (void)showCancelOkAlert:(NSString*)title withMessage:(NSString*)message  withCancelBlock:(AlertItemBlock)cancelBlock withOKBlock:(AlertItemBlock)okBlock withParent:(UIViewController*)parent{
    [self showCancelOkAlert:title withMessage:message withCancelText:@"取消" withCancelBlock:cancelBlock withOkText:@"确定" withOKBlock:okBlock withParent:parent];
}


/**
 显示带有两个按钮的alert

 @param title 标题
 @param message message提示语
 @param cancelText 第一个按钮文案
 @param cancelBlock 第一个按钮回调block
 @param okText 第二个按钮文案
 @param okBlock 第二个按钮回调block
 @param parent UIViewController对象
 */
- (void)showCancelOkAlert:(NSString*)title withMessage:(NSString*)message withCancelText:(NSString*)cancelText withCancelBlock:(AlertItemBlock)cancelBlock
       withOkText:(NSString*)okText withOKBlock:(AlertItemBlock)okBlock withParent:(UIViewController*)parent{
    NSArray<NSString*> *actionTexts = @[cancelText,okText];
    NSArray<AlertItemBlock> *actionBlocks = @[cancelBlock,okBlock];
    SCButtonAlert *alert = [[SCButtonAlert alloc] initWithTitle:title withMessage:message withActionTexts:actionTexts withActionBlocks:actionBlocks withParent:parent];
    [alert show];
}


/**
 显示单个按钮的alert

 @param title 标题
 @param message message提示语
 @param buttonText 按钮文案
 @param buttonBlock 按钮回调block
 @param parent UIViewController对象
 */
- (void)showSingleButtonAlert:(NSString*)title withMessage:(NSString*)message withButtonText:(NSString*)buttonText withButtonBlock:(AlertItemBlock)buttonBlock withParent:(UIViewController*)parent{
    NSArray<NSString*> *actionTexts = @[buttonText];
    NSArray<AlertItemBlock> *actionBlocks = @[buttonBlock];
    SCButtonAlert *alert = [[SCButtonAlert alloc] initWithTitle:title withMessage:message withActionTexts:actionTexts withActionBlocks:actionBlocks withParent:parent];
    [alert show];
}
@end
