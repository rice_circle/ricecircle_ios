//  带按钮的alert
//  SCButtonAlert.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/9.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SCButtonAlert.h"

@interface SCButtonAlert ()<UIAlertViewDelegate>
@property (nonatomic,strong) UIAlertController *alertVC;
@property (nonatomic,strong) UIAlertView *alertView;
@property (nonatomic,strong) UIViewController *parent;
@property (nonatomic,strong) NSArray<AlertItemBlock>* blocks;
@end

@implementation SCButtonAlert

- (instancetype)initWithTitle:(NSString*)title withMessage:(NSString*)message withActionTexts:(NSArray<NSString*>*) actionTexts withActionBlocks:(NSArray<AlertItemBlock>*) actionBlocks withParent:(UIViewController*)parent{
    self = [super init];
    _blocks = actionBlocks;
    if(self){
        if(IOS_VERSION_8_OR_LATER){
            _parent = parent;
            _alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            for (NSInteger i = 0; i < actionTexts.count; i++) {
                UIAlertAction *action = [UIAlertAction actionWithTitle:[actionTexts objectAtIndex:i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    AlertItemBlock block = [actionBlocks objectAtIndex:i];
                    if(block){
                        [self dismiss];
                        block();
                    }
                }];
                [_alertVC addAction:action];
            }
        }else{
//            _alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:okText, nil];
        }
    }
    return self;
}

-(void)show{
    if(IOS_VERSION_8_OR_LATER){
        if(_parent && _alertVC){
            [_parent presentViewController:_alertVC animated:YES completion:nil];
        }
    }else{
        if(_alertView){
            [_alertView show];
        }
    }
}

- (void)dismiss{
    if(IOS_VERSION_8_OR_LATER){
        if(_parent && _alertVC){
            [_parent dismissViewControllerAnimated:_alertVC completion:nil];
        }
    }
}

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            
            break;
            
        case 1:
            
            break;
    }
    
}


@end
