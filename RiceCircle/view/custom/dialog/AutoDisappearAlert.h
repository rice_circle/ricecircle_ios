//
//  AutoDisappearAlert.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/8.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AutoDisappearAlert : NSObject

/**
 创建自动销毁的alert
 
 @param title 标题
 @param message 显示信息
 @param parent 父窗口
 @param finish alert消失回调block
 */
- (void)showAlertWithTitle:(NSString *)title withMessage:(NSString *)message withParent:(UIViewController *)parent withFinish:(void (^)(void))finish;

@end
