//  自动消失的alert
//  AutoDisappearAlert.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/8.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "AutoDisappearAlert.h"

@implementation AutoDisappearAlert


/**
 创建自动销毁的alert

 @param title 标题
 @param message 显示信息
 @param parent 父窗口
 @param finish alert消失回调block
 */
- (void)showAlertWithTitle:(NSString *)title withMessage:(NSString *)message withParent:(UIViewController *)parent withFinish:(void (^)(void))finish{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [parent presentViewController:alert animated:YES completion:finish];
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(dismissAlert:) userInfo:alert repeats:NO];
}

//消失alert
- (void)dismissAlert:(NSTimer *)timer{
    UIAlertController * alert = (UIAlertController *)[timer userInfo];
    [alert dismissViewControllerAnimated:YES completion:nil];
    alert = nil;
}

@end
