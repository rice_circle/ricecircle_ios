//
//  SCButtonAlert.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/9.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^AlertItemBlock)(void);

@interface SCButtonAlert : NSObject

- (instancetype)initWithTitle:(NSString*)title withMessage:(NSString*)message withActionTexts:(NSArray<NSString*>*) actionTexts withActionBlocks:(NSArray<AlertItemBlock>*) actionBlocks withParent:(UIViewController*)parent;

-(void)show;

@end
