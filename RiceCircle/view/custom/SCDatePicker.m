//  日期选择
//  SCDatePicker.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/7.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SCDatePicker.h"
// 添加这个宏定义，以后在使用 Masonry 框架中的属性和方法的时候, 就可以省略mas_前缀
#define MAS_SHORTHAND
// 添加这个宏定义，就可以让 equalTo 方法就等价于 mas_equalTo 方法
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"
#import "UIColor+Hex.h"

@interface SCDatePicker()
//日历选择
@property (nonatomic,strong) UIDatePicker *datePicker;
//取消
@property (nonatomic,strong) UIButton *btnCancel;
//确定
@property (nonatomic,strong) UIButton *btnSure;
@property (nonatomic,strong) NSString *content;
@end

@implementation SCDatePicker

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setupViews];
    }
    return self;
}

- (void)setupViews{
    self.backgroundColor = [UIColor whiteColor];
    _datePicker = [[UIDatePicker alloc] init];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    //给UiDatePicker添加locale属性
    _datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh-Hans"];
    //添加滑动事件
    [_datePicker addTarget:self action:@selector(pickerChangeValue:) forControlEvents:UIControlEventValueChanged];
    _datePicker.maximumDate = [NSDate date];
    [self addSubview:_datePicker];
    
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:topView];
    
    _btnCancel = [UIButton buttonWithType:UIButtonTypeSystem];
    [_btnCancel setTitle:@"取消" forState:UIControlStateNormal ];
    [_btnCancel setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    _btnCancel.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_btnCancel addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    _btnCancel.titleLabel.font = [UIFont systemFontOfSize:14];
    [topView addSubview:_btnCancel];
    
    _btnSure = [UIButton buttonWithType:UIButtonTypeSystem];
    [_btnSure setTitle:@"确定" forState:UIControlStateNormal ];
    [_btnSure setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    _btnSure.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_btnSure addTarget:self action:@selector(sure) forControlEvents:UIControlEventTouchUpInside];
    _btnSure.titleLabel.font = [UIFont systemFontOfSize:14];
    [topView addSubview:_btnSure];
    
    [topView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.top).offset(0);
        make.left.equalTo(self.left).offset(0);
        make.right.equalTo(self.right).offset(0);
        make.height.equalTo(44);
    }];
    
    [_btnCancel makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(topView);
        make.left.equalTo(topView.left).offset(16);
        make.centerY.equalTo(topView);
    }];

    [_btnSure makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(topView);
        make.right.equalTo(topView.right).offset(-16);
        make.centerY.equalTo(topView);
    }];
    
    [_datePicker makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.bottom).offset(5);
        make.left.equalTo(self.left).offset(0);
        make.right.equalTo(self.right).offset(0);
    }];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 265);
    [self pickerChangeValue:_datePicker];
}

- (void)cancel{
    if(_cancelBlock){
        _cancelBlock();
    }
}

- (void)sure{
    if(_sureBlock){
        _sureBlock(_content);
    }
}

//监听UIDatePicker滑动
- (void)pickerChangeValue:(UIDatePicker *)picker{
    NSDate *selectDate = picker.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSString *format = @"yyyy-MM-dd";
    dateFormatter.dateFormat = format;
    NSString *value = [dateFormatter stringFromDate:selectDate];
    _content = value;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
