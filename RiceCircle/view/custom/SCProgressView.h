//
//  SCProgressView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/17.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCProgressView : UIView
//背景色
@property (nonatomic,strong) UIColor *progressBGColor;
//进度条颜色
@property (nonatomic,strong) UIColor *progressTintColor;
//进度
@property (nonatomic,assign) float progress;
//@角度
@property (nonatomic,assign) float radius;

//生成一个实例
+(instancetype)initProgress;
@end
