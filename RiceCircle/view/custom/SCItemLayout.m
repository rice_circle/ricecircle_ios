//
//  SCItemLayout.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SCItemLayout.h"
// 添加这个宏定义，以后在使用 Masonry 框架中的属性和方法的时候, 就可以省略mas_前缀
#define MAS_SHORTHAND
// 添加这个宏定义，就可以让 equalTo 方法就等价于 mas_equalTo 方法
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"

@interface SCItemLayout()
//标题
@property (nonatomic,strong) UILabel *lblTitle;
//箭头
@property (nonatomic,strong) UIImageView *imgArrow;

@property (nonatomic,strong) UIView *splitView;
@property (nonatomic,strong) UITextField *txfContent;
@property (nonatomic,strong) UILabel *lblContent;
@end

@implementation SCItemLayout

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self initData];
        [self setupViews];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self initData];
    [self setupViews];
}

- (void)initData{
    _itemType = Edit;
}

- (void)setupViews{
    _lblTitle = [[UILabel alloc] init];
    _lblTitle.font = SC_FONT_14;
    _lblTitle.textColor = SC_Color_Black;
    _lblTitle.text = @"测试";
    
    _imgArrow = [[UIImageView alloc] init];
    _imgArrow.image = [UIImage imageNamed:@"gray_arrow"];
    
    _txfContent = [[UITextField alloc] init];
    _txfContent.backgroundColor = [UIColor clearColor];
    _txfContent.font = SC_FONT_14;
    _txfContent.textColor = SC_Color_Black;
    _txfContent.clearButtonMode = UITextFieldViewModeNever;
    _txfContent.textAlignment = NSTextAlignmentRight;
    
    _splitView = [[UIView alloc] init];
    _splitView.backgroundColor = SC_Color_F8F8F8;
    
    _lblContent = [[UILabel alloc] init];
    _lblContent.font = SC_FONT_14;
    _lblContent.textColor = SC_Color_Black;
    _lblContent.text = @"内容";
    
    [self addSubview:_lblTitle];
    [self addSubview:_imgArrow];
    [self addSubview:_txfContent];
    [self addSubview:_splitView];
    [self addSubview:_lblContent];
    
    [_lblTitle makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(16);
        make.centerY.equalTo(self.centerY);
        make.height.equalTo(10);
    }];
    
    [_imgArrow makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-16);
        make.centerY.equalTo(self.centerY);
        make.width.equalTo(9);
        make.height.equalTo(16);
    }];
    
    [_lblContent makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_imgArrow.left).offset(-3);
        make.centerY.equalTo(self.centerY);
        make.height.equalTo(20);
    }];
    
    [_txfContent makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-16);
        make.centerY.equalTo(self.centerY);
        make.height.equalTo(20);
        make.width.equalTo(self.width).multipliedBy(0.65f);
    }];
    
    [_splitView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(0);
        make.height.equalTo(1);
    }];
}

- (void)setItemType:(ItemType)itemType{
    _itemType = itemType;
    switch (itemType) {
        case Edit:{
            _imgArrow.hidden = YES;
            _txfContent.hidden = NO;
            _lblContent.hidden = YES;
        }
            
            break;
            
        case Arrow:{
            _imgArrow.hidden = NO;
            _txfContent.hidden = YES;
            _lblContent.hidden = NO;
        }
            break;
        case Readonly:{
            _imgArrow.hidden = YES;
            _txfContent.hidden = YES;
            _lblContent.hidden = YES;
        }
            break;
    }
}

- (void)setNeedSplit:(BOOL)needSplit{
    _needSplit = needSplit;
    _splitView.hidden = needSplit;
}

- (UITextField*)obtainTextField{
    return _txfContent;
}

/**
 设置名称
 
 @param title 名称
 */
- (void)setTitle:(NSString *)title{
    _lblTitle.text = title;
}

/**
 设置内容
 
 @param content 内容
 */
- (void)setContent:(NSString *)content{
    if(_itemType == Edit){
        _txfContent.text = content;
    }else if(_itemType == Arrow){
        _lblContent.text = content;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
