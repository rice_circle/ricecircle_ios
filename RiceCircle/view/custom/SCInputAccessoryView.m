//
//  SCInputAccessoryView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/3.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SCInputAccessoryView.h"

@interface SCInputAccessoryView()
//存储文字
@property (nonatomic,strong) NSMutableArray *textArray;
//存储图片
@property (nonatomic,strong) NSMutableArray *imageArray;
//当前item类型是图片还是文字
@property (nonatomic,assign) BOOL isAllText;
@end

@implementation SCInputAccessoryView

- (instancetype)initWithFrame:(CGRect)frame{
    return [self initWithFrame:frame withType:Default];
}

- (instancetype)initWithFrame:(CGRect)frame withType:(ShowType)type{
    self = [super initWithFrame:frame];
    if(self){
        [self initData:type];
    }
    self.barStyle = UIBarStyleDefault;
    return self;
}

//初始化数据
- (void)initData:(ShowType)type{
    _showType = type;
    _isAllText = YES;
    switch (type) {
        case Default:
            [self initDefault];
            break;
            
        case Operable:
            [self initOperable];
            break;
        case OneItem:
            [self initOneItem];
            break;
    }
}

//初始化只有一个完成按钮
- (void)initOneItem{
    if(_textArray == nil){
        _textArray = [NSMutableArray array];
    }
    [_textArray addObject:@"完成"];
}

//初始化默认的数据
- (void)initDefault{
    if(_textArray == nil){
        _textArray = [NSMutableArray array];
    }
    [_textArray addObject:@"取消"];
    [_textArray addObject:@"完成"];
}

//初始化默认的数据
- (void)initOperable{
    if(_textArray == nil){
        _textArray = [NSMutableArray array];
    }
    [_textArray addObject:@"前一项"];
    [_textArray addObject:@"后一项"];
    [_textArray addObject:@"完成"];
}


/**
 添加子item

 @param items 子item
 @param textItem 子item是文字还是图片
 */
- (void)addItem:(NSMutableArray*)items isTextItem:(BOOL)textItem{
    _isAllText = textItem;
    if(_isAllText){
        if(_textArray == nil){
            _textArray = [NSMutableArray array];
        }
        [_textArray removeAllObjects];
        [_textArray addObjectsFromArray:items];
    }else{
        if(_imageArray == nil){
            _imageArray = [NSMutableArray array];
        }
        [_imageArray removeAllObjects];
        [_imageArray addObjectsFromArray:items];
    }
}

//构建view
- (void)buildViews{
    if(_isAllText){
        [self buildTextViews];
        return;
    }
    [self buildImageViews];
}

//构建纯文本view
- (void)buildTextViews{
    UIBarButtonItem *item = nil;
    NSMutableArray *items = [NSMutableArray array];
    for (NSInteger i = 0; i < _textArray.count; i ++) {
        if(i == _textArray.count - 1){
            UIBarButtonItem *itemFlexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
            [items addObject:itemFlexible];
        }
        item = [[UIBarButtonItem alloc] initWithTitle:[_textArray objectAtIndex:i]  style:UIBarButtonItemStylePlain target:self action:@selector(itemClick:)];
        item.tag = i;
        [items addObject:item];
    }
    self.items = items;
}

//构建纯图片view
- (void)buildImageViews{
    UIBarButtonItem *item = nil;
    NSMutableArray *items = [NSMutableArray array];
    for (NSInteger i = 0; i < _imageArray.count; i ++) {
        if(i == _imageArray.count - 1){
            UIBarButtonItem *itemFlexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
            [items addObject:itemFlexible];
        }
        item = [[UIBarButtonItem alloc] initWithImage:[_imageArray objectAtIndex:i] style:UIBarButtonItemStylePlain target:self action:@selector(itemClick:)];
        item.tag = i;
        [items addObject:item];
    }
    self.items = items;
}

- (void)itemClick:(UIBarButtonItem*)sender{
    NSInteger tag = sender.tag;
    NSLog(@"accessory tag=%lu",tag);
    if(nil != _accessoryDelegate){
        [_accessoryDelegate accessoryItemClick:tag];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
