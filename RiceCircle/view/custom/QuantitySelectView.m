//  数量选择控件
//  QuantitySelectView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/26.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "QuantitySelectView.h"
#import "UIColor+Hex.h"

@interface QuantitySelectView()<UITextFieldDelegate>
@property (nonatomic,strong) UITextField *txtContent;
@end

@implementation QuantitySelectView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self setupViews];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setupViews];
}

- (void)setupViews{
    _miniumValue = 1;
    CGRect frame = [self frame];
    CGFloat start = 0;
    UILabel *lblDecrease = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, frame.size.height)];
    lblDecrease.text = @"-";
    lblDecrease.font = SC_FONT_14;
    lblDecrease.textColor = SC_Color_Black;
    lblDecrease.textAlignment = NSTextAlignmentCenter;
    lblDecrease.userInteractionEnabled = YES;
    UITapGestureRecognizer *decreaseGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(decrease)];
    [lblDecrease addGestureRecognizer:decreaseGesture];
    [self addSubview:lblDecrease];
    start += 30;
    UIView *splitView1 = [[UIView alloc] initWithFrame:CGRectMake(start, 0, 1, frame.size.height)];
    splitView1.backgroundColor = SC_Color_Gray_D2D2D2;
    [self addSubview:splitView1];
    start += 1;
    _txtContent = [[UITextField alloc] initWithFrame:CGRectMake(start, 0, 50, frame.size.height)];
    _txtContent.backgroundColor = [UIColor clearColor];
    //输入类型为数字
    _txtContent.keyboardType = UIKeyboardTypeDecimalPad;
    _txtContent.clearButtonMode = UITextFieldViewModeNever;
    _txtContent.delegate = self;
    _txtContent.font = SC_FONT_14;
    _txtContent.textAlignment = NSTextAlignmentCenter;
    _txtContent.text = @"1";
    [self addSubview:_txtContent];
    start += 50;
    
    UIView *splitView2 = [[UIView alloc] initWithFrame:CGRectMake(start, 0, 1, frame.size.height)];
    splitView2.backgroundColor = SC_Color_Gray_D2D2D2;
    [self addSubview:splitView2];
    start += 1;
    
    UILabel *lblPlus = [[UILabel alloc] initWithFrame:CGRectMake(start, 0, 30, frame.size.height)];
    lblPlus.text = @"+";
    lblPlus.font = SC_FONT_14;
    lblPlus.textColor = SC_Color_Black;
    lblPlus.textAlignment = NSTextAlignmentCenter;
    lblPlus.userInteractionEnabled = YES;
    UITapGestureRecognizer *plusGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pluse)];
    [lblPlus addGestureRecognizer:plusGesture];
    [self addSubview:lblPlus];
    start += 30;
    
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 5;
    self.layer.borderWidth = 1;
    self.layer.borderColor = [[UIColor colorWithHexString:@"#D2D2D2"] CGColor];
    self.frame = CGRectMake(frame.origin.x, frame.origin.y, start, frame.size.height);
}

- (void)decrease{
    NSString *value = _txtContent.text;
    if(!NULLString(value)){
        NSInteger intValue = [value integerValue];
        if(intValue > _miniumValue){
            intValue -= 1;
            [_txtContent setText:[NSString stringWithFormat:@"%lu",intValue]];
            return;
        }
        if(_miniumBlock){
            self.miniumBlock();
        }
    }
}

- (void)pluse{
    NSString *value = _txtContent.text;
    if(!NULLString(value)){
        NSInteger intValue = [value integerValue];
        intValue += 1;
        [_txtContent setText:[NSString stringWithFormat:@"%lu",intValue]];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    NSLog(@"layoutSubviews");
}

#pragma mark UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSString *value = [textField text];
    if(NULLString(value)){
        [textField setText:@"1"];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
