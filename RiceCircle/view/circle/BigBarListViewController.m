//  大吧列表
//  BigBarListViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/29.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BigBarListViewController.h"
#import "RefreshTableView.h"
#import "ProductTableCellView.h"
#import "ShouldHelpIndexViewController.h"
#import "ProductListViewController.h"
#import "RecommendModel.h"
#import "MJRefresh.h"
#import "ActivityDetailViewController2.h"
#import "ShouldHelpTableCellView.h"

@interface BigBarListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) RefreshTableView *tableView;
@property (nonatomic,strong) NSMutableArray *contentDataSource;
@end

@implementation BigBarListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self initData];
    [self setupViews];
}

- (void)initData{
    _contentDataSource = [NSMutableArray array];
    RecommendModel *model = nil;
    for (NSInteger i = 0; i < 8; i++) {
        model = [[RecommendModel alloc] init];
        model.type = i % 2;
        [_contentDataSource addObject:model];
    }
    
}

- (void)setupViews{
    [self initNav];
    [self initTableView];
}

- (void)initNav{
    self.title = @"大吧";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStylePlain target:self action:@selector(search)];
    self.navigationItem.rightBarButtonItem = rightItem;
}


//初始化TableView
- (void)initTableView{
    _tableView = [[RefreshTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT) style:UITableViewStylePlain type:BOTH];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 80;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.separatorInset= UIEdgeInsetsZero;
    [self.tableView registerNib:[UINib nibWithNibName:@"ShouldHelpTableCellView" bundle:nil] forCellReuseIdentifier:@"shouldCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProductTableCellView" bundle:nil] forCellReuseIdentifier:@"productCell"];
    IMP_BLOCK_SELF(BigBarListViewController)
    self.tableView.refresh = ^{
        [block_self requestData:1];
    };
    self.tableView.loading = ^{
        [block_self requestData:2];
    };
    [self.view addSubview:_tableView];
}

- (void)search{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestData:(NSInteger)type{

}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
    RecommendModel *model = [_contentDataSource objectAtIndex:row];
    switch (model.type) {
        case 0:{//应援
            ShouldHelpIndexViewController *shouldvc = [[ShouldHelpIndexViewController alloc] init];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:shouldvc animated:YES];
            self.hidesBottomBarWhenPushed = NO;
            
        }
            break;
        case 1:{//商品
            //            ProductListViewController *shouldvc = [[ProductListViewController alloc] init];
            //            self.hidesBottomBarWhenPushed = YES;
            //            [self.navigationController pushViewController:shouldvc animated:YES];
            //            self.hidesBottomBarWhenPushed = NO;
            ActivityDetailViewController2 *detailVC = [[ActivityDetailViewController2 alloc] init];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:detailVC animated:YES];
            self.hidesBottomBarWhenPushed = NO;
        }
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _contentDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RecommendModel *content = _contentDataSource[indexPath.row];
    NSLog(@"cellForRowAtIndexPath row=%lu,type=%lu",indexPath.row,content.type);
    switch (content.type) {
        case 0:{
            ShouldHelpTableCellView *shouldCell = [ShouldHelpTableCellView cellWithTableView:tableView withIndex:indexPath.row];
            shouldCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return shouldCell;
        }
        case 1:{
            ProductTableCellView *productCell = [ProductTableCellView cellWithTableView:tableView withIndex:indexPath.row];
            productCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return productCell;
        }
    }
    return nil;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
