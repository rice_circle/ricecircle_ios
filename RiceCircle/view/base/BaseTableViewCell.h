//
//  BaseTableViewCell.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/20.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>
// 添加这个宏定义，以后在使用 Masonry 框架中的属性和方法的时候, 就可以省略mas_前缀
#define MAS_SHORTHAND
// 添加这个宏定义，就可以让 equalTo 方法就等价于 mas_equalTo 方法
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"

@protocol TableCellDelegate <NSObject>

- (void)tableCellClick:(NSInteger)index withPosition:(NSIndexPath *)position withModel:(NSObject*)object;

@end

@interface BaseTableViewCell : UITableViewCell

@property (nonatomic,assign) NSIndexPath *position;

@property (nonatomic,weak) id<TableCellDelegate> delegate;

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index;
@end
