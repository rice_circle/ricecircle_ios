//
//  项目中用到的UIViewController基类
//  BaseUIViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/17.
//  Copyright © 2017年 孙继常. All rights reserved.
//

// 添加这个宏定义，以后在使用 Masonry 框架中的属性和方法的时候, 就可以省略mas_前缀
#define MAS_SHORTHAND
// 添加这个宏定义，就可以让 equalTo 方法就等价于 mas_equalTo 方法
#define MAS_SHORTHAND_GLOBALS
#import <UIKit/UIKit.h>
#import "Masonry.h"

@protocol UIViewControllerBackDelegate <NSObject>

- (void)viewControllerBack:(NSObject*)object;

@end

@interface BaseUIViewController : UIViewController

@property (nonatomic,weak) id<UIViewControllerBackDelegate> delegate;

@end
