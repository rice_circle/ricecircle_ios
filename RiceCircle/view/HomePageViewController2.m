//
//  HomePageViewController2.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/14.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "HomePageViewController2.h"
#import "MJRefresh.h"
#import "CycleBannerView.h"
#import "AdvertisingModel.h"
#import "RecommendModel.h"
#import "HomeCenterTableCellView.h"
#import "ShouldHelpTableCellView.h"
#import "ProductTableCellView.h"
#import "ShouldHelpIndexViewController.h"
#import "ProductListViewController.h"
#import "ActivityDetailViewController2.h"

@interface HomePageViewController2 ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic , strong) CycleBannerView *bannerView;
//广告数据
@property (nonatomic,strong) NSMutableArray *adDataSource;
@property (nonatomic,strong) NSMutableArray *contentDataSource;
@end

@implementation HomePageViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    // Do any additional setup after loading the view.
    [self initTableView];
    [self initBanner];
    [self initTopNet];
}

- (void)initData{
    _contentDataSource = [NSMutableArray array];
    RecommendModel *model = nil;
    for (NSInteger i = 0; i < 8; i++) {
        model = [[RecommendModel alloc] init];
        if(i == 0){
            model.type = -1;
        }else{
            model.type = i % 2;
        }
        [_contentDataSource addObject:model];
    }
    
}

//初始化TableView
- (void)initTableView{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 80;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.separatorInset= UIEdgeInsetsZero;
    [self.tableView registerNib:[UINib nibWithNibName:@"HomeCenterTableCellView" bundle:nil] forCellReuseIdentifier:@"centerCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ShouldHelpTableCellView" bundle:nil] forCellReuseIdentifier:@"shouldCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProductTableCellView" bundle:nil] forCellReuseIdentifier:@"productCell"];
    MJRefreshNormalHeader *header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData:1];
    }];
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self requestData:2];
    }];
    _tableView.mj_header = header;
    _tableView.mj_footer = footer;
    [self.view addSubview:_tableView];
}

//初始化广告栏
- (void)initBanner{
    CycleBannerView *bannerView = [[CycleBannerView alloc] initWithFrame:CGRectMake(0, 100, SCREEN_WIDTH, 104)];
    //bannerView.bgImg = [UIImage imageNamed:@"shadow.png"];
    
    IMP_BLOCK_SELF(HomePageViewController2);
    bannerView.clickItemBlock = ^(NSInteger index) {
        
    };
    self.tableView.tableHeaderView = bannerView;
    self.bannerView = bannerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 网络请求数据

- (void)requestData:(NSInteger)type{
    
}


/**
 广告数据库请求
 */
- (void)initTopNet{
    _adDataSource = [NSMutableArray array];
    NSMutableArray *textArr = [NSMutableArray array];
    AdvertisingModel *ad = nil;
    NSArray *tmpArr = @[@"http://cms-bucket.nosdn.127.net/d6cb9a451f444c9b84f3148ac4c1638220170614073020.jpeg",@"http://cms-bucket.nosdn.127.net/c6618b132d52440cacac206f7cdc75ff20170613185959.jpeg",@"http://cms-bucket.nosdn.127.net/c606f550d88b4edb908d1082be4cf26a20170614025320.png",@"http://cms-bucket.nosdn.127.net/cc81d6d0bd0245949e1a8d8a35a92d5020170614065302.png",@"http://cms-bucket.nosdn.127.net/4d68cb2b33d94217947cd2e9ccd4e12a20170614062235.png"];
    for (NSInteger i = 0; i < tmpArr.count; i ++) {
        ad = [[AdvertisingModel alloc] init];
        ad.url = [tmpArr objectAtIndex:i];
        [_adDataSource addObject:ad];
        [textArr addObject:[NSString stringWithFormat:@"%lu",i]];
    }
    _bannerView.aryImg = tmpArr;
    _bannerView.aryText = textArr;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
    RecommendModel *model = [_contentDataSource objectAtIndex:row];
    switch (model.type) {
        case -1:
            
            break;
            
        case 0:{//应援
            ShouldHelpIndexViewController *shouldvc = [[ShouldHelpIndexViewController alloc] init];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:shouldvc animated:YES];
            self.hidesBottomBarWhenPushed = NO;
            
        }
            break;
        case 1:{//商品
//            ProductListViewController *shouldvc = [[ProductListViewController alloc] init];
//            self.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:shouldvc animated:YES];
//            self.hidesBottomBarWhenPushed = NO;
            ActivityDetailViewController2 *detailVC = [[ActivityDetailViewController2 alloc] init];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:detailVC animated:YES];
            self.hidesBottomBarWhenPushed = NO;
        }
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _contentDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RecommendModel *content = _contentDataSource[indexPath.row];
    NSLog(@"cellForRowAtIndexPath row=%lu,type=%lu",indexPath.row,content.type);
    switch (content.type) {
        case -1:{
            HomeCenterTableCellView *centerCell = [HomeCenterTableCellView cellWithTableView:tableView withIndex:indexPath.row];
            centerCell.selectionStyle = UITableViewCellSelectionStyleNone;
            centerCell.viewController = self;
            return centerCell;
            }
        case 0:{
            ShouldHelpTableCellView *shouldCell = [ShouldHelpTableCellView cellWithTableView:tableView withIndex:indexPath.row];
            shouldCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return shouldCell;
        }
        case 1:{
            ProductTableCellView *productCell = [ProductTableCellView cellWithTableView:tableView withIndex:indexPath.row];
            productCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return productCell;
        }
    }
    return nil;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
