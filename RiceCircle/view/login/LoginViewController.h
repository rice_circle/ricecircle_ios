//
//  LoginViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/18.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"

@interface LoginViewController : BaseUIViewController
@property (weak, nonatomic) IBOutlet UITextField *tfAccount;
@property (weak, nonatomic) IBOutlet UITextField *tfPwd;
@property (weak, nonatomic) IBOutlet UILabel *labelForget;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@end
