//
//  LoginViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/18.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "FindPwdViewController.h"
#import "RiceCircleController.h"
#import "SCDatePicker.h"
#import "UIColor+Hex.h"
#import "UIImageUtil.h"
#import "AutoDisappearAlert.h"
#import "UnderlineInputView.h"

@interface LoginViewController ()
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tfAccount.userInteractionEnabled=YES;
    [self initView];
}

- (void)initView{
    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:0];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"找回密码" style:UIBarButtonItemStyleDone target:self action:@selector(findPwd)];
    self.navigationItem.rightBarButtonItem = rightItem;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(findPwd)];
    _labelForget.userInteractionEnabled=YES;
    [_labelForget addGestureRecognizer:tap];
    
    //[_btnLogin setBackgroundImage:[UIImageUtil createImageWithColor:[UIColor colorWithHexString:@"#f25f72"]] forState:UIControlStateNormal];
    _btnLogin.backgroundColor = SC_Color_Red;
    _btnLogin.layer.cornerRadius = 5;
    _btnLogin.layer.borderColor = [UIColor colorWithHexString:@"#f25f73"].CGColor;
    _btnLogin.layer.borderWidth = 1;
    
    //[_btnRegister setBackgroundImage:[UIImageUtil createImageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    _btnRegister.backgroundColor = SC_Color_White;
    _btnRegister.layer.cornerRadius = 5;
    _btnRegister.layer.borderColor = [UIColor colorWithHexString:@"#f25f73"].CGColor;
    _btnRegister.layer.borderWidth = 1;
    
    UnderlineInputView *inputView = [[UnderlineInputView alloc] initWithFrame:CGRectMake(20, 430, 300, 50)];
    [inputView getContentView].placeholder = @"输入内容";
    [self.view addSubview:inputView];
}

- (void)viewWillAppear:(BOOL)animated{
    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:0];
}

//找回密码
- (void)findPwd{
    FindPwdViewController *findVC = [[FindPwdViewController alloc] init];
    self.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:findVC animated:YES];
    self.hidesBottomBarWhenPushed=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//注册
- (IBAction)gotoRegister:(id)sender {
    RegisterViewController *registerVC = [[RegisterViewController alloc] init];
    self.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:registerVC animated:YES];
    self.hidesBottomBarWhenPushed=NO;
}

//登录
- (IBAction)gotoLogin:(id)sender {
    RiceCircleController *rootTabBar = [[[NSBundle mainBundle] loadNibNamed:@"RiceCircleController" owner:self options:nil] lastObject];
    [self.navigationController pushViewController:rootTabBar animated:YES];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
