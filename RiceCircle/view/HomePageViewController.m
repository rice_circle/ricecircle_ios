//
//  HomePageViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "HomePageViewController.h"
#import "NetworkManager.h"
#import "Test.h"
#import "SessionModel.h"
#import "CacheManager.h"

@interface HomePageViewController ()<NetResponseDelegate>

@end

@implementation HomePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    NSLog(@"HomePageViewController viewDidLoad");
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)request:(id)sender {
    NSLog(@"request");
//    NetworkManager *net = [NetworkManager shareInstance];
//    net.baseUrl = @"http://202.5.16.149/connection/";
//    NSDictionary *params =@{@"pageSize":@"10",@"pageNum":@"1"};
//    [net sendRequestWithMethod:POST withPath:@"bulletin/queryAll" withParams:params withCls:Test.class withCacheTime:10 withAllowCache:YES withDelegate:self];
    [self requests];
}

- (void)requests{
    [CacheManager sharedCacheManager].SERVCIE_RANDOM_KEY = 111111;
    NetworkManager *net = [NetworkManager shareInstance];
    NSString *deviceUUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:deviceUUID forKey:@"Device"];
    [param setObject:[NSNumber numberWithInteger:[[CacheManager sharedCacheManager] SERVCIE_RANDOM_KEY]] forKey:@"ClientN"];
    [net sendRequestWithMethod:POST withPath:@"apply-session" withParams:param withCls:SessionModel.class withDelegate:self withEncrypt:SIGNATURE];
}

- (void)viewDidAppear:(BOOL)animated{
    NSLog(@"_labelName.text %@",_button.currentTitle);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)success:(id)result{
    NSLog(@"success");
    SessionModel *session = (SessionModel*)result;
    NSLog(@"SessionModel=%@",session.sign);
    NSDictionary *dict = @{@"value1":@"张三"};
    //NSLog(@"dict=%@",dict);
}

- (void)fail:(NSString *)error{

}

//- (instancetype)initWithCoder:(NSCoder *)aDecoder {
//    self = [super initWithCoder:aDecoder];
//    if(self){
//        //do something
//        [self setupViews];
//    }
//    return self;
//}
//
//-  (void)setupViews{
//    [[NSBundle mainBundle] loadNibNamed:@"HomePageViewController" owner:self options:nil];
//    //[self.view addSubview:_button];
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
