//
//  MineViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "MineViewController.h"
#import "CreateFansGroupViewController.h"
#import "OrderCenterViewController.h"
#import "ReceiptAddressListViewController.h"
#import "MyFollowViewController.h"
#import "PersonalInfoViewController.h"
#import "ActivityPublishViewController2.h"
#import "ActivityPublishViewController.h"

#define HEIGHT 34

@interface MineViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewTopConstraint;
//建粉丝团
@property (weak, nonatomic) IBOutlet UIView *createFansGroup;
//收货地址
@property (weak, nonatomic) IBOutlet UIView *receiptAddress;
//客户中心
@property (weak, nonatomic) IBOutlet UIView *customerService;
//意见反馈
@property (weak, nonatomic) IBOutlet UIView *feedback;
@property (weak, nonatomic) IBOutlet UIView *myFollowView;
//我的订单
@property (weak, nonatomic) IBOutlet UIView *layoutOrder;
//我的发布
@property (weak, nonatomic) IBOutlet UIView *layoutPublish;

@property (weak, nonatomic) IBOutlet UIImageView *imgHead;
@property (nonatomic,assign) CGFloat preAlpha;
@end

@implementation MineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupViews];
}

- (void)setupViews{
    _preAlpha = 0;
    self.title = @"我的";
    _scrollView.delegate = self;
    _scrollViewBottomConstraint.constant = -44;
    _scrollViewTopConstraint.constant = - 64;
    UITapGestureRecognizer *fansGroupGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fansGroup)];
    [_createFansGroup addGestureRecognizer:fansGroupGesture];
    
    UITapGestureRecognizer *myOrderGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(myOrder)];
    _layoutOrder.userInteractionEnabled = YES;
    [_layoutOrder addGestureRecognizer:myOrderGesture];
    
    UITapGestureRecognizer *addressGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(address)];
    [_receiptAddress addGestureRecognizer:addressGesture];
    
    UITapGestureRecognizer *followGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(myFollow)];
    [_myFollowView addGestureRecognizer:followGesture];
    
    UITapGestureRecognizer *personalGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(personalInfo)];
    _imgHead.userInteractionEnabled = YES;
    [_imgHead addGestureRecognizer:personalGesture];
    
    UITapGestureRecognizer *publishGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(publishManager)];
    _layoutPublish.userInteractionEnabled = YES;
    [_layoutPublish addGestureRecognizer:publishGesture];
}

//我的订单
- (void)myOrder{
    OrderCenterViewController *orderVC = [[OrderCenterViewController alloc] init];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:orderVC animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

//建团
- (void)fansGroup{
    CreateFansGroupViewController *fansVC = [[CreateFansGroupViewController alloc] init];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:fansVC animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

//收货地址
- (void)address{
    ReceiptAddressListViewController *addressVC = [[ReceiptAddressListViewController alloc] init];
    addressVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addressVC animated:YES];
}

//我的关注
- (void)myFollow{
    MyFollowViewController *followVC = [[MyFollowViewController alloc] init];
    followVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:followVC animated:YES];
}

//个人信息
- (void)personalInfo{
    PersonalInfoViewController *personalVC = [[PersonalInfoViewController alloc] init];
    personalVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:personalVC animated:YES];
}

//我的发布
- (void)publishManager{
    ActivityPublishViewController *publishVC = [[ActivityPublishViewController alloc] init];
    publishVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:publishVC animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    _preAlpha = 0;
    //设置导航栏透明
    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:_preAlpha];
    // 去掉navigationBar底部线条
    //[self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
}

- (void)viewWillDisappear:(BOOL)animated{
    //设置导航栏透明
    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        //do something
       // [self setupViews];
    }
    return self;
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat scrollY = scrollView.contentOffset.y;
    CGFloat down = fabs(scrollY > 0 ? 0 : scrollY) - HEIGHT;
    NSLog(@"scrollViewDidScroll down=%f,y=%f", down,scrollView.contentOffset.y);
    if (down < 0) {
        //down = 0;
    }
    if(down <= 0){
        float alpha = fabs(down) / (HEIGHT * 1.0f);
        //设置导航栏透明
        [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:alpha];
        _preAlpha = alpha;
    }else{
        [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:0];
        _preAlpha =0;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
