//
//  StartInfoCollectionCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/28.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartInfoCollectionCellView : UICollectionViewCell
+ (instancetype)cellWithTableView:(UICollectionView *)collectionView withIndex:(NSIndexPath *)indexPath;
@end
