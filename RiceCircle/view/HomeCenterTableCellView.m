//
//  HomeCenterTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/14.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "HomeCenterTableCellView.h"
#import "ShouldHelpIndexViewController.h"
#import "ProductListViewController.h"
#import "VoteListViewController.h"
#import "AttendanceIndexViewController.h"

@implementation HomeCenterTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    static NSString *ID = @"centerCell";
    HomeCenterTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[HomeCenterTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    NSLog(@"setupUI");
    _bottomSuperViewConstraint.constant = 10;
    UITapGestureRecognizer *shouldGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shouldHelp)];
    [_imgShouldHelp addGestureRecognizer:shouldGesture];
    
    UITapGestureRecognizer *productGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(product)];
    [_imgProduct addGestureRecognizer:productGesture];
    
    UITapGestureRecognizer *publicGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(public)];
    [_imgPublic addGestureRecognizer:publicGesture];
    
    UITapGestureRecognizer *voteGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(vote)];
    [_imgVote addGestureRecognizer:voteGesture];
    
    UITapGestureRecognizer *onlineGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(online)];
    [_imgOnline addGestureRecognizer:onlineGesture];
    
    UITapGestureRecognizer *checkGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkIn)];
    [_imgCheckIn addGestureRecognizer:checkGesture];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    NSLog(@"HomeCenterTableCellView awakeFromNib ");
    [self setupUI];
}

//应援
- (void)shouldHelp{
    ShouldHelpIndexViewController *shouldVC = [[ShouldHelpIndexViewController alloc] init];
    if(nil != _viewController){
        _viewController.hidesBottomBarWhenPushed = YES;
        [_viewController.navigationController pushViewController:shouldVC animated:YES];
        _viewController.hidesBottomBarWhenPushed = NO;
    }
}

//商品
- (void)product{
    ProductListViewController *productVC = [[ProductListViewController alloc] init];
    if(nil != _viewController){
        _viewController.hidesBottomBarWhenPushed = YES;
        [_viewController.navigationController pushViewController:productVC animated:YES];
        _viewController.hidesBottomBarWhenPushed = NO;
    }
}

//公益
- (void)public{

}

//投票
- (void)vote{
    VoteListViewController *voteVC = [[VoteListViewController alloc] init];
    if(nil != _viewController){
        _viewController.hidesBottomBarWhenPushed = YES;
        [_viewController.navigationController pushViewController:voteVC animated:YES];
        _viewController.hidesBottomBarWhenPushed = NO;
    }
}

//上线
- (void)online{

}

//签到
- (void)checkIn{
    AttendanceIndexViewController *attendanceVC = [[AttendanceIndexViewController alloc] init];
    if(nil != _viewController){
        attendanceVC.hidesBottomBarWhenPushed = YES;
        [_viewController.navigationController pushViewController:attendanceVC animated:YES];
        _viewController.hidesBottomBarWhenPushed = NO;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
