//
//  RiceCircleViewController3.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/29.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "RiceCircleViewController3.h"
#import "RiceCircleTopTableCellView.h"
#import "StarTableCellView.h"
#import "StarDetailViewController.h"

@interface RiceCircleViewController3 ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
//top10数据集
@property (nonatomic,strong) NSMutableArray *topDataSet;
@end

@implementation RiceCircleViewController3

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view.
    self.title = @"饭圈";
    [self initData];
    [self initTableView];
}

- (void)initData{
    _topDataSet = [NSMutableArray array];
    for (NSInteger i = 0; i < 20; i++) {
        Star *star = [[Star alloc] init];
        star.name = [NSString stringWithFormat:@"%tu张三%tu",1,i];
        star.fans = i;
        [_topDataSet addObject:star];
    }
}

//初始化TableView
- (void)initTableView{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - TAB_BAT_ITEM_HEIGHT) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 80;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.separatorInset= UIEdgeInsetsZero;
    [_tableView registerNib:[UINib nibWithNibName:@"RiceCircleTopTableCellView" bundle:nil]  forCellReuseIdentifier:@"rcTopCell"];
    [_tableView registerClass:[StarTableCellView class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITableViewDelegate

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(indexPath.row == 0){
//        return;
//    }
//    StarDetailViewController *detailVC = [[StarDetailViewController alloc] init];
//    detailVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:detailVC animated:YES];
//}


#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  _topDataSet.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        RiceCircleTopTableCellView *topCell = [RiceCircleTopTableCellView cellWithTableView:tableView withIndex:indexPath.row];
        topCell.viewController = self;
        return topCell;
    }
    StarTableCellView *cell = [StarTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    Star *star = [_topDataSet objectAtIndex:[indexPath row] - 1];
    [cell assignment:star];
    cell.viewController = self;
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
