//
//  StartInfoCollectionCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/28.
//  Copyright © 2017年 孙继常. All rights reserved.
//
// 添加这个宏定义，以后在使用 Masonry 框架中的属性和方法的时候, 就可以省略mas_前缀
#define MAS_SHORTHAND
// 添加这个宏定义，就可以让 equalTo 方法就等价于 mas_equalTo 方法
#define MAS_SHORTHAND_GLOBALS
#import "StartInfoCollectionCellView.h"
#import "Masonry.h"

@implementation StartInfoCollectionCellView

+ (instancetype)cellWithTableView:(UICollectionView *)collectionView withIndex:(NSIndexPath *)indexPath{
    NSString *ID = @"startInfoCell";
    StartInfoCollectionCellView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ID forIndexPath:indexPath];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[StartInfoCollectionCellView alloc] init];
    }
    return cell;
}

- (instancetype)init{
    self = [super init];
    if(self){
        [self setupViews];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        //initialize code...
        [self setupViews];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setupViews];
}

- (void)setupViews{
    //头像
    UIImageView *imgHead = [[UIImageView alloc] init];
    imgHead.image = [UIImage imageNamed:@"head"];
    [self.contentView addSubview:imgHead];
    //姓名
    UILabel *lblName = [[UILabel alloc] init];
    lblName.font = SC_FONT_14;
    lblName.textColor = SC_Color_Black;
    lblName.text = @"鹿晗";
    [self.contentView addSubview:lblName];
    
    UILabel *lblDes = [[UILabel alloc] init];
    lblDes.textColor = SC_Color_Black_999999;
    lblDes.font = SC_FONT_12;
    lblDes.text = @"粉丝数";
    [self.contentView addSubview:lblDes];
    
    UILabel *lblCount = [[UILabel alloc] init];
    lblCount.font = SC_FONT_12;
    lblCount.textColor = SC_Color_Red;
    lblCount.text = @"1000";
    [self.contentView addSubview:lblCount];
    
    [imgHead makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.contentView).offset(25);
        make.right.equalTo(self.contentView).offset(-25);
        make.height.equalTo(50);
        make.width.equalTo(50);
    }];
    
    [lblName makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgHead.bottom).offset(5);
        //make.height.equalTo(10);
        make.centerX.equalTo(imgHead.centerX);
    }];
    
    [lblDes makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lblName.bottom).offset(5);
        //make.height.equalTo(10);
        make.centerX.equalTo(imgHead.centerX);
    }];
    
    [lblCount makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lblDes.bottom).offset(5);
        //make.height.equalTo(10);
        make.centerX.equalTo(imgHead.centerX);
        make.bottom.equalTo(self.contentView.bottom).offset(-10);
    }];
    UIColor *bgColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"over_bg"]];
    self.contentView.backgroundColor = bgColor;
}
@end
