//
//  ShouldHelpIndexViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/15.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ShouldHelpIndexViewController.h"
#import "MJRefresh.h"
#import "ShouldHelpTableCellView.h"
#import "ShouldHelpModel.h"
#import "RefreshTableView.h"

@interface ShouldHelpIndexViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) RefreshTableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataSet;
@end

@implementation ShouldHelpIndexViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupView];
}

- (void)setupView{
    [self initNav];
    [self initTableView];
}

- (void)initNav{
    self.title = @"应援";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStyleDone target:self action:@selector(search)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)search{
    NSLog(@"search");
}

- (void)initTableView{
    _tableView = [[RefreshTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain type:BOTH];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"ShouldHelpTableCellView" bundle:nil] forCellReuseIdentifier:@"shouldCell"];
    _tableView.separatorInset= UIEdgeInsetsZero;
    _tableView.estimatedRowHeight = 82;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:_tableView];
    IMP_BLOCK_SELF(ShouldHelpIndexViewController)
    self.tableView.refresh = ^{
        [block_self requestData:1];
    };
    self.tableView.loading = ^{
        [block_self requestData:2];
    };
    // 马上进入刷新状态
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark net request
- (void)requestData:(NSInteger)type{
    [self test:type];
}

- (void)test:(NSInteger)type{
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self initData:type];
    });
}

- (void)initData:(NSInteger)type{
    if(type == 1 || _dataSet == nil){
        _dataSet = [[NSMutableArray alloc] initWithCapacity:20];
    }
    for (NSInteger i = 0; i < 20; i++) {
        NSLog(@"initData i=%tu",i);
        ShouldHelpModel *model = [[ShouldHelpModel alloc] init];
        [_dataSet addObject:model];
    }
    [_tableView reloadData];
    [_tableView.mj_header endRefreshing];
    [_tableView.mj_footer endRefreshing];
    NSLog(@"COUNT = %tu",[_dataSet count]);
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"heightForRowAtIndexPath %lu",indexPath.row);
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSLog(@"tableView count=%tu",self.dataSet.count);
    return self.dataSet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"cellForRowAtIndexPath %lu",indexPath.row);
    ShouldHelpTableCellView *cell = [ShouldHelpTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
