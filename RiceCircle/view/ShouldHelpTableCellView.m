//  应援tablecell
//  ShouldHelpTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/15.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ShouldHelpTableCellView.h"

@interface ShouldHelpTableCellView ()
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

@implementation ShouldHelpTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"shouldCell";
    ShouldHelpTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[ShouldHelpTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    _superViewRightConstraint.constant = 8;
    _bgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"over_bg"]];
    [_imgAttention setUserInteractionEnabled:YES];
    [_imgAttention addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(attention:)]];
    _progress.progress = 0.5f;
}

-(void)attention:(UITapGestureRecognizer* )gestureRecognizer{
    NSLog(@"点击关注");
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setupUI];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
