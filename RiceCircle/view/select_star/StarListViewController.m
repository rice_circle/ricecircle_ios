//  明星列表界面
//  StarListViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/25.
//  Copyright © 2017年 孙继常. All rights reserved.
//
#import "Masonry.h"
#import "StarListViewController.h"
#import "MJRefresh.h"
#import "StarTableCellView.h"

@interface StarListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataSet;
@property (nonatomic,assign) NSInteger type;
@end

@implementation StarListViewController

- (instancetype)initWithType:(NSInteger)type{
    self = [super init];
    if(self){
        _type = type;
    }
    return self;
}

- (void)viewDidLoad {
    NSLog(@"viewDidLoad type=%tu",_type);
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self initData];
    self.title = @"选择明星";
    [self initTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initData:(NSInteger)type{
    if(type == 1 || _dataSet == nil){
        _dataSet = [[NSMutableArray alloc] initWithCapacity:20];
    }
    for (NSInteger i = 0; i < 20; i++) {
        NSLog(@"initData i=%tu",i);
        Star *star = [[Star alloc] init];
        star.name = [NSString stringWithFormat:@"%tu张三%tu",_type,i];
        star.fans = i;
        star.relation = i % 2 == 0 ? YES : NO;
        [_dataSet addObject:star];
    }
    [_tableView reloadData];
    [_tableView.mj_header endRefreshing];
    [_tableView.mj_footer endRefreshing];
    NSLog(@"COUNT = %tu",[_dataSet count]);
}

- (void)initTableView{
    NSInteger height = SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT - 75.5;
    height = SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT;
    NSLog(@"height=%tu",height);
    UITableView *tableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    tableview.delegate = self;
    tableview.dataSource = self;
    tableview.separatorInset= UIEdgeInsetsZero;
    tableview.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableview.estimatedRowHeight = 82;
    tableview.rowHeight = UITableViewAutomaticDimension;
    tableview.separatorColor = [UIColor redColor];
    self.tableView = tableview;
    self.tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:tableview];
    IMP_BLOCK_SELF(StarListViewController);
    MJRefreshNormalHeader *header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData:1];
    }];
    // 设置文字
    [header setTitle:@"下拉可刷新" forState:MJRefreshStateIdle];
    [header setTitle:@"松开立即刷新" forState:MJRefreshStatePulling];
    [header setTitle:@"加载中..." forState:MJRefreshStateRefreshing];
    
    [header.lastUpdatedTimeLabel setText:@"最后更新"];
    
    self.tableView.mj_header = header;
    // 马上进入刷新状态
    [self.tableView.mj_header beginRefreshing];
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self requestData:2];
    }];
    [footer setTitle:@"点击或上拉加载更多" forState:MJRefreshStateIdle];
    [footer setTitle:@"正在加载更多数据..." forState:MJRefreshStateRefreshing];
    [footer setTitle:@"已全部加载完毕" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    [footer setAutomaticallyHidden:YES];
    
//    [_tableView makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.view).offset(0);
//        make.width.equalTo(SCREEN_WIDTH);
//        make.height.equalTo(500);
//    }];
}

- (void)requestData:(NSInteger)type{
    [self test:type];
}

- (void)test:(NSInteger)type{
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self initData:type];
    });
}

//修改大小
- (void)updateSize:(CGRect)frame{
    self.view.frame = CGRectMake(frame.origin.x, 0, SCREEN_WIDTH, frame.size.height);
    _tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, frame.size.height);
}

#pragma UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"heightForRowAtIndexPath %lu",indexPath.row);
//    StarTableCellView *cell = (StarTableCellView*)[self tableView:_tableView cellForRowAtIndexPath:indexPath];
    //return [StarTableCellView heightForCell];
//    return [cell heightForCell2];
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSLog(@"tableView count=%tu",self.dataSet.count);
    return self.dataSet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"cellForRowAtIndexPath %lu",indexPath.row);
    StarTableCellView *cell = [StarTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    Star *star = [_dataSet objectAtIndex:[indexPath row]];
    [cell assignment:star];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
