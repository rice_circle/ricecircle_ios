//  明星详情界面
//  StarDetailViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/20.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "StarDetailViewController.h"
#import "RelatedFansViewController.h"
#import "RelatedStarViewController.h"
#import "RelatedIntroductionViewController.h"

@interface StarDetailViewController ()<TabIndicatroItemDelegate,UIScrollViewDelegate>
@property (nonatomic,strong) NSMutableArray *views;
@property (nonatomic,assign) BOOL needScroll;
@end

@implementation StarDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // Do any additional setup after loading the view from its nib.
    [self setupViews];
    
}

- (void)setupViews{
    _needScroll = YES;
    NSArray *title = @[@"相关活动",@"相关粉丝团",@"明星简介"];
    _indicatorView.needCorner = YES;
    _indicatorView.indicatorDelegate = self;
    [_indicatorView setDataSet:title];
    
    //UIScrollView
    _contentView.alwaysBounceHorizontal = NO;
    _contentView.backgroundColor = [UIColor greenColor];
    _contentView.contentSize = CGSizeMake(SCREEN_WIDTH * _views.count, 0);
    _contentView.pagingEnabled = YES;
    _contentView.delegate = self;
    NSLog(@"contentview frame=%@",NSStringFromCGRect(_contentView.frame));
    [self selectVC:0];
}

- (void)selectVC:(NSInteger )index{
    UIViewController *vc = [_views objectAtIndex:index];
    if(index == 0){
        [((RelatedStarViewController *)vc) updateSize:_contentView.frame];
    }else if(index == 1){
        [((RelatedFansViewController *)vc) updateSize:CGRectMake(_contentView.frame.size.width, _contentView.frame.origin.y, _contentView.frame.size.width, _contentView.frame.size.height)];
    }else if(index == 2){
        [((RelatedIntroductionViewController *)vc) updateSize:CGRectMake(_contentView.frame.size.width * 2, _contentView.frame.origin.y, _contentView.frame.size.width, _contentView.frame.size.height)];
    }
    [self addChildViewController:vc];
    [_contentView addSubview:vc.view];
}

- (void)initData{
    _views = [NSMutableArray array];
    for (NSInteger i = 0; i < 3; i++) {
        switch (i) {
            case 0:{
                RelatedStarViewController *startVC = [[RelatedStarViewController alloc] init];
                //[startVC.view setFrame:_contentView.frame];
                [_views addObject:startVC];
            }
                break;
                
            case 1:{
                RelatedFansViewController *fansVC = [[RelatedFansViewController alloc] init];
                [_views addObject:fansVC];
            }
                break;
            case 2:{
                RelatedIntroductionViewController *introductionVC = [[RelatedIntroductionViewController alloc] init];
                [_views addObject:introductionVC];
            }
                break;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TabIndicatroItemDelegate
- (void)indicatorItemClick:(NSInteger)index{
    NSLog(@"StarDetailViewController indicatorItemClick index=%lu",index);
    _needScroll = NO;
    [_contentView setContentOffset:CGPointMake((index - 1) * SCREEN_WIDTH, 0) animated:YES];
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"StarDetailViewController scrollViewDidScroll _needScroll=%d",_needScroll);
    CGFloat index = scrollView.contentOffset.x / SCREEN_WIDTH;
    [self selectVC:index];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(decelerate == NO){
        [self scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(nonnull UIScrollView *)scrollView{
    // 在这里面写scrollView停止时需要做的事情
    NSLog(@"UIScrollView停止滚动了");
    CGFloat index = scrollView.contentOffset.x / SCREEN_WIDTH;
    if(_needScroll){
        _indicatorView.currentIndex = index;
    }
    _needScroll = YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
