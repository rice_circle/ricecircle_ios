//  明星简介
//  RelatedIntroductionViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/24.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "RelatedIntroductionViewController.h"
#import "UIColor+Hex.h"

@interface RelatedIntroductionViewController ()
@property (nonatomic,strong) UILabel *lblContent;
@end

@implementation RelatedIntroductionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupViews];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupViews{
    _lblContent = [[UILabel alloc] init];
    _lblContent.font = [UIFont systemFontOfSize:12];
    _lblContent.textColor = [UIColor colorWithHexString:@"#333333"];
    _lblContent.numberOfLines = 0;
    _lblContent.lineBreakMode = NSLineBreakByWordWrapping;
    _lblContent.text = @"是连接方式来得到了解放军考虑是否健康了忘记了色加快立法健康绿色空间里发健康绿色健康了 圣诞快乐分开了色考虑解放军俄罗斯理解是来看肌肤色家里快放假了可是 放色加快立法健康绿色交流看法色饭角色就哭了飞机了苦涩发了疯了教科书家里快放假了科技粉色礼服色镂空粉色饭角色就发了角色就发了色富乐山风景饿死了风景饿死了粉色风景饿死飞极乐世界粉色露肩风景饿死肌肤就俄罗斯饭角色就风景饿死就放假了色家粉丝交流";
    _lblContent.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_lblContent];
    [_lblContent makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(16);
        make.right.equalTo(self.view).offset(-16);
        make.top.equalTo(self.view).offset(0);
    }];
}

/**
 更新ViewController的大小
 
 @param frame 位置大小
 */
- (void)updateSize:(CGRect)frame{
    self.view.frame = CGRectMake(frame.origin.x, 0, SCREEN_WIDTH, frame.size.height);
    //_lblContent.frame = CGRectMake(0, 0, SCREEN_WIDTH, frame.size.height);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
