//
//  FansTableCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/24.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@interface FansTableCellView : BaseTableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *superViewBottomConstraint;

@end
