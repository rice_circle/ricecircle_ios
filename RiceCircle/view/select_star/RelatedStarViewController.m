//  相关明星
//  RelatedStarViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/23.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "RelatedStarViewController.h"
#import "RefreshTableView.h"
#import "ProductTableCellView.h"
#import "MJRefresh.h"

@interface RelatedStarViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) RefreshTableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataSet;
@end

@implementation RelatedStarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    // Do any additional setup after loading the view.
    [self setupViews];
    self.view.backgroundColor = [UIColor redColor];
}

- (void)initData{
    _dataSet = [NSMutableArray array];
    for (NSInteger i = 0; i < 8; i ++) {
        [_dataSet addObject:[NSString stringWithFormat:@"我是%lu",i]];
    }
}

- (void)setupViews{
    CGRect frame = [self.view frame];
    NSLog(@"RelatedStarViewController frame=%@",NSStringFromCGRect(frame));
    _tableView = [[RefreshTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, frame.size.height) style:UITableViewStylePlain type:BOTH];
    [self.tableView registerNib:[UINib nibWithNibName:@"ProductTableCellView" bundle:nil] forCellReuseIdentifier:@"productCell"];
    _tableView.estimatedRowHeight = 80;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    IMP_BLOCK_SELF(RelatedStarViewController)
    self.tableView.refresh = ^{
        [block_self requestData:1];
    };
    self.tableView.loading = ^{
        [block_self requestData:2];
    };
    [self.view addSubview:_tableView];
}

- (void)requestData:(NSInteger)type{
    [_tableView.mj_header endRefreshing];
    [_tableView.mj_footer endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateSize:(CGRect)frame{
    self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, frame.size.height);
//    [self setupViews];
    _tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, frame.size.height);
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}

#pragma mark UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProductTableCellView *cell = [ProductTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSLog(@"RelatedStarViewController _dataSet.count=%lu",_dataSet.count);
    return _dataSet.count;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
