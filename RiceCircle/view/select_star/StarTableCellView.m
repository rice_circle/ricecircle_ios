//
//  StarTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/25.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "StarTableCellView.h"
#import "Masonry.h"
#import "StarDetailViewController.h"

@interface StarTableCellView()
@property (nonatomic,strong) UIImageView *imgHead;
@property (nonatomic,strong) UILabel *lblName;
@property (nonatomic,strong) UILabel *lblFans;
@property (nonatomic,strong) UIImageView *imgBind;
@end

@implementation StarTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    static NSString *ID = @"cell";
    StarTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[StarTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    if (self) {
        _imgHead = [[UIImageView alloc] init];
        _lblName = [[UILabel alloc] init];
        _lblName.font = SC_FONT_14;
        _lblName.textColor = SC_Color_Black;
        CGFloat maxWidth = SCREEN_WIDTH - 150;
        _lblName.preferredMaxLayoutWidth = maxWidth;
        _lblFans = [[UILabel alloc] init];
        _lblFans.font = SC_FONT_12;
        _lblFans.numberOfLines = 0;
        _lblFans.preferredMaxLayoutWidth = maxWidth;
        _imgBind = [[UIImageView alloc] init];
        [self.contentView addSubview:_imgHead];
        [self.contentView addSubview:_lblName];
        [self.contentView addSubview:_lblFans];
        [self.contentView addSubview:_imgBind];
        [_imgHead makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.top).offset(16);
            make.bottom.equalTo(self.contentView.bottom).offset(-16);
            make.left.equalTo(self.contentView.left).offset(16);
            make.width.equalTo(50);
            make.height.equalTo(50);
        }];
        [_lblName makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.top).offset(21);
            make.left.equalTo(_imgHead.right).offset(10);
            make.height.equalTo(20);
        }];
        [_lblFans makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_lblName.bottom).offset(2);
            make.left.equalTo(_imgHead.right).offset(10);
            make.height.equalTo(20);
        }];
        [_imgBind makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.right).offset(-16);
            make.centerY.equalTo(self.centerY);
            make.height.equalTo(20);
            make.width.equalTo(22);
        }];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
        [self.contentView addGestureRecognizer:tapGesture];
    }
    return self;
}

- (void)click{
    if(_viewController != nil){
        StarDetailViewController *detailVC = [[StarDetailViewController alloc] init];
        detailVC.hidesBottomBarWhenPushed = YES;
        [_viewController.navigationController pushViewController:detailVC animated:YES];
    }
}

//赋值到cell
- (void)assignment:(Star *)star{
    [_lblName setText:star.name];
    [_lblFans setText:[NSString stringWithFormat:@"粉丝数 %tu",star.fans]];
    [_imgHead setImage:[UIImage imageNamed:@"head"]];
    if(star.relation){
        [_imgBind setImage:[UIImage imageNamed:@"has_store"]];
    }else{
        [_imgBind setImage:[UIImage imageNamed:@"store"]];
    }
}

//获取cell的高度
+ (CGFloat)heightForCell{
    return 82;
}

//获取cell的高度
- (CGFloat)heightForCell2{
    return 82;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
