//  明星选择界面
//  SelectStarViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/23.
//  Copyright © 2017年 孙继常. All rights reserved.
//
// 添加这个宏定义，以后在使用 Masonry 框架中的属性和方法的时候, 就可以省略mas_前缀
#define MAS_SHORTHAND
// 添加这个宏定义，就可以让 equalTo 方法就等价于 mas_equalTo 方法
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"
#import "SelectStarViewController.h"
#import "SCNavTabBar.h"
#import "StarListViewController.h"

@interface SelectStarViewController ()<SCNavTabBarDelegate,UIScrollViewDelegate>
@property (nonatomic, strong) NSMutableArray *subViewControllers;
@property (nonatomic,strong) NSArray *titles;
@property (nonatomic,strong) UIScrollView *contentView;
@property (nonatomic,strong) SCNavTabBar *navTabBar;
@property NSInteger currentIndex;
@end

@implementation SelectStarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initNav];
    [self initData];
    [self initViews];
    [self loadFirstViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initData{
    _titles = @[@"华语明星",@"日韩明星",@"欧美明星",@"亚太地区",@"非洲明星",@"东南亚地区"];
    _titles = @[@"华语明星"];
    _subViewControllers = [NSMutableArray array];
    for (NSInteger i = 0; i < _titles.count; i ++) {
        StarListViewController *starListVC = [[StarListViewController alloc] initWithType:i];
        starListVC.title = _titles[i];
        [_subViewControllers addObject:starListVC];
    }
}

//初始化navgation
- (void)initNav{
    self.title = @"选择明星";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(complete)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)complete{

}

//初始化view
- (void)initViews{
    [self.view setBackgroundColor:[UIColor darkGrayColor]];
    //初始化搜索框
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    searchBar.placeholder=@"搜索喜欢的明星";
    UITapGestureRecognizer *tagGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tagClick)];
    searchBar.barTintColor = [UIColor whiteColor];
    [searchBar addGestureRecognizer:tagGesture];
    [self.view addSubview:searchBar];
    searchBar.hidden = YES;
    CGFloat navHeight = self.navigationController.navigationBar.frame.size.height;
    CGFloat NavStateBarHeight = navHeight+STATUS_BAR_HEIGHT;
    CGFloat searchBarHeight = 32;
    CGFloat navTabBarHeight = 41.5;
    [searchBar makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.view).offset(0);
        make.trailing.equalTo(self.view).offset(0);
        make.height.equalTo(searchBarHeight);
        make.top.equalTo(self.view).offset(NavStateBarHeight);
    }];
    //添加滑动条
    _navTabBar = [[SCNavTabBar alloc] init];
    _navTabBar.itemTitles=_titles;
    _navTabBar.showSplitLine = NO;
    _navTabBar.backgroundColor=[UIColor whiteColor];
    _navTabBar.lineColor = [UIColor redColor];
    _navTabBar.delegate = self;
    [self.view addSubview:_navTabBar];
    _navTabBar.hidden = YES;
    CGFloat navTabBarMarginTop = 5;
    [_navTabBar updateData];
    navTabBarHeight = _navTabBar.calcHeight;
    [_navTabBar makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(searchBar.bottom).offset(navTabBarMarginTop);
        make.width.equalTo(SCREEN_WIDTH);
        make.height.equalTo(navTabBarHeight);
    }];
    //添加内容区域
    _contentView = [[UIScrollView alloc] init];
    _contentView.showsHorizontalScrollIndicator = NO;
    _contentView.showsVerticalScrollIndicator = NO;
    _contentView.delegate = self;
    _contentView.pagingEnabled = YES;
    _contentView.contentSize = CGSizeMake(SCREEN_WIDTH * _subViewControllers.count, 0);
    [self.view addSubview:_contentView];
    CGFloat contentMarginTop = 5;
    NSInteger height = SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT - searchBarHeight - navTabBarHeight - navTabBarMarginTop - contentMarginTop;
    height = SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT;
    [_contentView makeConstraints:^(MASConstraintMaker *make) {
        //make.top.equalTo(_navTabBar.bottom).offset(contentMarginTop);
        make.top.equalTo(self.view).offset(STATUS_AND_NAVIGATION_HEIGHT);
        make.width.equalTo(SCREEN_WIDTH);
        make.height.equalTo(height);
    }];
}

- (void)loadFirstViewController{
    //首先加载第一个视图
    StarListViewController *viewController = (StarListViewController *)_subViewControllers[0];
    CGFloat height = SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT - 75.5;
    height = SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT;
    CGFloat y = STATUS_AND_NAVIGATION_HEIGHT - 75.5;
    y = STATUS_AND_NAVIGATION_HEIGHT;
    [viewController updateSize:CGRectMake(0, y, SCREEN_HEIGHT, height)];
    [_contentView addSubview:viewController.view];
    [self addChildViewController:viewController];
}

- (void)tagClick{

}

#pragma SCNavTabBarDelegate
- (void)itemDidSelectedWithIndex:(NSInteger)index{

}

- (void)itemDidSelectedWithIndex:(NSInteger)index withCurrentIndex:(NSInteger)currentIndex{
    NSLog(@"itemDidSelectedWithIndex ");
    if (currentIndex - index >= 2 || currentIndex - index <= -2) {
        [_contentView setContentOffset:CGPointMake(index * SCREEN_WIDTH, 0) animated:NO];
    }else{
        [_contentView setContentOffset:CGPointMake(index * SCREEN_WIDTH, 0) animated:YES];
    }
}

#pragma UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"scrollViewDidScroll");
    _currentIndex = scrollView.contentOffset.x / SCREEN_WIDTH;
    _navTabBar.currentItemIndex = _currentIndex;
    
    /** 当scrollview滚动的时候加载当前视图 */
    StarListViewController *viewController = (StarListViewController *)_subViewControllers[_currentIndex];
    CGFloat y = STATUS_AND_NAVIGATION_HEIGHT - 75.5;
    [viewController updateSize:CGRectMake(_currentIndex * SCREEN_WIDTH, y, SCREEN_HEIGHT, _contentView.frame.size.height)];
    [_contentView addSubview:viewController.view];
    [self addChildViewController:viewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
