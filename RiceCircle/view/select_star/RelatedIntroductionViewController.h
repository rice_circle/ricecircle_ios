//
//  RelatedIntroductionViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/24.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"

@interface RelatedIntroductionViewController : BaseUIViewController

/**
 更新ViewController的大小
 
 @param frame 位置大小
 */
- (void)updateSize:(CGRect)frame;

@end
