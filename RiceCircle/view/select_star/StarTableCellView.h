//
//  StarTableCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/25.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Star.h"

@interface StarTableCellView : UITableViewCell

@property (nonatomic,weak) UIViewController *viewController;

//创建cell
+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index;

//赋值
- (void)assignment:(Star*)star;

//获取cell的高度
+ (CGFloat)heightForCell;

//获取cell的高度
- (CGFloat)heightForCell2;
@end
