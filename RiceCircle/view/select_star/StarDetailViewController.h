//
//  StarDetailViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/20.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"
#import "TabIndicatorView.h"

@interface StarDetailViewController : BaseUIViewController
//头像
@property (weak, nonatomic) IBOutlet UIImageView *imgHead;
//姓名
@property (weak, nonatomic) IBOutlet UILabel *lblName;
//是否已关注
@property (weak, nonatomic) IBOutlet UIImageView *imgAttention;
//封面图片
@property (weak, nonatomic) IBOutlet UIImageView *imgCover;
//指示器
@property (weak, nonatomic) IBOutlet TabIndicatorView *indicatorView;
//内容区域
@property (weak, nonatomic) IBOutlet UIScrollView *contentView;

@end
