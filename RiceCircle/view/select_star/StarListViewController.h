//
//  StarListViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/25.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"

@interface StarListViewController : BaseUIViewController
- (instancetype)initWithType:(NSInteger)type;

- (void)updateSize:(CGRect)frame;
@end
