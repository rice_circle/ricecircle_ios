//
//  OrderCenterViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/4.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "OrderCenterViewController.h"
#import "SCNavTabBar.h"
#import "TabIndicatorView.h"
#import "OrderListViewController.h"

@interface OrderCenterViewController ()<TabIndicatroItemDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet TabIndicatorView *indicatorView;
@property (weak, nonatomic) IBOutlet UIScrollView *contentView;
@property (nonatomic,strong) NSMutableArray *subViewControllers;
@property (nonatomic,strong) NSArray *titles;
@property (nonatomic,assign) BOOL needScroll;
@end

@implementation OrderCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self initData];
    [self setupViews];
}

- (void)initData{
    _titles = @[@"全部",@"已支付",@"待支付",@"失效"];
    _subViewControllers = [NSMutableArray array];
    for (NSInteger i = 0; i < _titles.count; i ++) {
        OrderListViewController *starListVC = [[OrderListViewController alloc] initWithType:i];
        starListVC.title = _titles[i];
        [_subViewControllers addObject:starListVC];
    }
}

- (void)setupViews{
    [self initNav];
    _needScroll = YES;
    _indicatorView.needCorner = YES;
    _indicatorView.indicatorDelegate = self;
    [_indicatorView setDataSet:_titles];
    
    _contentView.showsHorizontalScrollIndicator = NO;
    _contentView.showsVerticalScrollIndicator = NO;
    _contentView.delegate = self;
    _contentView.pagingEnabled = YES;
    _contentView.contentSize = CGSizeMake(SCREEN_WIDTH * _subViewControllers.count, 0);
    [self selectVC:0];
}

- (void)initNav{
    self.title = @"我的订单";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)selectVC:(NSInteger )index{
    OrderListViewController *vc = (OrderListViewController*)[_subViewControllers objectAtIndex:index];
    [vc updateSize:CGRectMake(_contentView.frame.size.width * index, _contentView.frame.origin.y, _contentView.frame.size.width, _contentView.frame.size.height)];
    [self addChildViewController:vc];
    [_contentView addSubview:vc.view];
}

#pragma mark TabIndicatroItemDelegate
- (void)indicatorItemClick:(NSInteger)index{
    NSLog(@"StarDetailViewController indicatorItemClick index=%lu",index);
    _needScroll = NO;
    [_contentView setContentOffset:CGPointMake((index - 1) * SCREEN_WIDTH, 0) animated:YES];
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"StarDetailViewController scrollViewDidScroll _needScroll=%d",_needScroll);
    CGFloat index = scrollView.contentOffset.x / SCREEN_WIDTH;
    [self selectVC:index];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(decelerate == NO){
        [self scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(nonnull UIScrollView *)scrollView{
    // 在这里面写scrollView停止时需要做的事情
    NSLog(@"UIScrollView停止滚动了");
    CGFloat index = scrollView.contentOffset.x / SCREEN_WIDTH;
    if(_needScroll){
        _indicatorView.currentIndex = index;
    }
    _needScroll = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
