//   订单详情界面
//  OrderDetailViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/4.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "UIColor+Hex.h"
#import "OrderInfoModel.h"
#import "OrderDetailItemTableCellView.h"
#import "OrderDetailBottomTableCellView.h"

@interface OrderDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataSet;
@end

@implementation OrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self initData];
    [self setupViews];
}

- (void)initData{
    _dataSet = [NSMutableArray array];
    for (NSInteger i = 0 ; i < 5; i++) {
        OrderInfoModel *info = [[OrderInfoModel alloc] init];
        [_dataSet addObject:info];
    }
}

- (void)setupViews{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT) style:UITableViewStylePlain];
    _tableView.separatorColor = [UIColor colorWithHexString:@"#f8f8f8"];
    _tableView.separatorInset = UIEdgeInsetsMake(0, 16, 0, 0);
    _tableView.delegate = self;
    _tableView.estimatedRowHeight = 100;
    _tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderDetailItemTableCellView" bundle:nil] forCellReuseIdentifier:@"orderDetailCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderDetailBottomTableCellView" bundle:nil] forCellReuseIdentifier:@"orderDetailBottomCell"];
    [self.view addSubview:_tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"heightForRowAtIndexPath %lu",indexPath.row);
    return UITableViewAutomaticDimension;
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSLog(@"tableView count=%tu",self.dataSet.count);
    return self.dataSet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"cellForRowAtIndexPath %lu",indexPath.row);
    if(indexPath.row < _dataSet.count - 1){
        OrderDetailItemTableCellView *cell = [OrderDetailItemTableCellView cellWithTableView:tableView withIndex:indexPath.row];
        return cell;
    }
    OrderDetailBottomTableCellView *cell = [OrderDetailBottomTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
