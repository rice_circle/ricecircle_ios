//
//  OrderListViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/4.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"

@interface OrderListViewController : BaseUIViewController


/**
 根据type初始化

 @param type 类型
 */
- (instancetype)initWithType:(NSInteger)type;


/**
 修改ViewController的大小和位置

 @param frame 大小和位置
 */
- (void)updateSize:(CGRect)frame;

@end
