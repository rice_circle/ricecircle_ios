//
//  OrderTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/4.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "OrderTableCellView.h"

@interface OrderTableCellView()
//发布人名称
@property (weak, nonatomic) IBOutlet UILabel *lblPublisherName;
//订单状态
@property (weak, nonatomic) IBOutlet UILabel *lblOrderStatus;
//商品图片
@property (weak, nonatomic) IBOutlet UIImageView *imgProductImage;
//商品名称
@property (weak, nonatomic) IBOutlet UILabel *imgProductName;
//商品规格
@property (weak, nonatomic) IBOutlet UILabel *lblProductSpecification;
//价格
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
//数量
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
//订单编号
@property (weak, nonatomic) IBOutlet UILabel *lblOrderId;
//总价
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPrice;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *superBottomConstraint;

@end

@implementation OrderTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"orderCell";
    OrderTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[OrderTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews{
    self.contentView.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _superBottomConstraint.constant = 15;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setupViews];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
