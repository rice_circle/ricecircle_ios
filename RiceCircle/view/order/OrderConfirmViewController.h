//
//  OrderConfirmViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"

@interface OrderConfirmViewController : BaseUIViewController
//商品图片
@property (weak, nonatomic) IBOutlet UIImageView *imgProductPic;
//商品名称
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
//类型名称
@property (weak, nonatomic) IBOutlet UILabel *lblType;
//单价
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
//购买数量
@property (weak, nonatomic) IBOutlet UILabel *lblBuyCount;
//总价
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UIView *layoutAddress;

@end
