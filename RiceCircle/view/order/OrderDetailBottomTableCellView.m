//
//  OrderDetailBottomTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/5.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "OrderDetailBottomTableCellView.h"

@interface OrderDetailBottomTableCellView()
//快递费用
@property (weak, nonatomic) IBOutlet UILabel *lblTracking;
//总计费用
@property (weak, nonatomic) IBOutlet UILabel *lblTotalPrice;
//收货人
@property (weak, nonatomic) IBOutlet UILabel *lblSignUpPeople;
//收货人电话
@property (weak, nonatomic) IBOutlet UILabel *lblSignUpPhone;
//收获地址
@property (weak, nonatomic) IBOutlet UILabel *lblSignUpAddress;
//订单状态
@property (weak, nonatomic) IBOutlet UILabel *lblOrderStatus;
//下单时间
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCreateTime;
//订单编号
@property (weak, nonatomic) IBOutlet UILabel *lblOrderId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *superBottonConstraint;
//支付按钮
@property (weak, nonatomic) IBOutlet UIButton *btnPay;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payButtonHeight;

@end


@implementation OrderDetailBottomTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"orderDetailBottomCell";
    OrderDetailBottomTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[OrderDetailBottomTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setupViews];
}

- (void)setupViews{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _superBottonConstraint.constant = 15;
    _btnPay.backgroundColor = SC_Color_Red;
    [_btnPay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _btnPay.layer.cornerRadius = _btnPay.frame.size.height / 2.0f;
    _payButtonHeight.constant = 0;
}
- (IBAction)pay:(id)sender {
    NSLog(@"pay");
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
