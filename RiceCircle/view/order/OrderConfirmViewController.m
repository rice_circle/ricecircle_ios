//  订单确认界面
//  OrderConfirmViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "OrderConfirmViewController.h"
#import "PayConfirmViewController.h"
#import "SelectAddressViewController.h"

@interface OrderConfirmViewController ()

@end

@implementation OrderConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view from its nib.
    [self setupViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupViews{
    [self initNav];
    //_layoutAddress.hidden = YES;
    //[_layoutAddress updateConstraints:^(MASConstraintMaker *make) {
    //    make.height.equalTo(0);
    //}];
    //[_layoutAddress layoutIfNeeded];
    UITapGestureRecognizer *selectGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoSelectAddress)];
    [_layoutAddress addGestureRecognizer:selectGesture];
}

- (void)initNav{
    self.title = @"确认订单";
}

//修改收货地址
- (void)gotoSelectAddress{
    SelectAddressViewController *selectVC = [[SelectAddressViewController alloc] init];
    selectVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:selectVC animated:YES];
}

//提交订单
- (IBAction)submitOrder:(id)sender {
    PayConfirmViewController *payVC = [[PayConfirmViewController alloc] init];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:payVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
