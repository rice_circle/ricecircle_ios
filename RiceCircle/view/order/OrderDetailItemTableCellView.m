//
//  OrderDetailItemTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/4.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "OrderDetailItemTableCellView.h"

@interface OrderDetailItemTableCellView()
//商品图片
@property (weak, nonatomic) IBOutlet UIImageView *imgProductPic;
//商品名称
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
//价格
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
//发布人
@property (weak, nonatomic) IBOutlet UILabel *lblPublisher;
//商品规格
@property (weak, nonatomic) IBOutlet UILabel *lblProductSpecification;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *superBottomConstraint;

@end

@implementation OrderDetailItemTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"orderDetailCell";
    OrderDetailItemTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[OrderDetailItemTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setupViews];
}

- (void)setupViews{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _superBottomConstraint.constant = 15;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
