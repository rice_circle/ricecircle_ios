//
//  PayConfirmViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"

@interface PayConfirmViewController : BaseUIViewController
//剩余时间
@property (weak, nonatomic) IBOutlet UILabel *lblLimtTime;
//支付金额
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@end
