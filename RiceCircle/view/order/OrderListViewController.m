//
//  OrderListViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/4.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "OrderListViewController.h"
#import "RefreshTableView.h"
#import "MJRefresh.h"
#import "OrderInfoModel.h"
#import "OrderTableCellView.h"
#import "OrderDetailViewController.h"

@interface OrderListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) RefreshTableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataSet;
@property (nonatomic,assign) NSInteger type;
@end

@implementation OrderListViewController

- (instancetype)initWithType:(NSInteger)type{
    self = [super init];
    if(self){
        _type = type;
    }
    return self;
}

- (void)updateSize:(CGRect)frame{
    self.view.frame = CGRectMake(frame.origin.x, 0, SCREEN_WIDTH, frame.size.height);
    _tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, frame.size.height);
}

- (void)setupViews{
    _tableView = [[RefreshTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) style:UITableViewStylePlain type:BOTH];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderTableCellView" bundle:nil] forCellReuseIdentifier:@"orderCell"];
    _tableView.separatorInset= UIEdgeInsetsZero;
    _tableView.estimatedRowHeight = 82;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:_tableView];
    IMP_BLOCK_SELF(OrderListViewController)
    self.tableView.refresh = ^{
        [block_self requestData:1];
    };
    self.tableView.loading = ^{
        [block_self requestData:2];
    };
    // 马上进入刷新状态
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setupViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark net request
- (void)requestData:(NSInteger)type{
    [self test:type];
}

- (void)test:(NSInteger)type{
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self initData:type];
    });
}

- (void)initData:(NSInteger)type{
    if(type == 1 || _dataSet == nil){
        _dataSet = [[NSMutableArray alloc] initWithCapacity:20];
    }
    for (NSInteger i = 0; i < 20; i++) {
        NSLog(@"initData i=%tu",i);
        OrderInfoModel *model = [[OrderInfoModel alloc] init];
        [_dataSet addObject:model];
    }
    [_tableView reloadData];
    [_tableView.mj_header endRefreshing];
    [_tableView.mj_footer endRefreshing];
    NSLog(@"COUNT = %tu",[_dataSet count]);
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"heightForRowAtIndexPath %lu",indexPath.row);
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderDetailViewController *detailVC = [[OrderDetailViewController alloc] init];
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSLog(@"tableView count=%tu",self.dataSet.count);
    return self.dataSet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"cellForRowAtIndexPath %lu",indexPath.row);
    OrderTableCellView *cell = [OrderTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
