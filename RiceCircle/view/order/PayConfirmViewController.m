//  支付确认界面
//  PayConfirmViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "PayConfirmViewController.h"
#import "NetworkManager.h"
#import "APIConstant.h"
#import "StringModel.h"
#import "PayManager.h"

@interface PayConfirmViewController ()<NetResponseDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblWechat;
@property (weak, nonatomic) IBOutlet UILabel *lblAlipay;

@end

@implementation PayConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setupViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupViews{
    [self intiNav];
    _lblAlipay.userInteractionEnabled = YES;
    UITapGestureRecognizer *alpayGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(alipay)];
    [_lblAlipay addGestureRecognizer:alpayGesture];
}

- (void)alipay{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:[NSNumber numberWithInt:1] forKey:@"orderId"];
    [params setValue:@"alipay" forKey:@"payType"];
    [[NetworkManager shareInstance] sendRequestWithMethod:POST withPath:API_ObtainOrderInfo withParams:params withCls:[StringModel class] withDelegate:self];
}

- (void)intiNav{
    self.title = @"支付确认";
}

#pragma mark NetResponseDelegate
- (void)success:(id)result url:(NSString *)url{
    NSString *info = ((StringModel*)result).data;
    NSLog(@"info = %@",info);
    [[PayManager sharedPayManager] alipay:info callbak:^(NSDictionary *dict) {
        
    }];
}

- (void)fail:(NSString *)error url:(NSString *)url{

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
