//  饭圈顶部cell
//  RiceCircleTopTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/28.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "RiceCircleTopTableCellView.h"
#import "StartInfoCollectionCellView.h"
#import "StarDetailViewController.h"
#import "BigBarListViewController.h"

@interface RiceCircleTopTableCellView()<UICollectionViewDelegate,UICollectionViewDataSource>
//顶部图片
@property (weak, nonatomic) IBOutlet UIImageView *imgTop;
//大吧
@property (weak, nonatomic) IBOutlet UIImageView *imgDaBa;
//个站
@property (weak, nonatomic) IBOutlet UIImageView *imgPersonal;
//饭绘
@property (weak, nonatomic) IBOutlet UIImageView *imgDraw;
//饭拍
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UICollectionView *starCollectionView;
@property (nonatomic,strong) NSMutableArray *startDataSet;
@end

@implementation RiceCircleTopTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"rcTopCell";
    RiceCircleTopTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[RiceCircleTopTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self initData];
    [self setupViews];
}

- (void)initData{
    _startDataSet = [NSMutableArray array];
    for (NSInteger i = 0; i < 10; i++) {
        [_startDataSet addObject:[NSString stringWithFormat:@"%lu",i]];
    }
}

- (void)setupViews{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    // 设置UICollectionView为横向滚动
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    // 每一行cell之间的间距
    //    flowLayout.minimumLineSpacing = 11;
    // 每一列cell之间的间距
    flowLayout.minimumInteritemSpacing = 10;
    // 设置第一个cell和最后一个cell,与父控件之间的间距
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 16, 0, 16);
    flowLayout.estimatedItemSize = CGSizeMake(100, 140);
    _starCollectionView.collectionViewLayout = flowLayout;
    _starCollectionView.delegate = self;
    _starCollectionView.dataSource = self;
    _starCollectionView.pagingEnabled = NO;
    _starCollectionView.showsVerticalScrollIndicator = NO;
    
    _imgPhoto.userInteractionEnabled = YES;
    
    [_starCollectionView registerClass:[StartInfoCollectionCellView class] forCellWithReuseIdentifier:@"startInfoCell"];
    UITapGestureRecognizer *bigBarGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bigBar)];
    [_imgDaBa addGestureRecognizer:bigBarGesture];
    
    UITapGestureRecognizer *personalGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(personal)];
    [_imgPersonal addGestureRecognizer:personalGesture];
    
    UITapGestureRecognizer *drawGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(draw)];
    [_imgDraw addGestureRecognizer:drawGesture];
    
    UITapGestureRecognizer *photoGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photo)];
    [_imgPhoto addGestureRecognizer:photoGesture];
}


//大吧
- (void)bigBar{
    if(nil != _viewController){
        BigBarListViewController *detailVC = [[BigBarListViewController alloc] init];
        detailVC.hidesBottomBarWhenPushed = YES;
        [_viewController.navigationController pushViewController:detailVC animated:YES];
    }
}

//个站
- (void)personal{

}

//饭绘
- (void)draw{

}

//饭拍
- (void)photo{

}

#pragma mark UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(nil != _viewController){
        StarDetailViewController *detailVC = [[StarDetailViewController alloc] init];
        detailVC.hidesBottomBarWhenPushed = YES;
        [_viewController.navigationController pushViewController:detailVC animated:YES];
    }
}

#pragma mark UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSLog(@"collectionView numberOfItemsInSection=%lu",_startDataSet.count);
    return _startDataSet.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    StartInfoCollectionCellView *infoCell = [StartInfoCollectionCellView cellWithTableView:collectionView withIndex:indexPath];
    return infoCell;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
