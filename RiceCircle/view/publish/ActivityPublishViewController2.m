//
//  ActivityPublishViewController2.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/11.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ActivityPublishViewController2.h"
#import "ActivityItemsView.h"

@interface ActivityPublishViewController2 ()
@property (nonatomic,strong) ActivityItemsView *itemView;
@end

@implementation ActivityPublishViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupViews];
    self.view.backgroundColor = [UIColor whiteColor];
}

-(void)setupViews{
    _itemView = [[ActivityItemsView alloc] initWithFrame:CGRectMake(16, 70, 250, 200)];
    [self.view addSubview:_itemView];
    
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeSystem];
    btnAdd.frame = CGRectMake(50, 300, 50, 50);
    [btnAdd setTitle:@"添加" forState:UIControlStateNormal];
    [btnAdd addTarget:self action:@selector(add) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnAdd];
}

- (void)add{
    [_itemView addItem];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
