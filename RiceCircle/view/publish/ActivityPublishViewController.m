//  活动发布界面
//  ActivityPublishViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/11.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ActivityPublishViewController.h"
#import "UIColor+Hex.h"
#import "ActivityItemsView.h"

@interface ActivityPublishViewController ()
//类型view高度约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *itemsHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet ActivityItemsView *itemViews;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation ActivityPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupViews{
    [self initNavigation];
    _btnAdd.layer.cornerRadius = 5;
    _btnAdd.layer.borderColor = [[UIColor colorWithHexString:@"f25f73"] CGColor];
    _btnAdd.layer.borderWidth = 1;
    [_btnAdd setTitleColor:[UIColor colorWithHexString:@"f25f73"] forState:UIControlStateNormal];
    NSLog(@"_itemViews.frame=%@",NSStringFromCGRect(_itemViews.frame));
    _itemsHeightConstraint.constant = _itemViews.frame.size.height;
    [_contentView layoutIfNeeded];
}

//初始化navigation
- (void)initNavigation{
    self.title = @"发布";
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"确定发布" style:UIBarButtonItemStyleDone target:self action:@selector(compelte)];
    self.navigationItem.rightBarButtonItem = rightButton;
}

- (void)compelte{

}

//添加规格
- (IBAction)addItems:(id)sender {
    [_itemViews addItem];
    NSLog(@"_itemViews.frame=%@",NSStringFromCGRect(_itemViews.frame));
    _itemsHeightConstraint.constant = _itemViews.frame.size.height;
    [_contentView layoutIfNeeded];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
