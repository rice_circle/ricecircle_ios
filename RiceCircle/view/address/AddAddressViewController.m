//
//  AddAddressViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/5.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "AddAddressViewController.h"
#import "AreaPickerView.h"
#import "SCCountTextView.h"

@interface AddAddressViewController ()<AreaPickerDelagate>
//收货人姓名
@property (weak, nonatomic) IBOutlet UITextField *txfName;
//收货人电话
@property (weak, nonatomic) IBOutlet UITextField *txfPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblZone;
@property (weak, nonatomic) IBOutlet UILabel *lblZoneContent;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;
//详细地址
@property (weak, nonatomic) IBOutlet SCCountTextView *txfDetail;
//选择设为默认地址
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;
@property (nonatomic,strong) AreaPickerView *pickerView;
@end

@implementation AddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    //注册观察键盘的变化
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(transformView:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self setupViews];
}

- (void)setupViews{
    [self initNav];
    _lblZone.userInteractionEnabled = YES;
    _lblZoneContent.userInteractionEnabled= YES;
    _imgArrow.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectArea)];
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectArea)];
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectArea)];
    [_lblZone addGestureRecognizer:tapGesture1];
    [_lblZoneContent addGestureRecognizer:tapGesture2];
    [_imgArrow addGestureRecognizer:tapGesture3];
    
    _pickerView = [[AreaPickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 100)];
    _pickerView.delegate = self;
    [self.view addSubview:_pickerView];
    [self initData];
}

//移动UIView
-(void)transformView:(NSNotification *)notification{
    // textfield的位置
    CGFloat viewY = [self screenViewYValue:self.txfDetail];
    // 键盘的Y值
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyboardEndY = value.CGRectValue.origin.y;
    // 动画
    NSNumber *duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:duration.doubleValue animations:^{
        CGRect rect = self.view.frame;
        CGFloat height = _txfDetail.frame.size.height;
        NSLog(@"transformView react=%@",NSStringFromCGRect(rect));
        if (viewY + 30 > keyboardEndY) {
            rect.origin.y += keyboardEndY - ( viewY + height);
            self.view.frame = rect;
        }
        NSLog(@"rect.origin.y=%f",rect.origin.y);
    }];
}

//获取UITextField在界面上的y轴位置
- (CGFloat)screenViewYValue:(UIView *)textfield {
    CGFloat y = 0;
    for (UIView *view = textfield; view; view = view.superview) {
        y += view.frame.origin.y;
        if ([view isKindOfClass:[UIScrollView class]]) {
            // 如果父视图是UIScrollView则要去掉内容滚动的距离
            UIScrollView* scrollView = (UIScrollView*)view;
            y -= scrollView.contentOffset.y;
        }
    }
    return y;
}

//键盘销毁
- (void)keyboardWillHide:(NSNotification *)notification{
    // 键盘的Y值
    NSDictionary *userInfo = [notification userInfo];
    // 动画
    NSNumber *duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:duration.doubleValue animations:^{
        CGRect rect = self.view.frame;
        rect.origin.y = 64;
        self.view.frame = rect;
    }];
}

- (void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initData{
    if(!_model){
        return;
    }
    _txfName.text = _model.receiptName;
    _txfPhone.text = _model.receiptPhone;
    [_txfDetail setContent:_model.detailAddress];
}

- (void)initNav{
    self.title = @"添加收货地址";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(complete)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)complete{

}

- (void)selectArea{
    NSLog(@"selectArea");
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = _pickerView.frame;
        NSLog(@"frame=%@",NSStringFromCGRect(frame));
        _pickerView.frame = CGRectMake(frame.origin.x, SCREEN_HEIGHT - frame.size.height - STATUS_AND_NAVIGATION_HEIGHT, frame.size.width, frame.size.height);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark AreaPickerDelagate

- (void)cancelAreaPicker{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = _pickerView.frame;
        NSLog(@"frame=%@",NSStringFromCGRect(frame));
        _pickerView.frame = CGRectMake(frame.origin.x, SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT, frame.size.width, frame.size.height);
    }];
}

- (void)selectComplete:(NSString *)content{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = _pickerView.frame;
        NSLog(@"frame=%@",NSStringFromCGRect(frame));
        _pickerView.frame = CGRectMake(frame.origin.x, SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT, frame.size.width, frame.size.height);
    }];
    _lblZoneContent.text = content;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
