//
//  ReceiptAddressTableCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/5.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "ReceiptAddressModel.h"

@interface ReceiptAddressTableCellView : BaseTableViewCell


/**
 设置model

 @param info model信息
 */
- (void)setModel:(ReceiptAddressModel*)info;
@end
