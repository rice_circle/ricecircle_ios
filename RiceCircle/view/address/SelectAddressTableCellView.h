//
//  SelectAddressTableCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/8.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "ReceiptAddressModel.h"

@interface SelectAddressTableCellView : BaseTableViewCell

-(void)setModel:(ReceiptAddressModel*)model;

@end
