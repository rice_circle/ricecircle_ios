//
//  SelectAddressTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/8.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SelectAddressTableCellView.h"

@interface SelectAddressTableCellView()
//是否选择
@property (nonatomic,strong) UIImageView *imgSelect;
//名称
@property (nonatomic,strong) UILabel *lblName;
//电话
@property (nonatomic,strong) UILabel *lblPhone;
//详细地址
@property (nonatomic,strong) UILabel *lblDetail;
@end

@implementation SelectAddressTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"selectAddressCell";
    SelectAddressTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[SelectAddressTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)setupViews{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _imgSelect = [[UIImageView alloc] init];
    _imgSelect.image = [UIImage imageNamed:@"icon"];
    
    _lblName = [[UILabel alloc] init];
    _lblName.font = SC_FONT_14;
    _lblName.textColor = SC_Color_Black;
    _lblName.text = @"风花雪月";
    
    _lblPhone = [[UILabel alloc] init];
    _lblPhone.font = SC_FONT_14;
    _lblPhone.textColor = SC_Color_Black;
    _lblPhone.text= @"12900000000";
    
    _lblDetail = [[UILabel alloc] init];
    _lblDetail.font = SC_FONT_12;
    _lblDetail.textColor =SC_Color_Black;
    _lblDetail.text= @"sjlslfljsljflsljfjsjlflslsjl";
    
    [self.contentView addSubview:_imgSelect];
    [self.contentView addSubview:_lblName];
    [self.contentView addSubview:_lblPhone];
    [self.contentView addSubview:_lblDetail];
    
    [_imgSelect makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(15);
        make.width.equalTo(15);
        make.left.equalTo(self.contentView.left).offset(16);
        make.centerY.equalTo(self.contentView.centerY);
    }];
    
    [_lblName makeConstraints:^(MASConstraintMaker *make) {
        //make.height.equalTo(15);
        make.left.equalTo(_imgSelect.right).offset(16);
        make.top.equalTo(self.contentView).offset(16);
    }];
    
    [_lblPhone makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-16);
        make.centerY.equalTo(_lblName.centerY);
    }];
    
    [_lblDetail makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(_lblName.leading).offset(0);
        make.top.equalTo(_lblName.bottom).offset(10);
        make.bottom.equalTo(self.contentView.bottom).offset(-16);
    }];
}

-(void)setModel:(ReceiptAddressModel*)model{
    if(!model.defaultAddress){
        _imgSelect.hidden = YES;
        [_imgSelect updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(0);
            make.left.equalTo(self.contentView.left).offset(0);
        }];
        [_lblName updateConstraints:^(MASConstraintMaker *make) {
            //make.left.equalTo(_imgSelect.left).offset(0);
        }];
        
    }else{
        _imgSelect.hidden = NO;
        [_imgSelect updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(15);
            make.left.equalTo(self.contentView.left).offset(16);
        }];
        
        [_lblName updateConstraints:^(MASConstraintMaker *make) {
            //make.left.equalTo(_imgSelect.right).offset(16);
        }];
        
    }
    //[_imgSelect layoutIfNeeded];
    //[_lblName layoutIfNeeded];
    [self.contentView layoutIfNeeded];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
