//
//  AddAddressViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/5.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"
#import "ReceiptAddressModel.h"

@interface AddAddressViewController : BaseUIViewController
@property (nonatomic,strong) ReceiptAddressModel *model;
@end
