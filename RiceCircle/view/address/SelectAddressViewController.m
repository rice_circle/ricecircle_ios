//  选择收货地址界面
//  SelectAddressViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/8.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SelectAddressViewController.h"
#import "RefreshTableView.h"
#import "MJRefresh.h"
#import "ReceiptAddressModel.h"
#import "SelectAddressTableCellView.h"
#import "ReceiptAddressListViewController.h"

@interface SelectAddressViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) RefreshTableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataSet;
@property (nonatomic,assign) NSInteger preIndex;
@end

@implementation SelectAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setupViews];
}

- (void)setupViews{
    [self initNav];
    _tableView = [[RefreshTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT) style:UITableViewStylePlain type:BOTH];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.tableView registerClass:[SelectAddressTableCellView class] forCellReuseIdentifier:@"selectAddressCell"];
    _tableView.separatorInset= UIEdgeInsetsZero;
    _tableView.estimatedRowHeight = 82;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:_tableView];
    IMP_BLOCK_SELF(SelectAddressViewController)
    self.tableView.refresh = ^{
        [block_self requestData:1];
    };
    self.tableView.loading = ^{
        [block_self requestData:2];
    };
    // 马上进入刷新状态
    [self.tableView.mj_header beginRefreshing];
}

- (void)initNav{
    self.title = @"选择收货地址";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"管理" style:UIBarButtonItemStylePlain target:self action:@selector(manager)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)manager{
    ReceiptAddressListViewController *receiptVC = [[ReceiptAddressListViewController alloc] init];
    receiptVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:receiptVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark net request
- (void)requestData:(NSInteger)type{
    [self test:type];
}

- (void)test:(NSInteger)type{
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self initData:type];
    });
}

- (void)initData:(NSInteger)type{
    if(type == 1 || _dataSet == nil){
        _dataSet = [[NSMutableArray alloc] initWithCapacity:20];
    }
    for (NSInteger i = 0; i < 20; i++) {
        NSLog(@"initData i=%tu",i);
        ReceiptAddressModel *model = [[ReceiptAddressModel alloc] init];
        if(i == 0 && type == 1){
            model.defaultAddress = YES;
            _preIndex = 0;
        }
        [_dataSet addObject:model];
    }
    [_tableView reloadData];
    [_tableView.mj_header endRefreshing];
    [_tableView.mj_footer endRefreshing];
    NSLog(@"COUNT = %tu",[_dataSet count]);
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"heightForRowAtIndexPath %lu",indexPath.row);
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ReceiptAddressModel *model = [_dataSet objectAtIndex:indexPath.row];
    if(_preIndex >= 0){
        ReceiptAddressModel *preModel = [_dataSet objectAtIndex:_preIndex];
        preModel.defaultAddress = NO;
    }
    model.defaultAddress = YES;
    _preIndex = indexPath.row;
    [_tableView reloadData];
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSLog(@"tableView count=%tu",self.dataSet.count);
    return self.dataSet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"cellForRowAtIndexPath %lu",indexPath.row);
    SelectAddressTableCellView *cell = [SelectAddressTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    
    ReceiptAddressModel *model = [_dataSet objectAtIndex:indexPath.row];
    [cell setModel:model];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
