//
//  ReceiptAddressTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/5.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ReceiptAddressTableCellView.h"

@interface ReceiptAddressTableCellView()
//收货人姓名
@property (weak, nonatomic) IBOutlet UILabel *lblPeopleName;
//收货人电话
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
//收货详细地址
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;
//默认地址
@property (weak, nonatomic) IBOutlet UILabel *lblDefault;
//编辑
@property (weak, nonatomic) IBOutlet UILabel *lblEdit;
//删除
@property (weak, nonatomic) IBOutlet UILabel *lblDelete;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *superBottomConstraint;
@end

@implementation ReceiptAddressTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"receiptAddressCell";
    ReceiptAddressTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[ReceiptAddressTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setupViews];
}

- (void)setupViews{
    self.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _superBottomConstraint.constant = 15;
    
    //设置默认值
    UITapGestureRecognizer *setDefaultGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setDefault)];
    _imgCheck.userInteractionEnabled = YES;
    [_imgCheck addGestureRecognizer:setDefaultGesture];
    
    //编辑
    UITapGestureRecognizer *editGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editAddress)];
    _lblEdit.userInteractionEnabled = YES;
    [_lblEdit addGestureRecognizer:editGesture];
    
    //删除
    UITapGestureRecognizer *deleteGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteAddress)];
    _lblDelete.userInteractionEnabled = YES;
    [_lblDelete addGestureRecognizer:deleteGesture];
}

//设置默认值
- (void)setDefault{
    if(nil != [self delegate]){
        [[self delegate] tableCellClick:0 withPosition:self.position withModel:nil];
    }
}

//编辑
-(void)editAddress{
    if(nil != [self delegate]){
        [[self delegate] tableCellClick:1 withPosition:self.position withModel:nil];
    }
}

//删除
- (void)deleteAddress{
    if(nil != [self delegate]){
        [[self delegate] tableCellClick:2 withPosition:self.position withModel:nil];
    }
}

/**
 设置model
 
 @param info model信息
 */
- (void)setModel:(ReceiptAddressModel*)info{
    _lblPhone.text = info.receiptPhone;
    if(info.defaultAddress){
        [_imgCheck setImage:[UIImage imageNamed:@"has_store"]];
        [_lblDefault setText:@"默认地址"];
    }else{
        [_imgCheck setImage:[UIImage imageNamed:@"store"]];
        [_lblDefault setText:@"设为默认"];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
