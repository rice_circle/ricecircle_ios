//  收货地址列表界面
//  ReceiptAddressListViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/5.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ReceiptAddressListViewController.h"
// 添加这个宏定义，以后在使用 Masonry 框架中的属性和方法的时候, 就可以省略mas_前缀
#define MAS_SHORTHAND
// 添加这个宏定义，就可以让 equalTo 方法就等价于 mas_equalTo 方法
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"
#import "UIColor+Hex.h"
#import "ReceiptAddressTableCellView.h"
#import "ReceiptAddressModel.h"
#import "AddAddressViewController.h"
#import "SCAlert.h"

@interface ReceiptAddressListViewController ()<UITableViewDelegate,UITableViewDataSource,TableCellDelegate>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataSet;
@property (nonatomic,assign) NSInteger preSelectIndex;
@end

@implementation ReceiptAddressListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self initData];
    [self setupViews];
}

- (void)initData{
    _dataSet = [NSMutableArray array];
    for (NSInteger i = 0 ; i < 5; i++) {
        ReceiptAddressModel *model = [[ReceiptAddressModel alloc] init];
        model.defaultAddress = i == 0 ? YES : NO;
        model.receiptName = [NSString stringWithFormat:@"张三%lu",i];
        model.receiptPhone = [NSString stringWithFormat:@"1391874009%lu",i];
        model.detailAddress =@"上海市上海市黄浦区xxxxx路xxxxx号";
        _preSelectIndex = 0;
        [_dataSet addObject:model];
    }
}

- (void)initNav{
    self.title = @"收货地址";
}

- (void)setupViews{
    [self initNav];
    CGFloat height = SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT - 50;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, height) style:UITableViewStylePlain];
    _tableView.estimatedRowHeight = 60;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"ReceiptAddressTableCellView" bundle:nil] forCellReuseIdentifier:@"receiptAddressCell"];
    [self.view addSubview:_tableView];
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor grayColor];
    [self.view addSubview:bottomView];
    
    UIButton *btnAdd = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnAdd setTitleColor:[UIColor colorWithHexString:@"#f25f72"] forState:UIControlStateNormal];
    [btnAdd setTitle:@"新增收货地址" forState:UIControlStateNormal];
    [btnAdd addTarget:self action:@selector(add) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:btnAdd];
    
    [_tableView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.equalTo(self.view).offset(0);
        make.right.equalTo(self.view).offset(0);
        make.height.equalTo(height);
    }];
    
    [bottomView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tableView.bottom).offset(0);
        make.left.equalTo(self.view).offset(0);
        make.right.equalTo(self.view).offset(0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    [btnAdd makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(bottomView);
    }];
}

- (void)add{
    NSLog(@"add");
    AddAddressViewController *addVC = [[AddAddressViewController alloc] init];
    addVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"heightForRowAtIndexPath %lu",indexPath.row);
    return UITableViewAutomaticDimension;
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSLog(@"tableView count=%tu",self.dataSet.count);
    return self.dataSet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"cellForRowAtIndexPath %lu",indexPath.row);
    ReceiptAddressModel *info = [_dataSet objectAtIndex:indexPath.row];
    ReceiptAddressTableCellView *cell = [ReceiptAddressTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    [cell setModel:info];
    cell.delegate = self;
    cell.position = indexPath;
    return cell;
}

#pragma mark TableCellDelegate

- (void)tableCellClick:(NSInteger)index withPosition:(NSIndexPath *)position withModel:(NSObject*)object{
    NSLog(@"tableCellClick index=%lu,position=%lu",index,position.row
          );
    switch (index) {
        case 0:
            [self setDefaultAddress:position.row];
            break;
            
        case 1:
            [self editAddress:position.row];
            break;
        case 2:
            [self deleteAddress:position];
            break;
    }
}

//设置默认收货地址
- (void)setDefaultAddress:(NSInteger)index{
    if(_preSelectIndex >= 0){
        ReceiptAddressModel *preModel = [_dataSet objectAtIndex:_preSelectIndex];
        preModel.defaultAddress = NO;
    }
    ReceiptAddressModel *model = [_dataSet objectAtIndex:index];
    model.defaultAddress = YES;
    _preSelectIndex = index;
    [_tableView reloadData];
}

//编辑
- (void)editAddress:(NSInteger)index{
    ReceiptAddressModel *model = [_dataSet objectAtIndex:index];
    AddAddressViewController *addVC = [[AddAddressViewController alloc] init];
    NSLog(@"index=%lu,name=%@",index,model.receiptName);
    addVC.model = model;
    addVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addVC animated:YES];
}

//删除收货地址
- (void)deleteAddress:(NSIndexPath *)position {
    [[SCAlert sharedSCAlert] showCancelOkAlert:@"提示" withMessage:@"确定删除？" withCancelBlock:^{
        NSLog(@"cancel");
    } withOKBlock:^{
        NSLog(@"ok");
        [self reAssignment:position.row];
        [_dataSet removeObjectAtIndex:position.row];
        ReceiptAddressModel *model = [_dataSet objectAtIndex:_preSelectIndex];
        model.defaultAddress = YES;
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:position] withRowAnimation:UITableViewRowAnimationLeft];//移除tableView中的数据
        [_tableView reloadData];
    } withParent:self];
}

//重新赋值_preSelectIndex
- (void)reAssignment:(NSInteger)index{
    if(_preSelectIndex < index){
        return;
    }else if(_preSelectIndex == index){
        if(index == _dataSet.count - 1 || index == 0){
            _preSelectIndex = 0;
        }
    }else{
        _preSelectIndex -= 1;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
