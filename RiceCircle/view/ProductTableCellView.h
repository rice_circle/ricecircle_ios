//
//  ProductTableCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/15.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductTableCellView : UITableViewCell
//头像
@property (weak, nonatomic) IBOutlet UIImageView *imgHead;
//商品名称
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
//商品介绍
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
//商品价格
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
//售出数量
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
//商品图片
@property (weak, nonatomic) IBOutlet UIImageView *imgUrl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *superViewRightConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pictrueBottomConstraint;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *superViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *superViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *bgView;

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index;
@end
