//
//  ShouldHelpTableCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/15.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCProgressView.h"

@interface ShouldHelpTableCellView : UITableViewCell
//头像
@property (weak, nonatomic) IBOutlet UIImageView *imgHead;
//应援标题
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
//应援描述
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
//是否已关注
@property (weak, nonatomic) IBOutlet UIImageView *imgAttention;
//应援主题图片
@property (weak, nonatomic) IBOutlet UIImageView *imgUrl;
//应援预计价格
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
//截止时间
@property (weak, nonatomic) IBOutlet UILabel *lblLimtTime;
@property (weak, nonatomic) IBOutlet SCProgressView *progress;
//最外层底部约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *superViewRightConstraint;

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index;
@end
