//
//  ActivityDetailRankingTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/20.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ActivityDetailRankingTableCellView.h"

@interface ActivityDetailRankingTableCellView()
//排名
@property (nonatomic,strong) UILabel *lblRanking;
//头像
@property (nonatomic,strong) UIImageView *imgHead;
//姓名
@property (nonatomic,strong) UILabel *lblName;
//应援价格
@property (nonatomic,strong) UILabel *lblPrice;
@end

@implementation ActivityDetailRankingTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"rankingCell";
    ActivityDetailRankingTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[ActivityDetailRankingTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initData];
    }
    return self;
}

- (void)initData{
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    //排名
    _lblRanking = [[UILabel alloc] init];
    _lblRanking.font = [UIFont systemFontOfSize:14];
    _lblRanking.textColor = SC_Color_Black;
    _lblRanking.text = @"1";
    [self.contentView addSubview:_lblRanking];
    [_lblRanking makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.left).offset(16);
        make.centerY.equalTo(self.contentView.centerY);
    }];
    //头像
    _imgHead = [[UIImageView alloc] init];
    _imgHead.image = [UIImage imageNamed:@"head"];
    [self.contentView addSubview:_imgHead];
    [_imgHead makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_lblRanking.right).offset(15);
        make.top.equalTo(self.contentView.top).offset(15);
        make.bottom.equalTo(self.contentView.bottom).offset(-15);
        make.width.equalTo(40);
        make.height.equalTo(40);
    }];
    
    //姓名
    _lblName = [[UILabel alloc] init];
    _lblName.font = [UIFont systemFontOfSize:12];
    _lblName.textColor = SC_Color_Black;
    _lblName.text = @"哈哈";
    [self.contentView addSubview:_lblName];
    [_lblName makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imgHead.right).offset(10);
        make.centerY.equalTo(_imgHead.centerY);
    }];
    
    //所应援价格
    _lblPrice = [[UILabel alloc] init];
    _lblPrice.font = [UIFont systemFontOfSize:12];
    _lblPrice.textColor = SC_Color_Red;
    _lblPrice.text = @"¥400";
    [self.contentView addSubview:_lblPrice];
    [_lblPrice makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.right).offset(-16);
        make.centerY.equalTo(_imgHead.centerY);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
