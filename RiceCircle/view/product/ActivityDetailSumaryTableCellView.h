//
//  ActivityDetailSumaryTableCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/19.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCProgressView.h"
#import "PhotoShowView.h"

@interface ActivityDetailSumaryTableCellView : UITableViewCell
//头像
@property (weak, nonatomic) IBOutlet UIImageView *imgHead;
//大吧名称
@property (weak, nonatomic) IBOutlet UILabel *lblName;
//大吧关注人数
@property (weak, nonatomic) IBOutlet UILabel *lblAttentionAccount;
@property (weak, nonatomic) IBOutlet UIImageView *imgAttention;
//活动名称
@property (weak, nonatomic) IBOutlet UILabel *lblActivityName;
//目标价格
@property (weak, nonatomic) IBOutlet UILabel *lblTargetPrice;
//活动截止时间
@property (weak, nonatomic) IBOutlet UILabel *lblLimtTime;
//价格筹备进度
@property (weak, nonatomic) IBOutlet SCProgressView *progressView;
//活动详情内容
@property (weak, nonatomic) IBOutlet UILabel *lblActivityDetail;
//相关明星的头像
@property (weak, nonatomic) IBOutlet PhotoShowView *photoShowView;
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *superViewBottomConstraint;
//查看更多
@property (weak, nonatomic) IBOutlet UILabel *lblViewMore;

@property (nonatomic,weak) UIViewController *viewController;

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index;

@end
