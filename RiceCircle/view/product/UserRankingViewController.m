//  用户排名界面
//  UserRankingViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/7.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "UserRankingViewController.h"
#import "RefreshTableView.h"
#import "ActivityDetailRankingTableCellView.h"
#import "MJRefresh.h"

@interface UserRankingViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) RefreshTableView *tableView;
@property (nonatomic,strong) NSMutableArray *dataSet;
@end

@implementation UserRankingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setupViews];
}

- (void)setupViews{
    _tableView = [[RefreshTableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - STATUS_AND_NAVIGATION_HEIGHT) style:UITableViewStylePlain type:BOTH];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.tableView registerClass:[ActivityDetailRankingTableCellView class] forCellReuseIdentifier:@"rankingCell"];
    _tableView.separatorInset= UIEdgeInsetsZero;
    _tableView.estimatedRowHeight = 82;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:_tableView];
    IMP_BLOCK_SELF(UserRankingViewController)
    self.tableView.refresh = ^{
        [block_self requestData:1];
    };
    self.tableView.loading = ^{
        [block_self requestData:2];
    };
    // 马上进入刷新状态
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestData:(NSInteger)type{
    [self test:type];
}

- (void)test:(NSInteger)type{
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self initData:type];
    });
}

- (void)initData:(NSInteger)type{
    if(type == 1 || _dataSet == nil){
        _dataSet = [[NSMutableArray alloc] initWithCapacity:20];
    }
    for (NSInteger i = 0; i < 20; i++) {
        [_dataSet addObject:[NSString stringWithFormat:@"%lu",i]];
    }
    [_tableView reloadData];
    [_tableView.mj_header endRefreshing];
    [_tableView.mj_footer endRefreshing];
    NSLog(@"COUNT = %tu",[_dataSet count]);
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"heightForRowAtIndexPath %lu",indexPath.row);
    return UITableViewAutomaticDimension;
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSLog(@"tableView count=%tu",self.dataSet.count);
    return self.dataSet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"cellForRowAtIndexPath %lu",indexPath.row);
    ActivityDetailRankingTableCellView *cell = [ActivityDetailRankingTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
