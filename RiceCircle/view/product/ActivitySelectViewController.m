//  商品、公益、应援购买选择界面
//  ActivitySelectViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/24.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ActivitySelectViewController.h"
#import "OrderConfirmViewController.h"
#import "AutoDisappearAlert.h"

@interface ActivitySelectViewController ()

@end

@implementation ActivitySelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setupViews];
}

- (void)setupViews{
    [self initNav];
    NSArray *dataSet = @[@"粉丝大码",@"粉丝小码",@"红色大码",@"红色小码",@"粉色偏大红色小码"];
    [_flowLayout setData:dataSet];
    NSLog(@"ActivitySelectViewController setupViews");
    _selectView.backgroundColor = [UIColor redColor];
    _selectView.miniumValue = 1;
    _selectView.miniumBlock = ^{
        AutoDisappearAlert *alert = [[AutoDisappearAlert alloc] init];
        [alert showAlertWithTitle:nil withMessage:@"宝贝不能再减少了" withParent:self withFinish:nil];
    };
    //[self.view updateConstraints];
    _flowLayoutHeightConstraint.constant = _flowLayout.frame.size.height;
    _selectViewWidthConstraint.constant = _selectView.frame.size.width;
}

- (void)initNav{
    self.title = @"选择商品";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buy:(id)sender {
    OrderConfirmViewController *orderVC = [[OrderConfirmViewController alloc] init];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:orderVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
