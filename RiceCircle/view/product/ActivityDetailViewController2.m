//
//  ActivityDetailViewController2.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/18.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ActivityDetailViewController2.h"
#import "ActivityDetailSumaryTableCellView.h"
#import "ActivityDetailRankingTableCellView.h"
#import "UIColor+Hex.h"
#import "ActivitySelectViewController.h"

#define HEIGHT 150
@interface ActivityDetailViewController2 ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UIImageView *imgTop;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,assign) CGFloat lastY;
@property (nonatomic,strong) NSArray *dataSet;
@property (nonatomic,assign) CGFloat preAlpha;
@property (nonatomic,strong) UILabel *lblPrice;
@property (nonatomic,strong) UILabel *lblSupport;
@end

@implementation ActivityDetailViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    self.automaticallyAdjustsScrollViewInsets=NO;
    //设置导航栏透明
    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:0];
    // 去掉navigationBar底部线条
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    // Do any additional setup after loading the view.
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -  50) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 50;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    [self.view addSubview:_tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"ActivityDetailSumaryTableCellView" bundle:nil] forCellReuseIdentifier:@"cell"];
    //设置tableView 的内边距 这样顶部的view 就能够完全的展示出来,不会挡住cell.
    self.tableView.contentInset = UIEdgeInsetsMake(HEIGHT , 0, 0, 0);
    _imgTop = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg"]];
    //设置图片的填充模式
    //等比例缩放,图片不会变形.填充整个父控件
    _imgTop.contentMode = UIViewContentModeScaleAspectFill;
    //设置顶部View的frame
    _imgTop.frame = CGRectMake(0, 0, SCREEN_WIDTH, HEIGHT);
    //为了不挡住tableView的cell内容所以将顶部的View插入到最下面的一层.
    [self.view addSubview:_imgTop];
    _lastY = HEIGHT;
    UIView *bottomView = [[UIView alloc] init];
    [self.view addSubview:bottomView];
    
    _lblPrice = [[UILabel alloc] init];
    _lblPrice.font = [UIFont systemFontOfSize:16];
    _lblPrice.textColor = [UIColor colorWithHexString:@"#F25F72"];
    _lblPrice.text = @"¥18";
    _lblPrice.textAlignment = NSTextAlignmentCenter;
    [bottomView addSubview:_lblPrice];
    
    _lblSupport = [[UILabel alloc] init];
    _lblSupport.font = [UIFont systemFontOfSize:16];
    _lblSupport.textColor = [UIColor colorWithHexString:@"#ffffff"];
    _lblSupport.text = @"支持一下";
    _lblSupport.textAlignment = NSTextAlignmentCenter;
    _lblSupport.userInteractionEnabled = YES;
    _lblSupport.backgroundColor = [UIColor colorWithHexString:@"#F25F72"];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buy)];
    [_lblSupport addGestureRecognizer:tapGesture];
    [bottomView addSubview:_lblSupport];
    
    
    [bottomView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(0);
        make.right.equalTo(self.view).offset(0);
        make.bottom.equalTo(self.view).offset(0);
        make.height.equalTo(50);
    }];
    
    [_lblPrice makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomView.left).offset(0);
        make.top.equalTo(bottomView).offset(0);
        make.width.equalTo(bottomView).multipliedBy(0.28);
        make.height.equalTo(bottomView);
    }];
    
    [_lblSupport makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bottomView).offset(0);
        make.top.equalTo(bottomView).offset(0);
        make.left.equalTo(_lblPrice.right).offset(0);
        make.height.equalTo(bottomView);
    }];
}

- (void)buy{
    NSLog(@"buy");
    ActivitySelectViewController *selectVC = [[ActivitySelectViewController alloc] init];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:selectVC animated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:_preAlpha];
}

- (void)viewWillDisappear:(BOOL)animated{
    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:1];
}

- (void)initData{
    _preAlpha = 0;
    _dataSet = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"2",@"14",@"15",@"16",@"17"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataSet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        ActivityDetailSumaryTableCellView *sumaryCell = [ActivityDetailSumaryTableCellView cellWithTableView:tableView withIndex:indexPath.row];
        sumaryCell.viewController = self;
        return sumaryCell;
    }
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell2"];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell2"];
//    }
//    cell.accessoryType = UITableViewCellAccessoryDetailButton;
//    cell.textLabel.text = [NSString stringWithFormat:@"row%@",[_dataSet objectAtIndex:indexPath.row]];
    ActivityDetailRankingTableCellView *rankingCell = [ActivityDetailRankingTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    return rankingCell;
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat scrollY = scrollView.contentOffset.y;
    CGFloat down = fabs(scrollY > 0 ? 0 : scrollY) - HEIGHT;
    NSLog(@"scrollViewDidScroll down=%f,y=%f", down,scrollView.contentOffset.y);
    if (down < 0) {
        //down = 0;
    }
    CGRect frame = self.imgTop.frame;
    CGFloat height = scrollY + HEIGHT;
    if(down >= -HEIGHT){
        frame.size.height = down + HEIGHT;
        self.imgTop.frame = frame;
        NSLog(@"height=%f",frame.size.height);
    }
    if(down <= 0){
        float alpha = fabs(down) / (HEIGHT * 1.0f);
        //设置导航栏透明
        [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:alpha];
        _preAlpha = alpha;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
