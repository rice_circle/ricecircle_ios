//
//  ActivityDetailSumaryTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/19.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ActivityDetailSumaryTableCellView.h"
#import "StarDetailViewController.h"
#import "UserRankingViewController.h"

@interface ActivityDetailSumaryTableCellView()<PhotoItemDelegate>

@end

@implementation ActivityDetailSumaryTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"cell";
    ActivityDetailSumaryTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[ActivityDetailSumaryTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initData];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self initData];
}

- (void)initData{
    self.layoutMargins = UIEdgeInsetsMake(0, 0, 0, 0);
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _superViewBottomConstraint.constant = 10;
    _progressView.progress = 0.7f;
    _photoShowView.backgroundColor = [UIColor redColor];
    _photoShowView.itemSize = 40;
    _photoShowView.itemPadding =10;
    NSArray *data = @[@"1"];
    _photoShowView.itemDelegate = self;
    [_photoShowView setData:data];
    
    _lblViewMore.userInteractionEnabled = YES;
    UITapGestureRecognizer *viewMoreGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewMore)];
    [_lblViewMore addGestureRecognizer:viewMoreGesture];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//查看更多
- (void)viewMore{
    if(nil == _viewController){
        return;
    }
    UserRankingViewController *rankingVC = [[UserRankingViewController alloc] init];
    rankingVC.hidesBottomBarWhenPushed = YES;
    [_viewController.navigationController pushViewController:rankingVC animated:YES];
    _viewController.hidesBottomBarWhenPushed = YES;
}

#pragma mark PhotoItemDelegate

- (void)itemClick:(NSInteger)index{
    if(nil == _viewController){
        return;
    }
    StarDetailViewController *starVC = [[StarDetailViewController alloc] init];
    _viewController.hidesBottomBarWhenPushed = YES;
    [_viewController.navigationController pushViewController:starVC animated:YES];
    //[[[_viewController.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:alpha];
}

@end
