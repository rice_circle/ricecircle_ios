//
//  ActivitySelectViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/24.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"
#import "SCFlowLayout.h"
#import "QuantitySelectView.h"

@interface ActivitySelectViewController : BaseUIViewController
//商品图片
@property (weak, nonatomic) IBOutlet UIImageView *imgPicture;
//商品名称
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
//商品价格
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
//库存量
@property (weak, nonatomic) IBOutlet UILabel *lblStore;
//类型选择
@property (weak, nonatomic) IBOutlet SCFlowLayout *flowLayout;
//数量选择
@property (weak, nonatomic) IBOutlet QuantitySelectView *selectView;
@property (weak, nonatomic) IBOutlet UIView *splitView;
//类型选择控件高度
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *flowLayoutHeightConstraint;
//数量选择控件宽度
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectViewWidthConstraint;

@end
