//  完善用户信息界面
//  PerfectUserInfoViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/22.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "PerfectUserInfoViewController.h"
#import "SelectStarViewController.h"
#import "SCImagePicker.h"
#import "UploadImageManager.h"
#import "StringListModel.h"

@interface PerfectUserInfoViewController ()<SCImagePickerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgHead;

@end

@implementation PerfectUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self initView];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_txtFieldNickName resignFirstResponder];
}

//初始化view
- (void)initView{
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"下一步" style:UIBarButtonItemStyleDone target:self action:@selector(next)];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.title=@"完善资料";
    
    _imgHead.userInteractionEnabled = YES;
    _imgHead.layer.shadowColor = [UIColor blackColor].CGColor;
    _imgHead.layer.shadowOffset = CGSizeMake(4, 4);
    _imgHead.layer.shadowOpacity = 0.5;
    _imgHead.layer.cornerRadius = _imgHead.frame.size.width/2;
    _imgHead.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _imgHead.layer.borderWidth = 2.0f;
    _imgHead.layer.masksToBounds = YES;
    UITapGestureRecognizer *photoGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectPhoto)];
    [_imgHead addGestureRecognizer:photoGesture];
    
}

//选择头像
- (void)selectPhoto{
    SCImagePicker *imagePicker = [[SCImagePicker alloc] initWithController:self withTitle:@"选择头像"];
    imagePicker.delegate = self;
    [imagePicker showPicker];
}

- (void)updateUserInfo{}

- (void)next{
    SelectStarViewController *selectVC = [[SelectStarViewController alloc] init];
    self.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:selectVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark SCImagePickerDelegate
- (void)imagePickerFinish:(UIImage *)image{
    _imgHead.image = image;
    NSData *data = UIImagePNGRepresentation(image);
    [UploadImageManager uploadImage:data withBlock:^(StringListModel *result) {
        NSLog(@"RESULT.code=%@",result.code);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
