//  注册Controller
//  RegisterViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/22.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "RegisterViewController.h"
#import "PerfectUserInfoViewController.h"
#import "UIColor+Hex.h"
#import "UIButton+Countdown.h"
#import "NetworkManager.h"
#import "APIConstant.h"
#import "VerifyCodeResultModel.h"
#import "AgreementViewController.h"
#import "StringUtil.h"
#import "AutoDisappearAlert.h"
#import "RegisterResultModel.h"
#import "EncryptUtil.h"
#import "NSUserDefaultsManager.h"
#import "AutoDisappearAlert.h"

@interface RegisterViewController ()<NetResponseDelegate>
//手机号
@property (weak, nonatomic) IBOutlet UITextField *txfPhone;
//密码
@property (weak, nonatomic) IBOutlet UITextField *txfPwd;
// 验证码
@property (weak, nonatomic) IBOutlet UITextField *txfVerifyCode;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UILabel *lblAgreement;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view from its nib.
    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:1];
    [self initView];
    [self setupViews];
}

- (void)setupViews{
    _btnSend.backgroundColor = SC_Color_White;
    _btnSend.layer.cornerRadius = 5;
    _btnSend.layer.borderColor = [[UIColor colorWithHexString:@"#f25f73"] CGColor];
    _btnSend.layer.borderWidth = 1;
    //[_btnSend beginCountdown:60];
    
    //设置为密码样式
    _txfPwd.secureTextEntry = YES;
}

//初始化view
- (void)initView{
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"下一步" style:UIBarButtonItemStyleDone target:self action:@selector(next)];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.title=@"注册";
    
    _lblAgreement.userInteractionEnabled = YES;
    NSString *content = @"注册即同意《饭圈儿APP用户协议》";
    NSLog(@"content.lenght=%lu",content.length);
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:content];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1] range:NSMakeRange(0,5)];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:242/255.0 green:95/255.0 blue:114/255.0 alpha:1/1.0] range:NSMakeRange(5,content.length - 5)];
    _lblAgreement.attributedText = str;
    UITapGestureRecognizer *viewAgreement = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewAgreement)];
    [_lblAgreement addGestureRecognizer:viewAgreement];
}

//查看用户协议
- (void)viewAgreement{
    AgreementViewController *agreementVC = [[AgreementViewController alloc] init];
    agreementVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:agreementVC animated:YES];
}

//下一步
- (void)next{
    Boolean f = YES;
    if(f){
        PerfectUserInfoViewController *perfectVC = [[PerfectUserInfoViewController alloc] init];
        self.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:perfectVC animated:YES];
        self.hidesBottomBarWhenPushed = NO;
        return;
    }
    NSString *phone = _txfPhone.text;
    if([StringUtil isBlankString:phone]){
        [[AutoDisappearAlert alloc] showAlertWithTitle:@"提示" withMessage:@"手机号不能为空" withParent:self withFinish:nil];
        return;
    }
    NSString *pwd = _txfPwd.text;
    if([StringUtil isBlankString:pwd]){
        [[AutoDisappearAlert alloc] showAlertWithTitle:@"提示" withMessage:@"密码不能为空" withParent:self withFinish:nil];
        return;
    }
    NSString *code = _txfVerifyCode.text;
    if([StringUtil isBlankString:code]){
        [[AutoDisappearAlert alloc] showAlertWithTitle:@"提示" withMessage:@"验证码不能为空" withParent:self withFinish:nil];
        return;
    }
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:phone forKey:@"mobile"];
    [param setValue:[EncryptUtil encryptPassword:pwd]  forKey:@"password"];
    [param setValue:code forKey:@"checkCode"];
    [[NetworkManager shareInstance] sendRequestWithPath:API_Register withParams:param withCls:[RegisterResultModel class] withDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

//发送验证码
- (IBAction)sendVerifyCode:(id)sender {
    NSString *phone = _txfPhone.text;
    if([StringUtil isBlankString:phone]){
        [[AutoDisappearAlert alloc] showAlertWithTitle:@"提示" withMessage:@"手机号不能为空" withParent:self withFinish:nil];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:phone forKey:@"phone"];
    [params setValue:[NSNumber numberWithInt:0] forKey:@"type"];
    [[NetworkManager shareInstance] sendRequestWithPath:API_SendVerifyCode withParams:params withCls:[VerifyCodeResultModel class] withDelegate:self];
}

//注册
- (void)registerUser{
    
}


#pragma mark NetResponseDelegate

- (void)success:(id)result url:(NSString *)url{
    if([API_Register isEqualToString:url]){
        RegisterResultModel *registerInfo = (RegisterResultModel*)result;
        NSLog(@"registerInfo ID=%lu",registerInfo.data.id);
        if([registerInfo.code isEqualToString:@"API_0000000"]){
            NSString *phone = _txfPhone.text;
            NSString *pwd = _txfPwd.text;
            [[NSUserDefaultsManager sharedNSUserDefaultsManager] saveAccount:phone pwd:pwd];
            PerfectUserInfoViewController *perfectVC = [[PerfectUserInfoViewController alloc] init];
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:perfectVC animated:YES];
            self.hidesBottomBarWhenPushed = NO;
            return;
        }
        [[AutoDisappearAlert alloc] showAlertWithTitle:@"提示" withMessage:[registerInfo message] withParent:self withFinish:nil];
        return;
    }
    VerifyCodeResultModel *info = (VerifyCodeResultModel*)result;
    NSLog(@"info.data.mobile=%@,%@,%lu",info.data.mobile,info.code,info.data.validateActiveTime);
    if([info.code isEqualToString:@"API_0000000"]){
        [_btnSend beginCountdown:60];
        return;
    }
    [[AutoDisappearAlert alloc] showAlertWithTitle:@"提示" withMessage:[info message] withParent:self withFinish:nil];
}

- (void)fail:(NSString *)error url:(NSString *)url{

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
