//  找回密码
//  FindPwdViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "FindPwdViewController.h"
#import "FindPwdPersenter.h"
#import "UIColor+Hex.h"

@interface FindPwdViewController ()
//手机号
@property (weak, nonatomic) IBOutlet UITextField *txfPhone;
//验证码
@property (weak, nonatomic) IBOutlet UITextField *txfVerifyCode;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (nonatomic,strong) FindPwdPersenter *persenter;
@end

@implementation FindPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[[self.navigationController.navigationBar subviews] objectAtIndex:0] setAlpha:1];
    _persenter = [[FindPwdPersenter alloc] init];
    [_persenter setBindVC:self];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view from its nib.
    [self initNav];
    [self setupViews];
}

- (void)setupViews{
    _btnSend.backgroundColor = SC_Color_White;
    _btnSend.layer.cornerRadius = 5;
    _btnSend.layer.borderColor = [[UIColor colorWithHexString:@"#f25f73"] CGColor];
    _btnSend.layer.borderWidth = 1;
}

- (void)initNav{
    self.title = @"找回密码";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"下一步" style:UIBarButtonItemStyleDone target:self action:@selector(nextStep)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

//下一步
- (void)nextStep{
    [_persenter sure];
}

//确定
- (IBAction)sure:(id)sender {
    [_persenter sure];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
