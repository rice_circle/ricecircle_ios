//
//  PerfectUserInfoViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/22.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"

@interface PerfectUserInfoViewController : BaseUIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtFieldNickName;

@end
