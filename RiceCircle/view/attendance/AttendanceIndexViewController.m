//  签到首页表
//  AttendanceIndexViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/11.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "AttendanceIndexViewController.h"
#import "GFCalendar.h"

@interface AttendanceIndexViewController ()
@property (nonatomic,strong) GFCalendarView *calendar;
@property (nonatomic,strong) UILabel *lblCount;
@end

@implementation AttendanceIndexViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupViews];
}

- (void)initNav{
    self.title = @"签到";
}

- (void)setupViews{
    [self initNav];
    CGFloat width = self.view.frame.size.width - 20;
    CGPoint origin = CGPointMake(10, 80.0);
    _calendar = [[GFCalendarView alloc] initWithFrameOrigin:origin width:width];
    _calendar.didSelectDayHandler = ^(NSInteger year, NSInteger month, NSInteger day) {
        NSLog(@"year=%lu,month=%lu,day=%lu",year,month,day);
        
    };
    [_calendar setMaxDate:[NSDate date]];
    [self.view addSubview:_calendar];
    
    _lblCount = [[UILabel alloc] init];
    _lblCount.font = SC_FONT_14;
    _lblCount.textColor = SC_Color_Black;
    _lblCount.text = @"本月累计签到：1次";
    [self.view addSubview:_lblCount];
    
    [_calendar makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.top).offset(80);
        make.left.equalTo(self.view.left).offset(10);
        make.width.equalTo(self.view.frame.size.width - 20);
        make.height.equalTo(_calendar.frame.size.height);
    }];
    
    [_lblCount makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_calendar.bottom).offset(10);
        make.left.equalTo(self.view.left).offset(16);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
