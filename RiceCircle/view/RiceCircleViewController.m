//
//  RiceCircleViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "RiceCircleViewController.h"
#import "LoginViewController.h"
#import "PhotoShowView.h"
#import "TabIndicatorView.h"
#import "SCFlowLayout.h"
#import "QuantitySelectView.h"

@interface RiceCircleViewController ()<PhotoItemDelegate>
@property (nonatomic,strong) PhotoShowView *photoView;
@end

@implementation RiceCircleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"RiceCircleViewController viewDidLoad");
    _progressView.progress = 0.7f;
    _photoView = [[PhotoShowView alloc] initWithFrame:CGRectMake(0, 80, SCREEN_WIDTH, 40)];
    _photoView.backgroundColor = [UIColor greenColor];
    _photoView.itemDelegate = self;
    NSArray *data = @ [@"1",@"2"];
    [_photoView setData:data];
    [self.view addSubview:_photoView];
    
    _photoShowView.backgroundColor = [UIColor redColor];
    _photoShowView.showsHorizontalScrollIndicator = NO;
//    _photoShowView.itemDelegate = self;
    NSArray *data2 = @ [@"1",@"2"];
    [_photoShowView setData:data2];
    
    TabIndicatorView *tab = [[TabIndicatorView alloc] initWithFrame:CGRectMake(20, 130, 250, 30)];
    tab.needCorner = YES;
    tab.backgroundColor = [UIColor greenColor];
    NSArray *data3 = @[@"应援",@"商品",@"粉丝团",@"公益"];
    [tab setDataSet:data3];
    [self.view addSubview:tab];
    
    SCFlowLayout *flowLayout = [[SCFlowLayout alloc] initWithFrame:CGRectMake(20, 170, 280, 10)];
    NSArray *data4 = @[@"粉丝大码",@"粉丝小码",@"红色大码",@"红色小码",@"粉色偏大红色小码"];
    [flowLayout setData:data4];
    flowLayout.backgroundColor = [UIColor greenColor];
    [self.view addSubview:flowLayout];
    
    QuantitySelectView *selectView = [[QuantitySelectView alloc] initWithFrame:CGRectMake(20, 245, 100, 25)];
    selectView.backgroundColor = [UIColor redColor];
    [self.view addSubview:selectView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    NSLog(@"initWithCoder");
    if(self){
        //do something
        [self setupViews];
    }
    return self;
}

-  (void)setupViews{
    //[[NSBundle mainBundle] loadNibNamed:@"RiceCircleViewController" owner:self options:nil];
    
}

- (IBAction)gotoLogin:(id)sender {
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    self.hidesBottomBarWhenPushed = YES;  
    [self.navigationController pushViewController:loginVC animated:YES];
    self.hidesBottomBarWhenPushed=NO;
}

#pragma mark PhotoItemDelegate

- (void)itemClick:(NSInteger)index{
    NSLog(@"点击了第%lu项",index);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
