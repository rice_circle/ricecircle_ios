//  个人信息界面
//  PersonalInfoViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/4.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "PersonalInfoViewController.h"
#import "SCItemLayout.h"
#import "SCDatePicker.h"
#import "InfoEditViewController.h"
#import "SCImagePicker.h"

@interface PersonalInfoViewController ()<SCImagePickerDelegate>
//头像
@property (weak, nonatomic) IBOutlet UIImageView *imgHead;
//姓名
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutName;
//性别
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutSex;

//生日
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutbirthday;
//简介
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutIntroduction;

@property (nonatomic,strong) SCDatePicker *datePicker;
@property (nonatomic,strong) UIAlertController *alertVC;
@end

@implementation PersonalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setupViews];
}

- (void)setupViews{
    [self initNav];
    
    _imgHead.userInteractionEnabled = YES;
    _imgHead.layer.shadowColor = [UIColor blackColor].CGColor;
    _imgHead.layer.shadowOffset = CGSizeMake(4, 4);
    _imgHead.layer.shadowOpacity = 0.5;
    _imgHead.layer.cornerRadius = _imgHead.frame.size.width/2;
    _imgHead.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _imgHead.layer.borderWidth = 2.0f;
    _imgHead.layer.masksToBounds = YES;
    UITapGestureRecognizer *photoGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectPhoto)];
    [_imgHead addGestureRecognizer:photoGesture];
    
    [_layoutName setItemType:Arrow];
    [_layoutName setContent:@"用户名"];
    UITapGestureRecognizer *nameGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editName)];
    [_layoutName addGestureRecognizer:nameGesture];
    
    [_layoutSex setItemType:Arrow];
    [_layoutSex setContent:@"男"];
    UITapGestureRecognizer *sexGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectSex)];
    [_layoutSex addGestureRecognizer:sexGesture];
    
    [_layoutbirthday setItemType:Arrow];
    [_layoutbirthday setContent:@"1990-09-02"];
    UITapGestureRecognizer *birthdayGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(birthday)];
    [_layoutbirthday addGestureRecognizer:birthdayGesture];
    
    [_layoutIntroduction setItemType:Arrow];
    [_layoutIntroduction setContent:@"简介简介"];
    UITapGestureRecognizer *introductionGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(introduction)];
    [_layoutIntroduction addGestureRecognizer:introductionGesture];
    
    _datePicker = [[SCDatePicker alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 0)];
    IMP_BLOCK_SELF(PersonalInfoViewController)
    _datePicker.sureBlock = ^(NSString *content) {
        [block_self.layoutbirthday setContent:content];
        [block_self showOrHiddenDatePicker:NO];
    };
    _datePicker.cancelBlock = ^{
        [block_self showOrHiddenDatePicker:NO];
    };
    [self.view addSubview:_datePicker];
}

- (void)initNav{
    self.title = @"个人信息";
}

- (void)showOrHiddenDatePicker:(BOOL)show{
    double duration = 0.25;
    if(show){
        [UIView animateWithDuration:duration animations:^{
            _datePicker.frame = CGRectMake(0, SCREEN_HEIGHT - _datePicker.frame.size.height - STATUS_AND_NAVIGATION_HEIGHT, SCREEN_WIDTH, _datePicker.frame.size.height);
        }];
        return;
    }
    [UIView animateWithDuration:duration animations:^{
        _datePicker.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, _datePicker.frame.size.height);
    }];
}

//选择生日
- (void)birthday{
    [self showOrHiddenDatePicker:YES];
}

//编辑简介
- (void)introduction{
    InfoEditViewController *infoVC = [[InfoEditViewController alloc] init];
    infoVC.hidesBottomBarWhenPushed = YES;
    IMP_BLOCK_SELF(PersonalInfoViewController)
    infoVC.completeBlock = ^(NSString *content) {
        [block_self.layoutIntroduction setContent:content];
    };
    [self.navigationController pushViewController:infoVC animated:YES];
}

//编辑用户名
- (void)editName{
    InfoEditViewController *infoVC = [[InfoEditViewController alloc] init];
    infoVC.hidesBottomBarWhenPushed = YES;
    IMP_BLOCK_SELF(PersonalInfoViewController)
    infoVC.completeBlock = ^(NSString *content) {
        [block_self.layoutName setContent:content];
    };
    [self.navigationController pushViewController:infoVC animated:YES];
}

//选择男女
- (void)selectSex{
    if(!_alertVC){
        _alertVC = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *maleAction = [UIAlertAction actionWithTitle:@"男" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [_layoutSex setContent:@"男"];
        }];
        UIAlertAction *femaleAction = [UIAlertAction actionWithTitle:@"女" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [_layoutSex setContent:@"女"];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [_alertVC addAction:maleAction];
        [_alertVC addAction:femaleAction];
        [_alertVC addAction:cancelAction];
    }
    [self presentViewController:_alertVC animated:YES completion:nil];
}

//选择头像
- (void)selectPhoto{
    SCImagePicker *imagePicker = [[SCImagePicker alloc] initWithController:self withTitle:@"选择头像"];
    imagePicker.delegate = self;
    [imagePicker showPicker];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark SCImagePickerDelegate
- (void)imagePickerFinish:(UIImage *)image{
    _imgHead.image = image;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
