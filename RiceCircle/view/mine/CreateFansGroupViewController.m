//  创建粉丝团界面
//  CreateFansGroupViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "CreateFansGroupViewController.h"
#import "SCItemLayout.h"
#import "SCInputAccessoryView.h"
#import "StarListViewController.h"
#include "InfoEditViewController.h"

@interface CreateFansGroupViewController ()<AccessoryItemDelegate>
//粉丝团名称
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutFansName;

//负责人名称
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutPrincipalName;
//负责人职位
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutPrincipalJob;
//联系人电话
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutPhone;
//相关联明星
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutStar;
//微博
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutWeibo;
//微信
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutWechat;
//qq
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutQQ;
//简介
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutIntroduction;
//上传身份证正面
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutUploadPositive;
//上传身份证反面
@property (weak, nonatomic) IBOutlet SCItemLayout *layoutUploadNegative;

@property (weak,nonatomic) UIView *editingTextField;
//工具栏
@property (strong,nonatomic) UIToolbar *toolBar;
//工具栏的高度
@property (nonatomic,assign) CGFloat toolViewHeight;
//输入法弹出窗口工具栏
@property (nonatomic,strong) SCInputAccessoryView *inputAccessoryView;
@property (nonatomic,strong) NSMutableArray *textFieldArray;
//当前有焦点的UITextField序号
@property (nonatomic,assign) NSInteger focusIndex;
@end

@implementation CreateFansGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setupViews];
    //注册观察键盘的变化
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(transformView:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated{
    //注册观察键盘的变化
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(transformView:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//键盘回收
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    for(UIView *view in self.view.subviews){
        [view resignFirstResponder];
    }
}

//获取UITextField在界面上的y轴位置
- (CGFloat)screenViewYValue:(UIView *)textfield {
    CGFloat y = 0;
    for (UIView *view = textfield; view; view = view.superview) {
        y += view.frame.origin.y;
        if ([view isKindOfClass:[UIScrollView class]]) {
            // 如果父视图是UIScrollView则要去掉内容滚动的距离
            UIScrollView* scrollView = (UIScrollView*)view;
            y -= scrollView.contentOffset.y;
        }
    }
    return y;
}

//获取当前获取焦点的UITextField
- (void)getIsEditingView:(UIView *)rootView {
    for (UIView *subView in rootView.subviews) {
        if ([subView isKindOfClass:[UITextField class]]) {
            if (((UITextField *)subView).isEditing) {
                self.editingTextField = subView;
                if(_focusIndex == -1){
                    for (NSInteger i = 0; i < _textFieldArray.count; i ++) {
                        if(((SCItemLayout*)_textFieldArray[i]).obtainTextField == (UITextField *)subView){
                            _focusIndex = i;
                            break;
                        }
                    }
                }
                return;
            }
        }
        [self getIsEditingView:subView];
    }
}

//移动UIView
-(void)transformView:(NSNotification *)notification{
    // 拿到正在编辑中的textfield
    [self getIsEditingView:self.view];
    // textfield的位置
    CGFloat viewY = [self screenViewYValue:self.editingTextField];
    // 键盘的Y值
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyboardEndY = value.CGRectValue.origin.y;
    // 动画
    NSNumber *duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:duration.doubleValue animations:^{
        CGRect rect = self.view.frame;
        NSLog(@"transformView react=%@",NSStringFromCGRect(rect));
        CGFloat offsetY = 20;
        if (viewY + offsetY > keyboardEndY) {
            rect.origin.y += keyboardEndY - ( viewY + offsetY);
            self.view.frame = rect;
        }else if(rect.origin.y < 0){
            rect.origin.y += keyboardEndY - (viewY + self.editingTextField.frame.size.height+ offsetY);
            self.view.frame = rect;
        }
        NSLog(@"rect.origin.y=%f",rect.origin.y);
    }];
}

//键盘销毁
- (void)keyboardWillHide:(NSNotification *)notification{
    // 键盘的Y值
    NSDictionary *userInfo = [notification userInfo];
    // 动画
    NSNumber *duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:duration.doubleValue animations:^{
        CGRect rect = self.view.frame;
        rect.origin.y = 64;
        self.view.frame = rect;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)keyboardWillShow:(NSNotification *)notification{
    
}

- (void)initNav{
    self.title = @"申请建团";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(complete)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)setupViews{
    _focusIndex = -1;
    [self initNav];
    [self initToolView];
    _textFieldArray = [NSMutableArray array];
    _layoutFansName.itemType = Edit;
    [_layoutFansName setTitle:@"粉丝团名称"];
    [_textFieldArray addObject:_layoutFansName];
    //[_layoutFansName obtainTextField].inputAccessoryView = _inputAccessoryView;
    
    _layoutPrincipalName.itemType = Edit;
    [_layoutPrincipalName setTitle:@"负责人名称"];
    [_textFieldArray addObject:_layoutPrincipalName];
    
    _layoutPrincipalJob.itemType = Edit;
    [_layoutPrincipalJob setTitle:@"负责人职位"];
    [_textFieldArray addObject:_layoutPrincipalJob];
    
    _layoutPhone.itemType = Edit;
    [_layoutPhone setTitle:@"联系电话"];
    [_textFieldArray addObject:_layoutPhone];
    
    _layoutStar.itemType = Arrow;
    [_layoutStar setTitle:@"相关艺人"];
    UITapGestureRecognizer *starGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectStar)];
    [_layoutStar addGestureRecognizer:starGesture];
    
    _layoutWeibo.itemType = Edit;
    [_layoutWeibo setTitle:@"微博"];
    [_textFieldArray addObject:_layoutWeibo];
    
    _layoutWechat.itemType = Edit;
    [_layoutWechat setTitle:@"微信"];
    [_textFieldArray addObject:_layoutWechat];
    
    _layoutQQ.itemType = Edit;
    [_layoutQQ setTitle:@"QQ"];
    [_textFieldArray addObject:_layoutQQ];
    
    _layoutIntroduction.itemType = Arrow;
    [_layoutIntroduction setTitle:@"认证简介"];
    UITapGestureRecognizer *introductionGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(introduction)];
    [_layoutIntroduction addGestureRecognizer:introductionGesture];
    
    _layoutUploadPositive.itemType = Arrow;
    [_layoutUploadPositive setTitle:@"上传身份证（正面）"];
    
    _layoutUploadNegative.itemType = Arrow;
    [_layoutUploadNegative setTitle:@"上传身份证（反面）"];
    [self addAccessoryView];
}

//选择明星
- (void)selectStar{
    StarListViewController *starListVC = [[StarListViewController alloc] init];
    self.hidesBottomBarWhenPushed =YES;
    [self.navigationController pushViewController:starListVC animated:YES];
}

//填写介绍
- (void)introduction{
    InfoEditViewController *infoVC = [[InfoEditViewController alloc] init];
    infoVC.hidesBottomBarWhenPushed =YES;
    [self.navigationController pushViewController:infoVC animated:YES];
    self.hidesBottomBarWhenPushed =YES;
}

//给UITextField添加inputAccessoryView
- (void)addAccessoryView{
    for (NSInteger i = 0; i < _textFieldArray.count; i ++) {
        ((SCItemLayout*)[_textFieldArray objectAtIndex:i]).obtainTextField.inputAccessoryView = _inputAccessoryView;
    }
}

//初始化工具栏
- (void)initToolView{
    CGFloat toolViewHeight = 44;
    _inputAccessoryView = [[SCInputAccessoryView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, toolViewHeight) withType:Operable];
    _inputAccessoryView.accessoryDelegate = self;
    [_inputAccessoryView buildViews];
    
}

//寻找上一个UITextField
- (void)previous{
    if(_focusIndex < 1){
        return;
    }
    _focusIndex -= 1;
    [((SCItemLayout*)[_textFieldArray objectAtIndex:_focusIndex]).obtainTextField becomeFirstResponder];
}

//寻找下一个UITextField
- (void)next{
    NSLog(@"next index1=%lu",_focusIndex);
    if(_focusIndex >= _textFieldArray.count - 1){
        return;
    }
    _focusIndex += 1;
    NSLog(@"next index2=%lu",_focusIndex);
    [((SCItemLayout*)[_textFieldArray objectAtIndex:_focusIndex]).obtainTextField becomeFirstResponder];
}

//编辑完成
- (void)editComplete{
    [self.view endEditing:YES];
}

//选择完成
- (void)complete{
    
}

#pragma mark AccessoryItemDelegate
- (void)accessoryItemClick:(NSInteger)index{
    NSLog(@"accessoryItemClick index=%lu",index);
    switch (index) {
        case 0:
            [self previous];
            break;
            
        case 1:
            [self next];
            break;
        case 2:
            [self editComplete];
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
