//
//  MyFollowViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/3.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "MyFollowViewController.h"
#import "TabIndicatorView.h"
#import "StarListViewController.h"
#import "RelatedFansViewController.h"

@interface MyFollowViewController ()<TabIndicatroItemDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet TabIndicatorView *indicatorView;
@property (weak, nonatomic) IBOutlet UIScrollView *contentView;
@property (nonatomic,strong) NSMutableArray *views;
@property (nonatomic,assign) BOOL needScroll;
@end

@implementation MyFollowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initData];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setupViews];
}

- (void)initNav{
    self.title = @"我的关注";
}

- (void)setupViews{
    [self initNav];
    _needScroll = YES;
    NSArray *title = @[@"明星",@"粉丝团"];
    _indicatorView.needCorner = YES;
    _indicatorView.indicatorDelegate = self;
    [_indicatorView setDataSet:title];
    
    //UIScrollView
    _contentView.alwaysBounceHorizontal = NO;
    _contentView.backgroundColor = [UIColor greenColor];
    _contentView.contentSize = CGSizeMake(SCREEN_WIDTH * _views.count, 0);
    _contentView.pagingEnabled = YES;
    _contentView.delegate = self;
    NSLog(@"contentview frame=%@",NSStringFromCGRect(_contentView.frame));
    [self selectVC:0];
}

- (void)initData{
    _views = [NSMutableArray array];
    for (NSInteger i = 0; i < 3; i++) {
        switch (i) {
            case 0:{
                StarListViewController *startVC = [[StarListViewController alloc] init];
                //[startVC.view setFrame:_contentView.frame];
                [_views addObject:startVC];
            }
                break;
                
            case 1:{
                RelatedFansViewController *fansVC = [[RelatedFansViewController alloc] init];
                [_views addObject:fansVC];
            }
                break;
        }
    }
}


- (void)selectVC:(NSInteger )index{
    UIViewController *vc = [_views objectAtIndex:index];
    CGRect frame = CGRectMake(_contentView.frame.origin.x, _contentView.frame.origin.y, _contentView.frame.size.width, _contentView.frame.size.height);
    if(index == 0){
        [((StarListViewController *)vc) updateSize:frame];
    }else if(index == 1){
        [((RelatedFansViewController *)vc) updateSize:CGRectMake(frame.size.width, frame.origin.y, frame.size.width, frame.size.height)];
    }
    [self addChildViewController:vc];
    [_contentView addSubview:vc.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TabIndicatroItemDelegate
- (void)indicatorItemClick:(NSInteger)index{
    NSLog(@"StarDetailViewController indicatorItemClick index=%lu",index);
    _needScroll = NO;
    [_contentView setContentOffset:CGPointMake((index - 1) * SCREEN_WIDTH, 0) animated:YES];
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"StarDetailViewController scrollViewDidScroll _needScroll=%d",_needScroll);
    CGFloat index = scrollView.contentOffset.x / SCREEN_WIDTH;
    [self selectVC:index];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(decelerate == NO){
        [self scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(nonnull UIScrollView *)scrollView{
    // 在这里面写scrollView停止时需要做的事情
    NSLog(@"UIScrollView停止滚动了");
    CGFloat index = scrollView.contentOffset.x / SCREEN_WIDTH;
    if(_needScroll){
        _indicatorView.currentIndex = index;
    }
    _needScroll = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
