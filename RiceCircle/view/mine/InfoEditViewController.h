//
//  InfoEditViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/4.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseUIViewController.h"


//编辑完成block
typedef void(^EditCompleteBlock)(NSString* content);


@interface InfoEditViewController : BaseUIViewController

@property (nonatomic,copy) EditCompleteBlock completeBlock;

@end
