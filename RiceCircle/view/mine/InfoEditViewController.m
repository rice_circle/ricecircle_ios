//
//  InfoEditViewController.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/4.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "InfoEditViewController.h"
#import "SCInputAccessoryView.h"
#import "SCCountTextView.h"

@interface InfoEditViewController ()<AccessoryItemDelegate>
@property (weak, nonatomic) IBOutlet SCCountTextView *contentView;

@property (nonatomic,strong) SCInputAccessoryView *inputView;
@end

@implementation InfoEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    //注册观察键盘的变化
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [self setupViews];
}

- (void)setupViews{
    [self initNav];
    [self initInputView];
    //_contentView.inputAccessoryView = _inputView;
    _contentView.placeHolder = @"请输入个人签名";
    _contentView.showCount = YES;
    _contentView.totalCount = 150;
    _contentView.needTranslateCount = YES;
}

//初始化导航栏
- (void)initNav{
    self.title = @"编辑";
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = leftItem;
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(complete)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)initInputView{
    _inputView = [[SCInputAccessoryView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44) withType:Default];
    _inputView.accessoryDelegate = self;
    [_inputView buildViews];
}

//取消
- (void)cancel{
    [self.navigationController popViewControllerAnimated:YES];
}

//完成
- (void)complete{
    [self.navigationController popViewControllerAnimated:YES];
    if(_completeBlock){
        NSString *content = [_contentView obtainContent];
        self.completeBlock(content);
    }
}

//移动UIView
-(void)keyboardWillChange:(NSNotification *)notification{
    // 键盘的Y值
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyboardEndY = value.CGRectValue.origin.y;
    NSLog(@"keyboardWillChange value=%@",NSStringFromCGRect(value.CGRectValue));
    // 动画
    NSNumber *duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    [_contentView transformCountView:keyboardEndY withDuration:duration.doubleValue];
}

#pragma mark AccessoryItemDelegate

- (void)accessoryItemClick:(NSInteger)index{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
