//
//  RiceCircleViewController2.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/28.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "RiceCircleViewController2.h"
#import "StartInfoCollectionCellView.h"
#import "StarTableCellView.h"

@interface RiceCircleViewController2 ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
//相关明星数据集
@property (nonatomic,strong) NSMutableArray *startDataSet;
//top10数据集
@property (nonatomic,strong) NSMutableArray *topDataSet;
//推荐
@property (weak, nonatomic) IBOutlet UIImageView *imgRecommend;
//大吧
@property (weak, nonatomic) IBOutlet UIImageView *imgDaBa;
//个站
@property (weak, nonatomic) IBOutlet UIImageView *imgPersonal;
//饭绘
@property (weak, nonatomic) IBOutlet UIImageView *imgDraw;
//饭拍
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
//关注明星列表
@property (weak, nonatomic) IBOutlet UICollectionView *collectionStarView;
//top10列表
@property (weak, nonatomic) IBOutlet UITableView *tableTop10;

@end

@implementation RiceCircleViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initData];
    [self setupViews];
}

- (void)initData{
    _startDataSet = [NSMutableArray array];
    for (NSInteger i = 0; i < 10; i++) {
        [_startDataSet addObject:[NSString stringWithFormat:@"%lu",i]];
    }
    _topDataSet = [NSMutableArray array];
    for (NSInteger i = 0; i < 20; i++) {
        Star *star = [[Star alloc] init];
        star.name = [NSString stringWithFormat:@"%tu张三%tu",1,i];
        star.fans = i;
        [_topDataSet addObject:star];
    }
}

- (void)setupViews{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    // 设置UICollectionView为横向滚动
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    // 每一行cell之间的间距
//    flowLayout.minimumLineSpacing = 11;
    // 每一列cell之间的间距
     flowLayout.minimumInteritemSpacing = 10;
    // 设置第一个cell和最后一个cell,与父控件之间的间距
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 16, 0, 16);
    flowLayout.estimatedItemSize = CGSizeMake(100, 140);
    _collectionStarView.collectionViewLayout = flowLayout;
    _collectionStarView.delegate = self;
    _collectionStarView.dataSource = self;
    _collectionStarView.pagingEnabled = NO;
    _collectionStarView.showsVerticalScrollIndicator = NO;
    [_collectionStarView registerClass:[StartInfoCollectionCellView class] forCellWithReuseIdentifier:@"startInfoCell"];
    
    _tableTop10.delegate = self;
    _tableTop10.dataSource = self;
    _tableTop10.estimatedRowHeight = 80;
    [_tableTop10 registerClass:[StarTableCellView class] forCellReuseIdentifier:@"cell"];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

}

#pragma mark UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSLog(@"collectionView numberOfItemsInSection=%lu",_startDataSet.count);
    return _startDataSet.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    StartInfoCollectionCellView *infoCell = [StartInfoCollectionCellView cellWithTableView:collectionView withIndex:indexPath];
    return infoCell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}


#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  _topDataSet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StarTableCellView *cell = [StarTableCellView cellWithTableView:tableView withIndex:indexPath.row];
    Star *star = [_topDataSet objectAtIndex:[indexPath row]];
    [cell assignment:star];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
