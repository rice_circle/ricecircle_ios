//
//  HomeCenterTableCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/14.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCenterTableCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSuperViewConstraint;
//应援
@property (weak, nonatomic) IBOutlet UIImageView *imgShouldHelp;
//商品
@property (weak, nonatomic) IBOutlet UIImageView *imgProduct;
//公益
@property (weak, nonatomic) IBOutlet UIImageView *imgPublic;
//投票
@property (weak, nonatomic) IBOutlet UIImageView *imgVote;
//上线
@property (weak, nonatomic) IBOutlet UIImageView *imgOnline;
//签到
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckIn;

@property (nonatomic,strong) UIViewController *viewController;
+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index;


@end
