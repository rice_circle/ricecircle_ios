//
//  VoteTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "VoteTableCellView.h"

@implementation VoteTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"voteCell";
    VoteTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[VoteTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupviews];
    }
    return self;
}

- (void)setupviews{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    UIColor *bgColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"over_bg"]];
    _bgView.backgroundColor = bgColor;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setupviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
