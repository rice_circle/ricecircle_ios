//
//  VoteTableCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@interface VoteTableCellView : BaseTableViewCell
//头像
@property (weak, nonatomic) IBOutlet UIImageView *imgHead;
//名称
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
//姓名
@property (weak, nonatomic) IBOutlet UILabel *lblName;
//参与人数
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
//收藏
@property (weak, nonatomic) IBOutlet UIImageView *imgAttention;
//封面图片
@property (weak, nonatomic) IBOutlet UIImageView *imgContentPictrue;
@property (weak, nonatomic) IBOutlet UIView *bgView;


@end
