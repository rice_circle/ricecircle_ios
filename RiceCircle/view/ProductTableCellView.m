//
//  ProductTableCellView.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/15.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "ProductTableCellView.h"

@implementation ProductTableCellView

+ (instancetype)cellWithTableView:(UITableView *)tableView withIndex:(NSInteger)index{
    NSString *ID = @"productCell";
    ProductTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (cell == nil) {
        cell = [[ProductTableCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _bgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"over_bg"]];
    _superViewRightConstraint.constant = 8;
    _superViewBottomConstraint.constant = 5;
    //_pictrueBottomConstraint.constant = 0;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setupUI];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
