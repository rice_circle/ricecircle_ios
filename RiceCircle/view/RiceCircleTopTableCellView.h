//
//  RiceCircleTopTableCellView.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/28.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@interface RiceCircleTopTableCellView : BaseTableViewCell
@property (nonatomic,weak) UIViewController *viewController;
@end
