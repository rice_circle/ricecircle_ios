//
//  RiceCircleViewController.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCProgressView.h"
#import "PhotoShowView.h"

@interface RiceCircleViewController : UIViewController
@property (weak, nonatomic) IBOutlet SCProgressView *progressView;
@property (weak, nonatomic) IBOutlet PhotoShowView *photoShowView;

@end
