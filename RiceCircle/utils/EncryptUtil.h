//
//  EncryptUtil.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EncryptUtil : NSObject

/**
 sha3加密

 @param str 需要加密的字串
 @param len 密钥长度（sh3-224 sha3-256 sha3-384 sha3-512等）
 @return sha3字符串
 */
+ (NSString*)SHA3:(NSString*)str withBitsLength:(NSUInteger)len;


/**
 AES加密

 @param content 需要加密的字串
 @param key 密钥
 @return AES字串
 */
+ (NSString*)AES:(NSString *)content key:(NSString *)key;


/**
 md5加密

 @param content 加密内容
 @return 加密字符串
 */
+ (NSString*)md5:(NSString*)content;

/**
 加密密码
 
 @param content 加密内容
 @param key 干扰key
 @return 加密字符串
 */
+ (NSString*)encryptPassword:(NSString*)content key:(NSString*)key;

/**
 加密密码
 
 @param content 加密内容
 @return 加密字符串
 */
+ (NSString*)encryptPassword:(NSString*)content;

@end
