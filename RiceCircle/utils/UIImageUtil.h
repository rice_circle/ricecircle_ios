//
//  UIImageUtil.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/7.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImageUtil : NSObject


/**
 颜色转图片

 @param color 颜色
 @return 图片
 */
+ (UIImage*) createImageWithColor: (UIColor*) color;

@end
