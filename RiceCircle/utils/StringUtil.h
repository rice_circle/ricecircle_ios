//
//  StringUtil.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringUtil : NSObject

/**
 
 Dictionary转化为json字符串
 @param dic 字典
 @return json字符串
 */
+(NSString*)dictionaryConvertJsonString:(NSDictionary *)dic;

/**
 json字符串转化为NSDictionary

 @param json json字符串
 @return NSDictionary
 */
+ (NSDictionary *)jsonStringConvertDictionary:(NSString *)json;

/**
 NSData转为HexString

 @param data nsdata数据
 @return HexString
 */
+ (NSString *)NSDataToHexString:(NSData*)data;

/**
 判断字符串是否为空
 
 @param string 字符串
 @return 如果为空返回YES,否则返回NO
 */
+ (BOOL) isBlankString:(NSString *)string;

/**
 nsstring转化为json格式的字符串
 
 @param aString nsstring字符串
 @return json格式的字符串
 */
+(NSString *)toJSONString:(NSString *)aString;
@end
