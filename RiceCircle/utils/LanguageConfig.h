//
//  LanguageConfig.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/9.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageConfig : NSObject
//生成单例对象
IOS_SINGLETON_H(LanguageConfig);

// 根据key获取相应的String
- (NSString *) getStringForKey:(NSString *) key;

- (void) setNewLanguage:(NSString *) language;

//切换语言
- (void) changeLanguage;
@end
