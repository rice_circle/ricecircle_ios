//  日期工具类
//  DateUtil.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/31.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "DateUtil.h"

@implementation DateUtil
/**
 获取当前时间，以毫秒为单位返回
 
 @return 返回以毫秒为单位的时间值
 */
+ (long)currentTimeMillis{
    NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
    double time = interval * 1000;
    NSString *timeString = [NSString stringWithFormat:@"%0.f", time];//转为字符型
    return [timeString longLongValue];
}

/**
 根据给定的日期格式解析日期字符串
 
 @param dateStr 日期字符串
 @param format 日期格式
 @return 格式化之后的日期字符串
 */
+ (NSString *)parseFormatDate:(NSString *)dateStr withFormat:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSDate *date = nil;
    if([self isYYYYMMDDHHMMSSSFormat:dateStr]){
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:sss";
        date = [formatter dateFromString:dateStr];
    }else if([self isYYYYMMDDHHMMSSFormat:dateStr]){
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        date = [formatter dateFromString:dateStr];
    }else if([self isYYYYMMDDHHMMFormat:dateStr]){
        formatter.dateFormat = @"yyyy-MM-dd HH:mm";
        date = [formatter dateFromString:dateStr];
    }else if([self isYYYYMMDDFormat:dateStr]){
        formatter.dateFormat = @"yyyy-MM-dd";
        date = [formatter dateFromString:dateStr];
    }else if([self isNumberDateFormat:dateStr]){
        date = [NSDate dateWithTimeIntervalSince1970:[dateStr longLongValue] / 1000.0];
    }else{
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en"];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss zzz";
        date = [formatter dateFromString:dateStr];
    }
    formatter.dateFormat = format;
    NSLog(@"date=%@",date);
    return [formatter stringFromDate:date];
}


//判断是否为纯数字日期格式(例如：1496225395881)
+ (BOOL)isNumberDateFormat:(NSString *) numStr{
    NSString* regex=@"^[0-9]+$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:numStr];
}

//判断是否为yyyy-MM-dd HH:mm:sss日期格式(例如：2017-05-31 19:03:023)
+ (BOOL)isYYYYMMDDHHMMSSSFormat:(NSString *)dateStr{
    NSString *regex = @"^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{3}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:dateStr];
}

//判断是否为yyyy-MM-dd HH:mm:ss日期格式(例如：2017-05-31 19:03:02)
+ (BOOL)isYYYYMMDDHHMMSSFormat:(NSString *)dateStr{
    NSString *regex = @"^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:dateStr];
}

//判断是否为yyyy-MM-dd HH:mm日期格式(例如：2017-05-31 19:03)
+ (BOOL)isYYYYMMDDHHMMFormat:(NSString *)dateStr{
    NSString *regex = @"^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:dateStr];
}

//判断是否为yyyy-MM-dd日期格式(例如：2017-05-31)
+ (BOOL)isYYYYMMDDFormat:(NSString *)dateStr{
    NSString *regex = @"^\\d{4}-\\d{2}-\\d{2}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:dateStr];
}

@end
