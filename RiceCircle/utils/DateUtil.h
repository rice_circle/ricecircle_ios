//
//  DateUtil.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/31.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtil : NSObject


/**
 获取当前时间，以毫秒为单位返回
 
 @return 返回以毫秒为单位的时间值
 */
+ (long)currentTimeMillis;


/**
 根据给定的日期格式解析日期字符串
 
 @param date 日期字符串
 @param format 日期格式
 @return 格式化之后的日期字符串
 */
+ (NSString*)parseFormatDate:(NSString*)date withFormat:(NSString *)format;
@end
