//  多语言配置类
//  LanguageConfig.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/9.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "LanguageConfig.h"

#define Language_Key @"languageKey"
#define Chinese_Simple @"zh-Hans"
#define Chinese_Traditional @"zh-Hant"
#define English_US @"en"


@interface LanguageConfig()

//当前语言
@property (nonatomic) NSString *language;

@end

@implementation LanguageConfig
//实现单例对象
IOS_SINGLETON_M(LanguageConfig);

- (instancetype)init{
    self = [super init];
    if (self){
        [self initLanguage];
    }
    return self;
}

// 根据语言名获取bundle
- (NSBundle *)bundle{
    NSString *language = [[NSUserDefaults standardUserDefaults] objectForKey:Language_Key];
    //默认是简体中文
    if (language == nil) {
        language = Chinese_Simple;
    }
    NSString * bundlePath = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
    return [NSBundle bundleWithPath:bundlePath];
}

//根据key获取string
- (NSString *)getStringForKey:(NSString *)key{
    NSBundle * bundle = [[LanguageConfig sharedLanguageConfig] bundle];
    if (bundle) {
        return NSLocalizedStringFromTableInBundle(key, @"Localizable", bundle, nil);
    }
    [NSString stringWithFormat:@"%@1",@"1" ];
    return NSLocalizedString(key, nil);
}

/**
 * 初始化语言
 */
- (void) initLanguage{
    self.language = [[NSUserDefaults standardUserDefaults] objectForKey:Language_Key];
}

/**
 * 设置新语言
 */
- (void)setNewLanguage:(NSString *)language{
    //判断当前语言是否为空
    if(NULLString(self.language)){
        [self initLanguage];
    }
    if ([language isEqualToString:self.language]){
        return;
    }
    
    if (![language isEqualToString:English_US] && ![language isEqualToString:Chinese_Simple]
        && ![language isEqualToString:Chinese_Traditional]){
        return;
    }
    self.language = language;
    [[NSUserDefaults standardUserDefaults]setObject:language forKey:Language_Key];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

/**
 * 切换语言
 */
- (void)changeLanguage{
    
}

@end
