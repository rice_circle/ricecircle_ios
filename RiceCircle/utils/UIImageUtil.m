//  UIImage工具类
//  UIImageUtil.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/7.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "UIImageUtil.h"

@implementation UIImageUtil

/**
 颜色转图片
 
 @param color 颜色
 @return 图片
 */
+ (UIImage*) createImageWithColor: (UIColor*) color{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

@end
