//  加密工具类
//  EncryptUtil.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "EncryptUtil.h"
#import <CommonCrypto/CommonCryptor.h>
#import "keccak.h"
#import "StringUtil.h"
#import <CommonCrypto/CommonDigest.h>

//初始向量的值
NSString const *kInitVector = @"A-16-Byte-String";
//密钥长度
size_t const kKeySize = kCCKeySizeAES128;

@implementation EncryptUtil

/**
 sha3加密
 
 @param str 需要加密的字串
 @param len 密钥长度（sh3-224 sha3-256 sha3-384 sha3-512等）
 @return sha3字符串
 */
+ (NSString *)SHA3:(NSString *)str withBitsLength:(NSUInteger)len{
    int bytes = (int)(len/8);
    const char * string = [str UTF8String];
    int size=(int)strlen(string);
    uint8_t md[bytes];
    keccak((uint8_t*)string, size, md, bytes);
    NSMutableString *sha3 = [[NSMutableString alloc] initWithCapacity:len/4];
    
    for(int32_t i=0;i<bytes;i++){
        [sha3 appendFormat:@"%02x", md[i]];
    }
    return sha3;
}

/**
 AES加密
 
 @param content 需要加密的字串
 @param key 密钥
 @return AES字串
 */
+ (NSString *)AES:(NSString *)content key:(NSString *)key{
    NSData *contentData = [content dataUsingEncoding:NSUTF8StringEncoding];
    NSUInteger dataLength = contentData.length;
        
    // 为结束符'\0' +1
    char keyPtr[kKeySize + 1];
    memset(keyPtr, 0, sizeof(keyPtr));
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
        
    // 密文长度 <= 明文长度 + BlockSize
    size_t encryptSize = dataLength + kCCBlockSizeAES128;
    void *encryptedBytes = malloc(encryptSize);
    size_t actualOutSize = 0;
        
    NSData *initVector = [kInitVector dataUsingEncoding:NSUTF8StringEncoding];
        
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt,kCCAlgorithmAES,
                        kCCOptionPKCS7Padding,  // 系统默认使用 CBC，然后指明使用 PKCS7Padding
                        keyPtr,
                        kKeySize,
                        initVector.bytes,
                        contentData.bytes,
                        dataLength,
                        encryptedBytes,
                        encryptSize,
                        &actualOutSize);
        
    if (cryptStatus == kCCSuccess) {
        NSData *data = [NSData dataWithBytesNoCopy:encryptedBytes length:actualOutSize];
        NSLog(@"data=%@",data);
        // 对加密后的数据进行 base64 编码
//        return [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        //转换为2进制字符串
        if (data && data.length > 0) {
            Byte *datas = (Byte*)[data bytes];
            NSMutableString *output = [NSMutableString stringWithCapacity:data.length * 2];
            for(int i = 0; i < data.length; i++){
                [output appendFormat:@"%02x", datas[i]];
            }
            return output;
        }
//        return [StringUtil NSDataToHexString:data];
        //return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    free(encryptedBytes);
    return nil;
}

/**
 md5加密
 
 @param content 加密内容
 @return 加密字符串
 */
+ (NSString*)md5:(NSString*)content{
    const char *cStr = [content UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest);
    
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [result appendFormat:@"%02X", digest[i]];
    }
    return result;
}

/**
 md5加密，算法为：先分别md5加密内容和key，然后key加密后的md5字符串和content加密后的字符串拼接到一起，在一起用md5加密一次，
 NSString *result = md5(md5(key)+md5(content))
 
 @param content 加密内容
 @return 加密字符串
 */
+ (NSString*)encryptPassword:(NSString*)content{
    NSString *key = @"dui3_!@#$%^&*()_rice_circle";
    return [self encryptPassword:content key:key];
}

/**
 md5加密，算法为：先分别md5加密内容和key，然后key加密后的md5字符串和content加密后的字符串拼接到一起，在一起用md5加密一次，
 NSString *result = md5(md5(key)+md5(content))
 
 @param content 加密内容
 @param key 干扰key
 @return 加密字符串
 */
+ (NSString*)encryptPassword:(NSString*)content key:(NSString*)key{
    //加密content
    NSString *resultContent = [self md5:content];
    
    //加密key
    NSString *resultKey = [self md5:key];
    
    NSString *result = [resultKey stringByAppendingString:resultContent];
    return [self md5:result];
}

@end
