//
//  FindPwdPersenter.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "FindPwdPersenter.h"
#import "UpdatePwdViewController.h"


@implementation FindPwdPersenter


- (void)sure{
    if(nil == self.bindVC){
        return;
    }
    UpdatePwdViewController *updateVC = [[UpdatePwdViewController alloc] init];
    self.bindVC.hidesBottomBarWhenPushed = YES;
    [self.bindVC.navigationController pushViewController:updateVC animated:YES];
    //self.bindVC.hidesBottomBarWhenPushed = NO;
}
@end
