//
//  FindPwdPersenter.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePersenter.h"

@interface FindPwdPersenter : BasePersenter
- (void)sure;
@end
