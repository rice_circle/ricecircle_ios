//  倒计时显示按钮
//  UIButton+Countdown.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/8.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "UIButton+Countdown.h"
#import "UIColor+Hex.h"

@implementation UIButton (Countdown)


/**
 开始倒计时

 @param limtTime 倒计时时间
 */
- (void)beginCountdown:(NSInteger) limtTime{
    NSString *countdownText = @"重新发送(%ds)";
    UIColor *countdownColor = [UIColor lightGrayColor];
    NSString *normalText = @"发送验证码";
    UIColor *normalColor = [UIColor whiteColor];
    [self beginCountdown:limtTime withCountdownText:countdownText withCountdownColor:countdownColor withNoramlText:normalText withNormalColor:normalColor];
}



/**
 开始倒计时

 @param limtTime 倒计时时间
 @param countdownText 倒计时显示文案
 @param countdownColor 倒计时之后按钮颜色
 @param normalText 正常显示文案
 @param normalColor 正常按钮颜色
 */
- (void)beginCountdown:(NSInteger) limtTime withCountdownText:(NSString*)countdownText withCountdownColor:(UIColor*)countdownColor withNoramlText:(NSString*)normalText withNormalColor:(UIColor*)normalColor{
    __block NSInteger time = limtTime; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time <= 0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置按钮的样式
                [self setTitle:normalText forState:UIControlStateNormal];
                [self setTitleColor:[UIColor colorWithHexString:@"f25f73"] forState:UIControlStateNormal];
                self.userInteractionEnabled = YES;
            });
            
        }else{
            //int seconds = time % 60;
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置按钮显示读秒效果
                [self setTitle:[NSString stringWithFormat:countdownText, time] forState:UIControlStateNormal];
                [self setTitleColor:[UIColor colorWithHexString:@"f25f73"] forState:UIControlStateNormal];
                self.userInteractionEnabled = NO;
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}

@end
