//
//  NSString+Extension.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/22.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "NSString+Extension.h"

@implementation NSString (Extension)
- (CGRect)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil];
}
@end
