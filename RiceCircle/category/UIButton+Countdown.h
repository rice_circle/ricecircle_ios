//
//  UIButton+Countdown.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/8.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Countdown)

/**
 开始倒计时
 
 @param limtTime 倒计时时间
 */
- (void)beginCountdown:(NSInteger) limtTime;

/**
 开始倒计时
 
 @param limtTime 倒计时时间
 @param countdownText 倒计时显示文案
 @param countdownColor 倒计时之后按钮颜色
 @param normalText 正常显示文案
 @param normalColor 正常按钮颜色
 */
- (void)beginCountdown:(NSInteger) limtTime withCountdownText:(NSString*)countdownText withCountdownColor:(UIColor*)countdownColor withNoramlText:(NSString*)normalText withNormalColor:(UIColor*)normalColor;
@end
