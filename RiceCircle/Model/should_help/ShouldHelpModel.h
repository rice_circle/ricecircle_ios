//
//  ShouldHelpModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/16.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseModel.h"

@interface ShouldHelpModel : BaseModel
//活动ID
@property (nonatomic,strong) NSString *activeId;
//发布人
@property (nonatomic,strong) NSString *activePublisher;
//发布人头像地址
@property (nonatomic,strong) NSString *publisherUrl;
//主题
@property (nonatomic,strong) NSString *activeSubject;
//应援描述
@property (nonatomic,strong) NSString *describe;
//活动图片
@property (nonatomic,strong) NSString *activePicture;
//活动价格范围
@property (nonatomic,strong) NSString *activePrice;
//活动状态
@property (nonatomic,strong) NSString *activeStatus;
//应援目标金额
@property (nonatomic,strong) NSString *activeTargetMoney;
//已筹金额
@property (nonatomic,assign) float *activeCurrentMoney;
//详情图片
@property (nonatomic,strong) NSString *activeDetailsPic;
//详情文字
@property (nonatomic,strong) NSString *activeDetailsTx;
//结束日期
@property (nonatomic,strong) NSString *activeEndDate;
//点赞次数
@property (nonatomic,strong) NSString *activeFabulous;
//关联明星(可能多个)
@property (nonatomic,strong) NSString *associateStars;

@end
