//
//  ReceiptAddressModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/5.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface ReceiptAddressModel : BaseModel
//记录id
@property (nonatomic,strong) NSString *id;
//用户id
@property (nonatomic,strong) NSString *userId;
//收货人姓名
@property (nonatomic,strong) NSString *receiptName;
//手机号码
@property (nonatomic,strong) NSString *receiptPhone;
//所在省市区
@property (nonatomic,strong) NSString *province;
//详细地址
@property (nonatomic,strong) NSString *detailAddress;
//是否为默认地址
@property (nonatomic,assign) BOOL defaultAddress;
//创建时间
@property (nonatomic,strong) NSString *createTime;
//更新时间
@property (nonatomic,strong) NSString *updateTime;
@end
