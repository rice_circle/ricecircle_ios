//
//  StringListModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/8/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseModel.h"

@interface StringListModel : BaseModel

@property (nonatomic,strong) NSArray *data;

@end
