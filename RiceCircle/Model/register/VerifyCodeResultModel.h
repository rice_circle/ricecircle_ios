//
//  VerifyCodeResultModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/30.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseModel.h"
#import "VerifyCodeModel.h"

@interface VerifyCodeResultModel : BaseModel

@property (nonatomic,strong) VerifyCodeModel *data;
    
@end
