//
//  VerifyCodeModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/30.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseModel.h"

@interface VerifyCodeModel : MTLModel<MTLJSONSerializing>

//手机号
@property (nonatomic,strong) NSString *mobile;

//验证码
@property (nonatomic,strong) NSString *verifyCode;
//有效时间
@property (nonatomic,assign) NSInteger validateActiveTime;
@end
