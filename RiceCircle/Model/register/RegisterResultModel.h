//
//  RegisterResultModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/30.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseModel.h"
#import "UserModel.h"

@interface RegisterResultModel : BaseModel

@property (nonatomic,strong) UserModel *data;

@end
