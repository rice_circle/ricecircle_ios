//
//  Test.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface Test : BaseModel
@property (nonatomic,assign) NSInteger pageNum;
@property (nonatomic,assign) NSInteger pageSize;
@end
