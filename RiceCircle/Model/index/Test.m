//
//  Test.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "Test.h"

@implementation Test

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[super JSONKeyPathsByPropertyKey]];
    [dict setObject:@"pageNum" forKey:@"pageNum"];
    [dict setObject:@"pageSize" forKey:@"pageSize"];
    return dict;
}
@end
