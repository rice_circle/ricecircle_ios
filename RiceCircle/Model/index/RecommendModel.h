//
//  RecommendModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/14.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseModel.h"

@interface RecommendModel : BaseModel
@property (nonatomic,assign) NSInteger type;
@end
