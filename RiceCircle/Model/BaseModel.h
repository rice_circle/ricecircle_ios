//
//  BaseModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mantle.h"

@interface BaseModel : MTLModel<MTLJSONSerializing>
@property (nonatomic,assign) NSString *message;
@property (nonatomic,assign) NSString *code;
@end
