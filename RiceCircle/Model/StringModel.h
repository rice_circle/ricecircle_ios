//
//  StringModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseModel.h"

@interface StringModel : BaseModel

@property (nonatomic,strong) NSString *data;

@end
