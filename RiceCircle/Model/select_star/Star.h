//
//  Star.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/25.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Star : NSObject
//姓名
@property (nonatomic , copy) NSString *name;
//头像
@property (nonatomic , copy) NSString *photoUrl;
//粉丝数
@property (nonatomic , assign) NSInteger fans;
//是否已选择
@property (nonatomic , assign) BOOL relation;
@end
