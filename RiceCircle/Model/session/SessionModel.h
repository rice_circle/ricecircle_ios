//
//  SessionModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/2.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseModel.h"

@interface SessionModel : BaseModel
//sessionId
@property (nonatomic,assign) long sessionId;
//服务器下发的随机码(也可以称为公钥)
@property (nonatomic,assign) long randomN;
//内容签名
@property (nonatomic,assign) NSString *sign;
@end
