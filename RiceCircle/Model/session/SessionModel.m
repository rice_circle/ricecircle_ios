//
//  SessionModel.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/2.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "SessionModel.h"

@implementation SessionModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[super JSONKeyPathsByPropertyKey]];
    [dict setObject:@"Data.SessionId" forKey:@"sessionId"];
    [dict setObject:@"Data.RandomN" forKey:@"randomN"];
    [dict setObject:@"Data.Sign" forKey:@"sign"];
    return dict;
}
@end
