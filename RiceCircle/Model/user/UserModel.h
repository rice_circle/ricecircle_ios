//  用户model
//  UserModel.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/16.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "BaseModel.h"

@interface UserModel : BaseModel
/**手机*/
@property (nonatomic,assign) NSInteger id;

/**手机*/
@property (nonatomic,strong) NSString *mobile;

/**密码*/
@property (nonatomic,strong) NSString *password;

/**昵称*/
@property (nonatomic,strong) NSString *nickName;

/**性别（0女,1男）*/
@property (nonatomic,assign) NSInteger sex;

/**生日*/
@property (nonatomic,strong) NSString *birthday;

/**个性签名*/
@property (nonatomic,strong) NSString *motto;

/**积分*/
@property (nonatomic,assign) NSInteger points;

/**关注明星*/
@property (nonatomic,assign) NSInteger starId;

/**头像*/
@property (nonatomic,strong) NSString *portrait;

/**是否认证（0待认证,1已认证）*/
@property (nonatomic,assign) NSInteger isCertificated;

@end
