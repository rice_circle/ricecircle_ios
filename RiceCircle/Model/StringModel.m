//
//  StringModel.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/27.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "StringModel.h"

@implementation StringModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:[super JSONKeyPathsByPropertyKey]];
    [result setValue:@"data" forKey:@"data"];
    return result;
}
@end
