//
//  NSUserDefaultsManager.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/31.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaultsManager : NSObject

IOS_SINGLETON_H(NSUserDefaultsManager)


/**
 保存账号

 @param phone 手机号
 @param pwd 密码
 */
- (void)saveAccount:(NSString*)phone pwd:(NSString*)pwd;


/**
 获取手机号

 @return 手机号
 */
- (NSString*)getPhone;


/**
 获取密码

 @return 密码
 */
- (NSString*)getPassword;
@end
