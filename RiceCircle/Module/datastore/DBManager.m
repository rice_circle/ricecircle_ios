//
//  DBManager.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/30.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "DBManager.h"
#import "FMDB.h"
#import "DateUtil.h"

//当前数据库版本号
static const NSInteger CURRENT_DB_VERSION = 1;

@interface DBManager()
@property (nonatomic,strong) FMDatabaseQueue *dbQueue;
@property (nonatomic,strong) FMDatabase *db;
@end

@implementation DBManager

static DBManager *_instance = nil;

//单例对象
+ (instancetype)shareInstance{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] initQueue] ;
    }) ;
    return _instance ;
}

//重写allocWithZone:方法，在这里创建唯一的实例(注意线程安全)
+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [super allocWithZone:zone];
    });
    return _instance;
}

-(id)copyWithZone:(struct _NSZone *)zone{
    return _instance;
}

//逻辑：先判断数据库文件是否存在document中，如果不存在的话，就创建数据库文件，如果存在
//就从UserDefaults中获取保存的数据库版本号，然后递归处理每个版本之间的差异
- (instancetype)initQueue{
    self = [super init];
    if (self) {
        NSInteger dbVersion = 0;
        if ([self exitDB]) {
            dbVersion = [self getPreDBVersion];
        }
        [self openDB];
        if(dbVersion != CURRENT_DB_VERSION){
            [self upgradeDB:dbVersion];
        }
    }
    return self;
}

//递归处理每个数据库版本之间的差异
- (void)upgradeDB:(NSInteger)dbVersion{
    if(dbVersion > CURRENT_DB_VERSION){
        [self setPreDBVersion:CURRENT_DB_VERSION];
        return;
    }
    switch (dbVersion) {
        case 1:
            [self handlerVersion1];
            break;
    }
    dbVersion ++;
    [self upgradeDB:dbVersion];
}

//获取本地保存的数据库版本号
- (NSInteger)getPreDBVersion{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSInteger version = [user integerForKey:@"rice_circle_db_version"];
    NSLog(@"getPreDBVersion version=%lu",version);
    return version;
}

//设置本地数据库版本号
- (void)setPreDBVersion:(NSInteger)version{
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setInteger:version forKey:@"rice_circle_db_version"];
}

//判断数据库是否存在
- (BOOL)exitDB{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:[self getDBPath]];
}

//获取数据库文件路径
- (NSString *)getDBPath{
    //获取document路径
    NSString *documentPath = IOSDocumentPath;
    //生成数据库文件路径
    NSString *dbPath = [documentPath stringByAppendingString:@"/RiceCircle.sqlite"];
    NSLog(@"db path=%@",dbPath);
    return dbPath;
}

//创建数据库
- (void)openDB{
    NSLog(@"openDB");
    _db = [[FMDatabase alloc] initWithPath:[self getDBPath]];
    _dbQueue = [[FMDatabaseQueue alloc] initWithPath:[self getDBPath]];
    if(_db.open){
        NSLog(@"打开成功");
    }else{
        NSLog(@"打开失败");
    }
}


/**
 关闭数据库连接
 */
- (void)closeDBConnect{
    if (_db != nil) {
        [_db close];
    }
}
#pragma 处理每个数据库版本之间的差异
//version为1时，创建http数据请求表
- (void)handlerVersion1{
    [_dbQueue inDatabase:^(FMDatabase *db) {
        NSString *httpRequestDataSql = @"CREATE TABLE http_request_data (url nvarchar(100) PRIMARY KEY NOT NULL,content text,create_date long(15) NOT NULL,limt_time long(10) NOT NULL)";
        [db executeUpdate:httpRequestDataSql];
        //BOOL result = [db close];
        //NSLog(@"handlerVersion1 result=%d",result);
    }];
}


#pragma 具体数据库操作


/**
 添加请求数据

 @param data 请求数据
 */
- (void)insertNetData:(NetResponeData *)data{
    NSLog(@"insertNetData data=%@",data.url);
    [_dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSLog(@"data = %@",data);
        NSString *sql = @"select * from http_request_data where url = ?";
        NSString *url = data.url;
        FMResultSet *result = [db executeQuery:sql,url];
        BOOL hasNext = result.next;
        //如果对应的url的数据在数据库中已存在，就更新已有的，否则就创建一条新的数据
        long time = [DateUtil currentTimeMillis];
        if(hasNext){
            sql = @"update http_request_data set content = ?,create_date = ?,limt_time=? where url=?";
            [db executeUpdate:sql,data.content,[NSNumber numberWithLong:time],[NSNumber numberWithLong:data.limtTime],data.url];
        }else{
            sql = @"insert into http_request_data(url,content,create_date,limt_time) values(?,?,?,?)";
            [db executeUpdate:sql,data.url,data.content, [NSNumber numberWithLong:time] ,[NSNumber numberWithLong:data.limtTime]];
        }
    }];
}

/**
 根据id查询http请求数据
 
 @param _id id主键
 @return http请求数据
 */
- (NetResponeData*)queryNetDataById:(NSString*)_id{
    __block NetResponeData *data = nil;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        NSString *sql = @"select * from http_request_data where url = ?";
        NSLog(@"queryNetDataById");
        FMResultSet *result = [db executeQuery:sql,_id];
        if(result.next){
            data = [[NetResponeData alloc] init];
            data.url = [result stringForColumn:@"url"];
            data.content = [result stringForColumn:@"content"];
            data.createDate = [result longForColumn:@"create_date"];
            data.limtTime = [result longForColumn:@"limt_time"];
        }
    }];
    return data;
}
@end
