//  NSUserDefaults管理类
//  NSUserDefaultsManager.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/31.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "NSUserDefaultsManager.h"
#import "EncryptUtil.h"

//手机号保存key
NSString * const PHONE = @"phone";
//密码保存key
NSString * const PASSWORD = @"password";

@interface NSUserDefaultsManager()

@end

@implementation NSUserDefaultsManager

IOS_SINGLETON_M(NSUserDefaultsManager)

/**
 保存账号
 
 @param phone 手机号
 @param pwd 密码
 */
- (void)saveAccount:(NSString*)phone pwd:(NSString*)pwd{
    NSString *pwdCiphertext = [EncryptUtil encryptPassword:pwd];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:phone forKey:PHONE];
    [userDefaults setObject:pwdCiphertext forKey:PASSWORD];
    [userDefaults synchronize];
}

/**
 获取手机号
 
 @return 手机号
 */
- (NSString*)getPhone{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults stringForKey:PHONE];
}


/**
 获取密码
 
 @return 密码
 */
- (NSString*)getPassword{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults stringForKey:PASSWORD];
}

@end
