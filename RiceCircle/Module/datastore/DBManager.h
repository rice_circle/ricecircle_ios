//
//  DBManager.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/30.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetResponeData.h"

@interface DBManager : NSObject
//生成单例对象
+(instancetype) shareInstance;


/**
 关闭数据库连接
 */
- (void)closeDBConnect;

/**
 插入http请求的数据
 
 @param data 请求数据
 */
- (void)insertNetData:(NetResponeData *)data;


/**
 根据id查询http请求数据

 @param _id id主键
 @return http请求数据
 */
- (NetResponeData*)queryNetDataById:(NSString*)_id;
@end
