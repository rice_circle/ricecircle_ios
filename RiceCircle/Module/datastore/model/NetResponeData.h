//
//  NetResponeData.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/31.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetResponeData : NSObject
//请求地址
@property NSString *url;
//请求返回的内容
@property NSString *content;
//创建时间
@property long createDate;
//失效时间
@property long limtTime;
@end
