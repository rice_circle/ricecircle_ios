//
//  CacheManager.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/6/2.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>

//http请求签名加密key
static  NSString * const PRIVATE_KEY = @"hello key";

@interface CacheManager : NSObject
//服务器下发的随机code
@property (nonatomic,assign) NSInteger SERVCIE_RANDOM_KEY;
//http请求session
@property (nonatomic,strong) NSString *httpSession;

IOS_SINGLETON_H(CacheManager);

@end
