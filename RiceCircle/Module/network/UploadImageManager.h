//
//  UploadImageManager.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/8/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StringListModel.h"

typedef void(^ImageUploadBlock)(StringListModel *result);

@interface UploadImageManager : NSObject

/**
 图片上传
 
 @param data 图片内容
 */
+ (void)uploadImage:(NSData*) data withBlock:(ImageUploadBlock)block;
@end
