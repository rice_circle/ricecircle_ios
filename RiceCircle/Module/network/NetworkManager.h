//
//  NetworkManager.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/29.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>



@protocol NetResponseDelegate <NSObject>
//返回结果
- (void)success:(id)result url:(NSString*)url;
//错误结果
- (void)fail:(NSString *)error url:(NSString*)url;
@end

@interface NetworkManager : NSObject

//请求方法define
typedef enum {
    GET,
    POST,
    PUT,
    DELETE,
    HEAD
} HTTPMethod;

//api签名或者加密
typedef NS_ENUM(NSInteger,APIEncrypt){
    NONE = -1,
    ENCRYPT=0,
    SIGNATURE = 1,
    ENCRYPT_AND_SIGN = 2
};
//请求地址域名后者ip(例如：http://com.example.v1/user/login ),其中http://com.example.v1 则为baseUrl
@property (nonatomic,assign) NSString *baseUrl;

//生成单例对象
+(instancetype) shareInstance ;

/**
 发送http post请求(默认也签名也不加密，同时也不缓存请求数据)
 
 @param path 接口路径
 @param params 请求参数
 @param cls 用来解析返回结果的class
 @param delegate 响应回调代理
 */
- (void)sendRequestWithPath:(NSString *)path withParams:(NSMutableDictionary *)params withCls:(__unsafe_unretained Class)cls withDelegate:(id<NetResponseDelegate>)delegate;

/**
 发送http请求(默认也签名也不加密，同时也不缓存请求数据)

 @param method 请求方法(post、get等)
 @param path 接口路径
 @param params 请求参数
 @param cls 用来解析返回结果的class
 @param delegate 响应回调代理
 */
- (void) sendRequestWithMethod:(HTTPMethod) method withPath:(NSString *)path
                    withParams:(NSMutableDictionary*)params withCls:(Class)cls withDelegate:(id<NetResponseDelegate>) delegate;


/**
 发送http请求(使用者可以根据自身情况选择加密或者签名，不缓存请求数据)

 @param method 请求方法(post、get等)
 @param path 接口路径
 @param params 请求参数
 @param cls 用来解析返回结果的class
 @param delegate 响应回调代理
 @param encrypt api签名或者加密
 */
- (void) sendRequestWithMethod:(HTTPMethod) method withPath:(NSString *)path
                    withParams:(NSMutableDictionary*)params withCls:(Class)cls withDelegate:(id<NetResponseDelegate>) delegate withEncrypt:(APIEncrypt) encrypt;

/**
 发送http请求(默认也签名也不加密，缓存请求数据)

 @param method 请求方法(post、get等)
 @param path 接口路径
 @param params 请求参数
 @param cls 用来解析返回结果的class
 @param cacheTime 响应数据缓存时间
 @param delegate 响应回调代理
 @param allow 是否允许缓存
 */
- (void) sendRequestWithMethod:(HTTPMethod) method withPath:(NSString *)path
                    withParams:(NSMutableDictionary*)params withCls:(Class)cls withCacheTime:(long)cacheTime withAllowCache:(BOOL)allow withDelegate:(id<NetResponseDelegate>) delegate;

/**
 发送http请求(使用者可以根据自身情况选择加密或者签名，也可以选择是否缓存请求数据)
 
 @param method 请求方法(post、get等)
 @param path 接口路径
 @param params 请求参数
 @param cls 用来解析返回结果的class
 @param cacheTime 响应数据缓存时间
 @param delegate 响应回调代理
 @param allow 是否允许缓存
 @param encrypt api签名或者加密
 */
- (void) sendRequestWithMethod:(HTTPMethod) method withPath:(NSString *)path
                    withParams:(NSMutableDictionary*)params withCls:(Class)cls withCacheTime:(long)cacheTime withAllowCache:(BOOL)allow withDelegate:(id<NetResponseDelegate>) delegate withEncrypt:(APIEncrypt) encrypt;
@end
