//  图片上传管理类
//  UploadImageManager.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/8/1.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "UploadImageManager.h"
#import "APIConstant.h"
#import "Mantle.h"
#import "StringListModel.h"

@implementation UploadImageManager


/**
 图片上传

 @param data 图片内容
 */
+ (void)uploadImage:(NSData*) data withBlock:(ImageUploadBlock)block{
    NSString *url = [API_BASE_URL stringByAppendingString:API_UploadImage];
    //构建请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
    request.HTTPMethod = @"POST";
    //分界线的标识符
    NSString *TWITTERFON_FORM_BOUNDARY = @"AaB03x";
    
    //分界线 --AaB03x
    NSString *MPboundary=[[NSString alloc]initWithFormat:@"--%@",TWITTERFON_FORM_BOUNDARY];
    //结束符 AaB03x--
    NSString *endMPboundary=[[NSString alloc]initWithFormat:@"%@--",MPboundary];
    
    /*
     上传格图片格式：
     --AaB03x
     Content-Disposition: form-data; name="file"; filename="currentImage.png"
     Content-Type: image/png
     */
    //http body的字符串
    NSMutableString *body=[[NSMutableString alloc]init];
    // 添加分界线，换行
    // [body appendFormat:@"%@\r\n",MPboundary];
    //添加分界线，换行
    [body appendFormat:@"%@\r\n",MPboundary];
    //声明file字段，文件名为currentImage.png
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    formatter.dateFormat=@"yyyyMMddHHmmss";
    NSString *str=[formatter stringFromDate:[NSDate date]];
    NSString *fileName=[NSString stringWithFormat:@"%@.png",str];
    [body appendFormat:@"%@", [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n",fileName]];
    //声明上传文件的格式
    [body appendFormat:@"Content-Type: image/png\r\n\r\n"];
    
    NSLog(@"网络请求body:%@",body);
    //声明结束符：--AaB03x--
    NSString *end=[[NSString alloc]initWithFormat:@"\r\n%@",endMPboundary];
    //声明myRequestData，用来放入http body
    NSMutableData *myRequestData=[NSMutableData data];
    //将body字符串转化为UTF8格式的二进制
    [myRequestData appendData:[body dataUsingEncoding:NSUTF8StringEncoding]];
    //将image的data加入
    [myRequestData appendData:data];
    //加入结束符--AaB03x--
    [myRequestData appendData:[end dataUsingEncoding:NSUTF8StringEncoding]];
    
    //设置HTTPHeader中Content-Type的值
    NSString *content=[[NSString alloc]initWithFormat:@"multipart/form-data; boundary=%@",TWITTERFON_FORM_BOUNDARY];
    //设置HTTPHeader
    [request setValue:content forHTTPHeaderField:@"Content-Type"];
    //设置Content-Length
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[myRequestData length]] forHTTPHeaderField:@"Content-Length"];
    //设置http body
    [request setHTTPBody:myRequestData];
    
    
    //NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        //网络请求失败
        if (error != nil) {
            return;
        }
        NSString *aString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"aString=%@",aString);
        //成功进行解析
        NSMutableDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSError *MantleError;
        StringListModel *result = [MTLJSONAdapter modelOfClass:[StringListModel class] fromJSONDictionary:dict error:&MantleError];
        NSLog(@"%@--%@",dict, response);
        if(block){
            block(result);
        }
    }];
    
    [task resume];
}

@end
