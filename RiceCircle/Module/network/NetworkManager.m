//
//  NetworkManager.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/29.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "NetworkManager.h"
#import "AFNetworking.h"
#import "DBManager.h"
#import "NetResponeData.h"
#import "DateUtil.h"
#import "MJExtension.h"
#import "Mantle.h"
#import "StringUtil.h"
#import "EncryptUtil.h"
#import "CacheManager.h"
#import "MBProgressHUD.h"

@interface NetworkManager()

@property(nonatomic,strong) AFHTTPSessionManager *sessionManager;

@property (nonatomic,strong) MBProgressHUD *progressBar;
@end

@implementation NetworkManager


static NetworkManager* _instance = nil;

//单例实现
+(instancetype) shareInstance{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] initAF] ;
    }) ;
    
    return _instance ;
}

//重写allocWithZone:方法，在这里创建唯一的实例(注意线程安全)
+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [super allocWithZone:zone];
    });
    return _instance;
}

-(id)copyWithZone:(struct _NSZone *)zone{
    return _instance;
}

//初始化anf
- (instancetype)initAF{
    self = [super init];
    if(self){
        _sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:nil];
        //最大请求并发任务数
        _sessionManager.operationQueue.maxConcurrentOperationCount = 5;
        // 请求格式
        // AFHTTPRequestSerializer            二进制格式
        // AFJSONRequestSerializer            JSON
        // AFPropertyListRequestSerializer    PList(是一种特殊的XML,解析起来相对容易)
        _sessionManager.requestSerializer = [AFHTTPRequestSerializer serializer]; // json格式上传
//        [_sessionManager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
        [_sessionManager.requestSerializer setValue:@"UTF-8" forHTTPHeaderField:@"Charset"];
        [_sessionManager.requestSerializer setValue:@"UTF-8" forHTTPHeaderField:@"Accept-Charset"];
        //[_sessionManager.requestSerializer setValue:@"gzip,deflate" forHTTPHeaderField:@"Accept-Encoding"];
        [_sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        // 超时时间
        _sessionManager.requestSerializer.timeoutInterval = 30.0f;
        //, @"text/plain", @"text/javascript", @"text/json", @"text/html"
        _sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", nil];
        _sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

/**
 发送http post请求(默认也签名也不加密，同时也不缓存请求数据)
 
 @param path 接口路径
 @param params 请求参数
 @param cls 用来解析返回结果的class
 @param delegate 响应回调代理
 */
- (void)sendRequestWithPath:(NSString *)path withParams:(NSMutableDictionary *)params withCls:(__unsafe_unretained Class)cls withDelegate:(id<NetResponseDelegate>)delegate{
    [self sendRequestWithMethod:POST withPath:path withParams:params withCls:cls withDelegate:delegate];
}


/**
 发送http请求(默认也签名也不加密，同时也不缓存请求数据)
 
 @param method 请求方法(post、get等)
 @param path 接口路径
 @param params 请求参数
 @param cls 用来解析返回结果的class
 @param delegate 响应回调代理
 */
- (void)sendRequestWithMethod:(HTTPMethod)method withPath:(NSString *)path withParams:(NSMutableDictionary *)params withCls:(__unsafe_unretained Class)cls withDelegate:(id<NetResponseDelegate>)delegate{
    [self sendRequestWithMethod:method withPath:path withParams:params withCls:cls withDelegate:delegate withEncrypt:NONE];
}

/**
 发送http请求(使用者可以根据自身情况选择加密或者签名，不缓存请求数据)
 
 @param method 请求方法(post、get等)
 @param path 接口路径
 @param params 请求参数
 @param cls 用来解析返回结果的class
 @param delegate 响应回调代理
 @param encrypt api签名或者加密
 */
- (void)sendRequestWithMethod:(HTTPMethod)method withPath:(NSString *)path withParams:(NSMutableDictionary *)params withCls:(Class)cls withDelegate:(id<NetResponseDelegate>)delegate withEncrypt:(APIEncrypt)encrypt{
    [self sendRequestWithMethod:method withPath:path withParams:params withCls:cls withCacheTime:-1 withAllowCache:NO withDelegate:delegate withEncrypt:encrypt];
}

/**
 发送http请求
 
 @param method 请求方法(post、get等)
 @param path 接口路径
 @param params 请求参数
 @param cls 用来解析返回结果的class
 @param cacheTime 响应数据缓存时间(单位为秒)
 @param delegate 响应回调代理
 @param allow 是否允许缓存
 */
- (void) sendRequestWithMethod:(HTTPMethod) method withPath:(NSString *)path
                    withParams:(NSMutableDictionary*)params withCls:(__unsafe_unretained Class)cls withCacheTime:(long)cacheTime withAllowCache:(BOOL)allow withDelegate:(id<NetResponseDelegate>) delegate{
    [self sendRequestWithMethod:method withPath:path withParams:params withCls:cls withCacheTime:cacheTime withAllowCache:allow withDelegate:delegate withEncrypt:NONE];
}

/**
 发送http请求(使用者可以根据自身情况选择加密或者签名，也可以选择是否缓存请求数据)
 
 @param method 请求方法(post、get等)
 @param path 接口路径
 @param params 请求参数
 @param cls 用来解析返回结果的class
 @param cacheTime 响应数据缓存时间
 @param delegate 响应回调代理
 @param allow 是否允许缓存
 @param encrypt api签名或者加密
 */
- (void) sendRequestWithMethod:(HTTPMethod) method withPath:(NSString *)path
                    withParams:(NSMutableDictionary*)params withCls:(Class)cls withCacheTime:(long)cacheTime withAllowCache:(BOOL)allow withDelegate:(id<NetResponseDelegate>) delegate withEncrypt:(APIEncrypt) encrypt{
    [self showLoading];
    //获取请求地址
    NSString *url = [_baseUrl stringByAppendingString:path];
    if(allow){
        NSLog(@"allow");
        NetResponeData *data = [[DBManager shareInstance] queryNetDataById:url];
        long currentTime = [DateUtil currentTimeMillis];
        NSLog(@"data=%@",data);
        NSLog(@"currentTime=%ld,data.limtime=%ld,createDate=%ld",currentTime,data.limtTime,data.createDate);
        if(nil != data && ((data.createDate + data.limtTime * 1000) >= currentTime)){
            NSLog(@"load from cache");
            //把json字符串转成NSDictionary
            NSDictionary *dict = [StringUtil jsonStringConvertDictionary:data.content];
            if(dict != nil){
                NSError *error;
                id result = [MTLJSONAdapter modelOfClass:cls fromJSONDictionary:dict error:&error];
                if(nil != delegate){
                    [delegate success:result url:path];
                    [self closeLoading];
                    return;
                }
            }
        }
    }
    NSDictionary *resultParam = [self handlerParam:params withRule:encrypt];
    switch (method) {
        case GET:
            [self getRequestWithPath:path withParams:resultParam withCls:cls withCacheTime:cacheTime withAllowCache:allow withDelegate:delegate];
            break;
        case POST:
            [self postRequestWithPath:path withParams:resultParam withCls:cls withCacheTime:cacheTime withAllowCache:allow withDelegate:delegate];
            break;
        default:
            break;
    }
}

//处理参数加密或者签名
- (NSMutableDictionary*)handlerParam:(NSMutableDictionary*)param withRule:(APIEncrypt)rule{
    switch (rule) {
        case ENCRYPT://加密
            //return [self encryptParam:param];
        case SIGNATURE://签名
            //return [self singParam:param];
        case ENCRYPT_AND_SIGN://加密或者签名
            
            break;
        default:
            break;
    }
    return param;
}

//参数加密，
- (NSMutableDictionary*)encryptParam:(NSMutableDictionary*)param{
    //公钥
    NSInteger publicKey = [[CacheManager sharedCacheManager] SERVCIE_RANDOM_KEY];
    //私钥
    NSString *privateKey = PRIVATE_KEY;
    NSString *tempKey = [privateKey stringByAppendingString:[NSString stringWithFormat:@"%lu",publicKey]];
    //公钥和私钥组成一个字符串，然后采用sha3加密
    NSString *ciphertext = [EncryptUtil SHA3:tempKey withBitsLength:256];
    //参数转化为json字符串
    NSString *content = [StringUtil dictionaryConvertJsonString:param];
    NSString *result = [EncryptUtil AES:content key:ciphertext];
    return [NSMutableDictionary dictionaryWithObject:result forKey:@"Content"];
}

//参数签名
- (NSMutableDictionary*)singParam:(NSMutableDictionary*)param{
    //公钥
    NSInteger publicKey = [[CacheManager sharedCacheManager] SERVCIE_RANDOM_KEY];
    //私钥
    NSString *privateKey = PRIVATE_KEY;
    NSString *tempKey = [privateKey stringByAppendingString:[NSString stringWithFormat:@"%lu",publicKey]];
    NSLog(@"singParam 密钥和公钥字符串=%@",tempKey);
    //公钥和私钥组成一个字符串，然后采用sha3加密
    NSString *ciphertext = [EncryptUtil SHA3:tempKey withBitsLength:256];
    NSLog(@"singParam sha3 256加密之后字符串=%@",ciphertext);
    NSDictionary *sortDict = [self sortDictionary:param];
    NSString *jsonStr = [StringUtil dictionaryConvertJsonString:sortDict];
    NSLog(@"singParam 参数排序之后的字符串=%@",jsonStr);
    NSString *sha3Str = [EncryptUtil SHA3:jsonStr withBitsLength:256];
    NSLog(@"singParam 参数sha3加密之后的字符串=%@",sha3Str);
    NSString *result = [EncryptUtil AES:sha3Str key:tempKey];
    NSLog(@"singParam 参数和密钥sea加密之后的字符串=%@",result);
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:param];
    [dict setObject:result forKey:@"Sign"];
    return dict;
}

//get请求
- (void)getRequestWithPath:(NSString *)path withParams:(NSDictionary *)params withCls:(__unsafe_unretained Class)cls withCacheTime:(long)cacheTime withAllowCache:(BOOL)allow withDelegate:(id<NetResponseDelegate>)delegate{
    [_sessionManager GET:path parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = (NSDictionary *)responseObject;
        NSError *error;
        id result = [MTLJSONAdapter modelOfClass:cls fromJSONDictionary:dict error:&error];
        if(nil != delegate){
            [delegate success:result url:path];
        }
        //缓存网络请求数据
        if (allow) {
            [self cacheNetDataWithPath:path withCacheTime:cacheTime withData:dict];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(nil != delegate){
            [delegate fail: error.localizedDescription url:path];
        }
    }];
}

//post请求
- (void)postRequestWithPath:(NSString *)path withParams:(NSDictionary *)params withCls:(__unsafe_unretained Class)cls withCacheTime:(long)cacheTime withAllowCache:(BOOL)allow withDelegate:(id<NetResponseDelegate>)delegate{
    //获取请求地址
    NSString *url = [_baseUrl stringByAppendingString:path];
    NSLog(@"post request url=%@,params=%@",url,params);
    NSLog(@"param json=%@",[StringUtil dictionaryConvertJsonString:params]);
    [_sessionManager POST:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)[task response];
        NSLog(@"post request success %lu",response.statusCode);
        NSLog(@"header = %@",response.allHeaderFields);
        NSLog(@"responseObject=%@",responseObject);
        NSMutableDictionary *dict =[NSMutableDictionary dictionaryWithDictionary:(NSDictionary *)responseObject] ;
        NSLog(@"响应dict=%@",dict);
        NSLog(@"响应内容 = %@",[StringUtil dictionaryConvertJsonString:dict]);
        //NSString *jsonString = [dict objectForKey:@"data"];
        //NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        //NSString *json = (NSString*)[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //NSLog(@"转化之后data1=%@",json);
        //json = [StringUtil toJSONString:jsonString];
        //NSLog(@"转化之后data=%@",json);
        //[dict setObject:json forKey:@"data"];
        NSLog(@"转化之后的dict = %@",[StringUtil dictionaryConvertJsonString:dict]);
        NSError *error;
        id result = [MTLJSONAdapter modelOfClass:cls fromJSONDictionary:dict error:&error];
        if(nil != delegate){
            [delegate success:result url:path];
        }
        //缓存网络请求数据
        if (allow) {
            [self cacheNetDataWithPath:path withCacheTime:cacheTime withData:dict];
        }
        [self closeLoading];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(nil != delegate){
            [delegate fail: error.localizedDescription url:path];
        }
        NSData * data = error.userInfo[@"com.alamofire.serialization.response.error.data"];
        NSString * str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"服务器的错误原因:%@",str);
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)[task response];
        NSLog(@"header = %@",response.allHeaderFields);
        NSLog(@"post request fail error=%@",error);
        [self closeLoading];
    }];
}

//NSDictionary 按照字母降序排序
- (NSDictionary*)sortDictionary:(NSDictionary*)dict{
    if(dict == nil){
        return nil;
    }
    NSArray *keys = [dict allKeys];
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
    for (NSString *key in sortedArray) {
        [tempDict setObject:[dict objectForKey:key] forKey:key];
    }
    return [NSDictionary dictionaryWithDictionary:tempDict];
}

//显示加载框
- (void)showLoading{
    if(nil == _progressBar){
        UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
        _progressBar = [[MBProgressHUD alloc] initWithView:window];
        _progressBar.opaque = 0.1f;
        [window addSubview:_progressBar];
    }
    [_progressBar showAnimated:YES];
}

//关闭显示框
- (void)closeLoading{
    if(_progressBar){
        [_progressBar hideAnimated:YES];
        _progressBar = nil;
    }
}

/**
 缓存网络请求数据

 @param path 请求地址
 @param cacheTime 缓存时间
 @param dict 数据字典
 */
- (void)cacheNetDataWithPath:(NSString *)path withCacheTime:(long)cacheTime withData:(NSDictionary *)dict{
    NSLog(@"cacheNetDataWithPath path=%@",path);
    //获取请求地址
    NSString *url = [_baseUrl stringByAppendingString:path];
    NetResponeData *data = [[NetResponeData alloc] init];
    data.url = url;
    data.content = [StringUtil dictionaryConvertJsonString:dict];
    data.createDate = [DateUtil currentTimeMillis];
    data.limtTime = cacheTime > 0 ? cacheTime : NSIntegerMax;
    [[DBManager shareInstance] insertNetData:data];
}
@end
