//
//  NetDataCacheLimtTime.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/5/31.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#ifndef NetDataCacheLimtTime_h
#define NetDataCacheLimtTime_h

#define SECOND = 1//一秒钟
#define HALF_MINUTE = 30//30秒
#define MINUTE = 60//一分钟
#define FIFTEEN_MINUTES = 90//15分钟
#define YEAR = 10000
#endif /* NetDataCacheLimtTime_h */
