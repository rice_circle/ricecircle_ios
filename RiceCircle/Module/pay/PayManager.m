//
//  PayManager.m
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/26.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import "PayManager.h"
#import <AlipaySDK/AlipaySDK.h>

@implementation PayManager

IOS_SINGLETON_M(PayManager)

/**
 支付宝支付
 
 @param orderInfo 订单信息
 */
- (void)alipay:(NSString*)orderInfo callbak:(PayResultCallback)resultCallback{
    // NOTE: 如果加签成功，则继续执行支付
    if (orderInfo != nil) {
        //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
        NSString *appScheme = @"rice.circle.alipay";
        
        // NOTE: 调用支付结果开始支付
        [[AlipaySDK defaultService] payOrder:orderInfo fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            NSLog(@"reslut = %@",resultDic);
            resultCallback(resultDic);
        }];
    }
}

@end
