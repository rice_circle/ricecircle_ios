//
//  PayManager.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/26.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^PayResultCallback)(NSDictionary *dict);

@interface PayManager : NSObject

IOS_SINGLETON_H(PayManager)


/**
 支付宝支付

 @param orderInfo 订单信息
 */
- (void)alipay:(NSString*)orderInfo callbak:(PayResultCallback)callback;

@end
