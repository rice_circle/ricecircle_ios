//
//  APIConstant.h
//  RiceCircle
//
//  Created by 孙继常 on 2017/7/29.
//  Copyright © 2017年 孙继常. All rights reserved.
//

#ifndef APIConstant_h
#define APIConstant_h

static NSString *const API_BASE_URL = @"http://localhost:8080/simpleframe/";

//获取订单签名接口
static NSString *const API_ObtainOrderInfo =@"api/pay/obtainOrderSignInfo";
//发送验证码
static NSString *const API_SendVerifyCode =@"api/shortMessage/sendVerifyCode";

//注册
static NSString *const API_Register =@"api/member/regist";
//图片上传
static NSString *const API_UploadImage =@"api/upload/uploadImage";
//获取关注的明星
static NSString *const API_AttentionStar =@"api/star/getListWithAttention";

#endif /* APIConstant_h */
